<h1 style="text-align:center"><strong>nameOfTrustgoiper<br />
Table of Contents</strong></h1>

<p><span style="color:teal; font-family:webdings; font-size:11.0pt">U</span><span style="color:teal">We recommend purchasing sets of colored index tabs [1-10] and using them to organize your business plan &ndash; align the numbers with the tabs and delete this comment to make this page fit to one page.</span></p>

<p><span style="color:teal; font-family:webdings; font-size:11.0pt">U</span><span style="color:teal">Adjust the order, size and line spacing of the headings to match your business trust!</span></p>

<p><span style="font-size:14.0pt">Net Estate Calculator.............................................................................. 1</span></p>

<p><span style="font-size:14.0pt">Accounts &amp; Contacts.............................................................................. &nbsp;2</span></p>

<p><span style="font-size:14.0pt">Living Trust &nbsp;&nbsp; .......................................................................................... 3</span></p>

<p><span style="font-size:14.0pt">Pour-Over Will .........................................................................................3</span></p>

<p><span style="font-size:14.0pt">Quitclaim Deed .......................................................................................&nbsp;4</span></p>

<p><span style="font-size:14.0pt">Transfer of Assets Letter ........................................................................ 4</span></p>

<p><span style="font-size:14.0pt">Successor Trustee....................................................................................5</span></p>

<p><span style="font-size:14.0pt">Durable Power of Attorney ...................................................................... 6</span></p>

<p><span style="font-size:14.0pt">Amendment of Trust .................................................................................7</span></p>

<p><span style="font-size:14.0pt">Advance Directive to Physicians ............................................................. 8</span></p>

<p><span style="font-size:14.0pt">Living Will&nbsp;&nbsp;&nbsp;&nbsp;&nbsp; ........................................................................................... 8</span></p>

<p><span style="font-size:14.0pt">Health Care Power of Attorney..................................................................8</span></p>

<p><span style="font-size:14.0pt">HIPAA Privacy Authorization.................................................................... 8</span></p>

<p><span style="font-size:14.0pt">California</span><span style="font-size:14.0pt"> Directive to Physicians............................................................. 8</span></p>

<p><span style="font-size:14.0pt">Business - General Partnership Agreement ............................................&nbsp;9</span></p>

<p><span style="font-size:14.0pt">Business - Corporate Buy-Sell Agreement .............................................. 9</span></p>

<p><span style="font-size:14.0pt">After Death Checklist .............................................................................. 10</span></p>

<p>&nbsp;</p>

<p>&nbsp;</p>
