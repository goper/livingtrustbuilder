
$( document ).ready(function() {

	$( "#loginBtn" ).on( "click", function() {
			$('#mainTitle').text('LOGIN');
			$('.textContainer').css('background-color', '#337ab7');
			$('.footerContainer').css('background-color', '#337ab7');
			$('.registerContainer').css('display', 'none');
			$('.login').css('display', 'block');
	});

	$( "#registerBtn" ).on( "click", function() {
			register();
	});

	$( "#resetBtn" ).on( "click", function() {
			reset();
	});

	$( "#forgotPassword" ).on( "click", function() {
			reset();
	});

	$( "#signUpBtn" ).on( "click", function() {
			register();
	});

	function register()
	{
		$('#mainTitle').text('REGISTER');
		$('.textContainer').css('background-color', '#662c92');
		$('.footerContainer').css('background-color', '#662c92');
		$('.login').css('display', 'none');
		$('.registerContainer').css('display', 'block');
	}

	function reset()
	{
		$('#mainTitle').text('ACCOUNT RESET');
		$('.textContainer').css('background-color', '#f05a28');
		$('.footerContainer').css('background-color', '#f05a28');
	}

	
});