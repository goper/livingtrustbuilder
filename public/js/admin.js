
$( document ).ready(function() {

	/*Staff Page JS*/
	$( ".clickStaff" ).on( "click", function() {
			$('#removeStaffBtn').attr('disabled');
			resetBackColor();
	    str = $(this).attr('id');
	    id = str.substr(0,str.indexOf(' '));

	    name = str.substr(str.indexOf(' ')+1);
	    $('#removeStaffName').html(name);
	    $('#choosenId').val(id);
	    $(this).css('background-color', '#d9534f');
	    if (id > 0) {
	    	$('#removeStaffBtn').removeAttr('disabled');
	  	} 
	  	
	});

	function resetBackColor()
	{
		$(".clickStaff").css('background-color', '#ffffff');
	}

	/*Start New Project JS */
	$('#selectBasePlan').on('change', function() {
		selected = $('#selectBasePlan').find(":selected").text();
		
		if (selected == 'Single Person') {
			$('#basePlanheader').text('Single Person');
			$('#forSingleChoices').css('display', 'block');
			$('#forMarriedChoices').css('display', 'none');

		} else {
			$('#basePlanheader').text('Married Couple');
			$('#forSingleChoices').css('display', 'none');
			$('#forMarriedChoices').css('display', 'block');
		}
	  
	});

	$( "#nextBtnFirstPage" ).on( "click", function() {
			$('#firstPage').css('display', 'none');
	  	$('#secondPage').css('display', 'block');
	  	$('#backBtnFirstPage').css('display', 'none');
	  	$('#backBtnSecondPage').css('display', 'block');
	  	$('#nextBtnFirstPage').css('display', 'none');
	  	$('#nextBtnSecondPage').css('display', 'block');

	});

	$( "#backBtnSecondPage" ).on( "click", function() {
			$('#backBtnSecondPage').css('display', 'none');
			$('#backBtnFirstPage').css('display', 'block');
	  	$('#secondPage').css('display', 'none');
	  	$('#firstPage').css('display', 'block');
	  	$('#nextBtnFirstPage').css('display', 'block');
	  	$('#nextBtnSecondPage').css('display', 'none');
	});

  $('#selectAll').click(function(event) {
	  if(this.checked) { // check select status
	    $('.chkBx').each(function() { //loop through each checkbox
	      this.checked = true;  //select all checkboxes with class "checkbox1"               
	    });
	  } else {
	    $('.chkBx').each(function() { //loop through each checkbox
	        this.checked = false; //deselect all checkboxes with class "checkbox1"                       
	    });         
	  }
  });

  $('#toggleLeft').click(function(event) {
  	$('#menuLeft').toggle("slow", function(){
  		$('#mainPage').attr('class', 'col-lg-10 col-md-10');
  		$('#mainPage').attr('class', 'col-lg-11 col-md-11');
  		$('#mainPage').attr('class', 'col-lg-12 col-md-12');
		  $('#mainPage').css('left', '0');
		  $('#mainPage').css('margin-left', '-76px');
  	});
  	
  	$('#tooggler').css('display', 'block');
	  $('#tooggler').css('margin-left', '17px');
	 /* $('#menuLeft').css('display', 'none');
	  $('#menuLeft').attr('class', '');

	  $('#mainPage').attr('class', 'col-lg-12 col-md-12');
	  $('#mainPage').css('left', '0');
	  $('#mainPage').css('margin-left', '-76px');

	  $('#tooggler').css('display', 'block');
	  $('#tooggler').css('margin-left', '17px');*/
  });

  $('#tooggler').click(function(event) {
  	$('#menuLeft').toggle("slow",  function(){
  		$('#tooggler').css('display', 'none');
  		$('#mainPage').attr('class', 'col-lg-9 col-md-9');
	  	$('#mainPage').css('left', '0');
	  	$('#mainPage').css('margin-left', '0px');
  	});
  	
	  /*$('#menuLeft').css('display', 'block');
	  $('#menuLeft').attr('class', 'col-lg-3 col-md-3');

	  $('#mainPage').attr('class', 'col-lg-9 col-md-9');
	  $('#mainPage').css('left', '0');
	  $('#mainPage').css('margin-left', '0px');

	  $('#tooggler').css('display', 'none');*/
  });

  $('#addRowBankAccount').on('click', function() {
  	var inserData = '<tr><td><input type="text" value="" id="bankName" class="form-control input-sm" style="width: 100%"></td><td><select class="form-control input-sm"><option>Checking</option><option>Savings</option>' + 
      '</select></td><td><input type="text" value="" id="bankName" class="form-control input-sm" style="width: 100%"></td><td><select class="form-control input-sm">' + 
      '<option>Husband/Single</option><option>Wife</option><option>Joinly Held</option></select></td><td><input type="text" value="" id="bankName" class="form-control input-sm" style="width: 100%"></td>' +
      '<td><input type="text" value="" id="bankName" class="form-control input-sm" style="width: 100%"></td><td><input type="text" value="" id="bankName" class="form-control input-sm" style="width: 100%"></td>' +
    '<td><input type="text" value="" id="bankName" class="form-control input-sm" style="width: 100%"></td></tr>';
  	
  	$(inserData).insertBefore('#addRow');
    	
  });


	$("#myTable table td:first-child input").change(function(){
		alert('asd');
    $(this)
       .closest('tr') // find the parent row
           .find(":input[type='text']") // find text elements in that row
               .attr('disabled',false) // enable them
           .end() // go back to the row
           .siblings() // get its siblings
               .find(":input[type='text']") // find text elements in those rows
                   .attr('disabled',true); // disable them
	});

	$('.wpcufpn_default').attr('src', 'http://elec.ookcreatives.com/wp-content/uploads/2013/01/news_icon_2.png');
});

/*wpcufpn_default
wpcufpn_default*/