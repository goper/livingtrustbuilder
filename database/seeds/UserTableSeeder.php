<?php

use Illuminate\Database\Seeder;
use App\User;

class UserTableSeeder extends Seeder {

    public function run()
    {

      User::create(array(
      	'first_name' => 'ook',
      	'last_name' => 'admin',
	      'email' => 'admin@outsource.com',
	      'password' => Hash::make('admin'),
	    ));
    }

}