<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateNetEstateTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('net_estate', function (Blueprint $table) {
            $table->increments('id')->unsigned();
            $table->integer('plan_id')->unsigned();
            $table->string('type', 40);
            $table->string('property_of', 20);
            $table->string('child_1', 10);
            $table->string('child_2', 10);
            $table->string('child_3', 10);
            $table->string('child_total', 10);
            $table->string('col_1', 50);
            $table->string('col_2', 50);
            $table->string('col_3', 50);
            $table->string('col_4', 50);
            $table->timestamps();

            $table->foreign('plan_id')
              ->references('id')->on('plans')
              ->onDelete('cascade');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::drop('net_estate');
    }
}
