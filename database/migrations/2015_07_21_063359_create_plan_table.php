<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreatePlanTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('plans', function (Blueprint $table) {
            $table->increments('id')->unsigned();
            $table->integer('project_id')->unsigned();
            $table->string('name', 30);
            $table->string('base_plan', 20);
            $table->string('first_name', 20);
            $table->string('last_name', 30);
            //Base on software
            $table->string('name_of_trust', 30);
            $table->date('date_of_trust');
            $table->string('trustor_name', 30);
            $table->string('second_trustor_name', 20);
            $table->string('address', 40);
            $table->string('city', 20);
            $table->string('country', 20);
            $table->string('state', 20);
            $table->integer('zip_code');
            $table->string('telephone', 20);
            $table->string('child_1', 30);
            $table->string('child_2', 30);
            $table->string('child_3', 30);
            $table->string('first_successor_trustee', 50);
            $table->string('second_successor_trustee', 50);
            $table->string('trustor_1s_primary_beneficiary_1', 50);
            $table->string('trustor_1s_alternate_beneficiary_1', 50);
            $table->string('trustor_1s_primary_beneficiary_2', 50);
            $table->string('trustor_1s_alternate_beneficiary_2', 50);
            $table->string('trustor_1s_primary_beneficiary_3', 50);
            $table->string('trustor_1s_alternate_beneficiary_3', 50);
            $table->string('trustor_1s_primary_beneficiary_4', 50);
            $table->string('trustor_1s_alternate_beneficiary_4', 50);
            $table->string('trustor_1s_residual_beneficiary', 50);
            $table->string('trustor_1s_alternate_residuary_beneficiary', 50);
            //second trustess
            $table->string('trustor_2s_primary_beneficiary_1', 50);
            $table->string('trustor_2s_alternate_beneficiary_1', 50);
            $table->string('trustor_2s_primary_beneficiary_2', 50);
            $table->string('trustor_2s_alternate_beneficiary_2', 50);
            $table->string('trustor_2s_primary_beneficiary_3', 50);
            $table->string('trustor_2s_alternate_beneficiary_3', 50);
            $table->string('trustor_2s_primary_beneficiary_4', 50);
            $table->string('trustor_2s_alternate_beneficiary_4', 50);
            $table->string('trustor_2s_residual_beneficiary', 50);
            $table->string('trustor_2s_alternate_residuary_beneficiary', 50);

            $table->string('enter_none_for_no_beneficiary', 10);

            $table->string('child_1_beneficiary_for_child_trust', 50);
            $table->integer('age_of_child_1_beneficiary_when_child_trus_will_end')->unsigned();
            $table->string('child_2_beneficiary_for_child_trust', 50);
            $table->integer('age_of_child_2_beneficiary_when_child_trus_will_end')->unsigned();
            $table->string('child_3_beneficiary_for_child_trust', 50);
            $table->integer('age_of_child_3_beneficiary_when_child_trus_will_end')->unsigned();
            $table->string('child_4_beneficiary_for_child_trust', 50);
            $table->integer('age_of_child_4_beneficiary_when_child_trus_will_end')->unsigned();

            $table->string('child_1_beneficiary_for_utma', 30);
            $table->string('utma_custodian_for_child_1_beneficiary', 30);
            $table->integer('age_of_child_1_beneficiary_when_utma_custodianship_will_end');
            $table->string('child_2_beneficiary_for_utma', 30);
            $table->string('utma_custodian_for_child_2_beneficiary', 30);
            $table->integer('age_of_child_2_beneficiary_when_utma_custodianship_will_end');
            $table->string('child_3_beneficiary_for_utma', 30);
            $table->string('utma_custodian_for_child_3_beneficiary', 30);
            $table->integer('age_of_child_3_beneficiary_when_utma_custodianship_will_end');
            $table->string('child_4_beneficiary_for_utma', 30);
            $table->string('utma_custodian_for_child_4_beneficiary', 30);
            $table->integer('age_of_child_4_beneficiary_when_utma_custodianship_will_end');

            $table->string('person_1_to_determine_incapacity_of_trustors', 30);
            $table->string('person_2_to_determine_incapacity_of_trustors', 30);
            $table->string('person_3_to_determine_incapacity_of_trustors', 30);

            $table->timestamps();

            $table->foreign('project_id')
              ->references('id')->on('projects')
              ->onDelete('cascade');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::drop('plans');
    }
}
