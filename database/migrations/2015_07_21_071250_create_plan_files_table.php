<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreatePlanFilesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('plan_files', function (Blueprint $table) {
            $table->increments('id')->unsigned();
            $table->integer('plan_id')->unsigned();
            $table->string('file_name', 50);
            $table->text('content');
            $table->string('type', 10);
            $table->timestamps();

            $table->foreign('plan_id')
              ->references('id')->on('plans')
              ->onDelete('cascade');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::drop('plan_files');
    }
}
