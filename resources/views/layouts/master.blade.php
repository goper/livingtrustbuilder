<!doctype html>
<html lang="en">
<head>
<meta charset="UTF-8">
<meta name="viewport" content="width=device-width, initial-scale=1">
<title>Living Builders</title>
<link rel="shortcut icon" type="image/x-icon" href="{{ asset('img/logo_top.png') }}" />
<link rel="stylesheet" href="{{ asset('bootstrap/css/bootstrap.css')}}">
<link rel="stylesheet" href="{{ asset('bootstrap/css/bootstrap.css')}}">
<link rel="stylesheet" href="{{ asset('font-awesome/css/font-awesome.css') }}">
<link rel="stylesheet" href="{{ asset('css/common.css') }}">
<link rel="stylesheet" href="{{ asset('css/sidebar.css') }}">
<link rel="stylesheet" href="{{ asset('css/admin.css') }}">

<script src="{{ asset('js/jquery.js')}}"></script>
<script src="{{ asset('bootstrap/js/bootstrap.js')}}"></script>
<script src="{{ asset('js/admin.js')}}"></script>
</head>
<body>

<nav class="navbar navbar-default" style="background-color: #ffd777;">
  <div class="container-fluid">
    <div class="navbar-header">
      <a class="navbar-brand" href="#" style="color: #fffffa; font-size: 23pt;"><strong>Living Trust Builder</strong></a>
    </div>
    <div class="collapse navbar-collapse" id="bs-example-navbar-collapse-1">
      <ul class="nav navbar-nav margin-left-md">
        <li class="dropdown">
          <a href="#" class="dropdown-toggle" data-toggle="dropdown" role="button" aria-haspopup="true" aria-expanded="false">
            <span class="fa fa-list" style="font-size: 17pt;"></span>
          </a>
          <ul class="dropdown-menu">
            <li><a href="#">Action</a></li>
            <li><a href="#">Another action</a></li>
            <li><a href="#">Something else here</a></li>
            <li role="separator" class="divider"></li>
            <li><a href="#">Separated link</a></li>
            <li role="separator" class="divider"></li>
            <li><a href="#">One more separated link</a></li>
          </ul>
        </li>
      </ul>
      <ul class="nav navbar-nav">
        <li class="dropdown">
          <a href="#" class="dropdown-toggle" data-toggle="dropdown" role="button" aria-haspopup="true" aria-expanded="false">
            <span class="fa fa-envelope" style="font-size: 17pt;"></span>
          </a>
          <ul class="dropdown-menu">
            <li><a href="#">Action</a></li>
            <li><a href="#">Another action</a></li>
            <li><a href="#">Something else here</a></li>
            <li role="separator" class="divider"></li>
            <li><a href="#">Separated link</a></li>
            <li role="separator" class="divider"></li>
            <li><a href="#">One more separated link</a></li>
          </ul>
        </li>
      </ul>
      <div class="nav navbar-nav navbar-right">
      <!-- <ul style="list-style-type: none;" class="margin-top-md">
        <li style="margin-right: 30px;"><a href="#" class="btn btn-primary btn-md">Logout</a></li>
      </ul> -->
       <ul class="nav navbar-nav">
        <li class="dropdown">
            <a class="profileImageContainer" href="#" class="dropdown-toggle" data-toggle="dropdown" role="button" aria-haspopup="true" aria-expanded="false">
              <span>
                <span style="margin-right: 3px;">
                  Welcome {{ ucfirst(Auth::user()->name) }},
                </span>
                <img src="{{ asset('img/avatar/default.png') }}" class="img-circle" id="profileImage" width="37" height="37">
              </span>
            </a>
          <ul class="dropdown-menu">
            <li><a href="#"><span class="fa fa-gear margin-right-md"></span>Settings</a></li>
            <li role="separator" class="divider"></li>
            <li><a href="#"><span class="fa fa-user margin-right-md"></span>Profile</a></li>
            <li role="separator" class="divider"></li>
            <li><a href="/logout"><span class="fa fa-power-off margin-right-md"></span>Logout</a></li>
          </ul>
        </li>
      </ul>
    </div>
    </div><!-- /.navbar-collapse -->
    
  </div>
</nav>
 
<main>
  <div class="row">
@section('sidebar')

@show
    <div class="container">
      <div class="col-lg-10 col-md-10" style="border-left: 1px solid #b7bbb9">
        @yield('content')
      </div>
    </div>
</div>
</main>
</body>
</html>