<!doctype html>
<html lang="en">
<head>
<meta charset="UTF-8">
<meta name="viewport" content="width=device-width, initial-scale=1">
<title>Living Builders</title>
<link rel="shortcut icon" type="image/x-icon" href="{{ asset('img/logo_top.png') }}" />
<link rel="stylesheet" href="{{ asset('bootstrap/css/bootstrap.css')}}">
<link rel="stylesheet" href="{{ asset('font-awesome/css/font-awesome.css') }}">
<link rel="stylesheet" href="{{ asset('css/common.css') }}">
<link rel="stylesheet" href="{{ asset('css/sidebar.css') }}">
<link rel="stylesheet" href="{{ asset('css/admin.css') }}">

<script src="{{ asset('js/jquery.js')}}"></script>
<script src="{{ asset('bootstrap/js/bootstrap.js')}}"></script>
<script src="{{ asset('js/admin.js')}}"></script>
</head>
<body>
<div class="row">
<nav class="navbar navbar-default headerNav col-lg-12 col-md-12" style="background-color: #337ab7;">
  <div class="col-lg-4 col-md-4">
    <div class="container-fluid">
      <div class="navbar-header">
        <a class="navbar-brand headerNavbarText" href="#" style="color: #fffffa; font-size: 23pt;"><strong>Living Trust Builder</strong></a>
    </div>
    </div>
  </div>
  <div class="col-lg-6 col-md-6">
     <ul class="nav nav-pills navHeader text-center">
      <li role="presentation" class="navIcon active">
        <a href="/getting-started" style="{{ Request::is('getting-started') ? 'color: #337ab7;  background-color: #f9f9f9;' : 'color: #f9f9f9' }}">
          <i class="fa fa-home fa-2x" style=""></i>
          <br>
          Home
        </a>
      </li>
      <li role="presentation" class="navIcon">
        <a href="#" style="color: #f9f9f9;" id="navId">
          <i class="fa fa-file-text fa-2x" style=""></i>
          <br>
          File
        </a>
      </li>
      <li role="presentation" class="navIcon">
        <a href="/new-project" style="{{ Request::is('new-project') ? 'color: #337ab7;  background-color: #f9f9f9;' : 'color: #f9f9f9' }}" id="navId">
          <i class="fa fa-folder fa-2x"></i><br>
          Projects</a>
      </li>
      <li role="presentation" class="navIcon">
        <a href="#" style="color: #f9f9f9" id="navId">
          <i class="fa fa-gears fa-2x"></i><br>
          Settings</a>
      </li>
    </ul>
  </div>
  <div class="col-lg-2 col-md-2  pull-right">
    <ul class="nav nav-pills navHeader text-center" style="margin-left: 49%">
      <li role="presentation" class="navIcon active">
        <a href="/logout">
          <i class="fa fa-sign-out fa-2x" style=""></i>
          <br>
          Logout
        </a>
      </li>
    </ul>
  </div>
</nav>
 </div>
<main>

  <div class="row containerCustom">
    <aside>
    <div class="col-lg-3 col-md-3">
      @include('include.startSidebar')
    </div>
    </aside>
    <div class="col-lg-9 col-md-9">
      @yield('content')
    </div>
  </div>

</main>
</body>
</html>