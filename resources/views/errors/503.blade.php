<!DOCTYPE html>
<html>
    <head>
        <title>Be right back.</title>

        <link href="//fonts.googleapis.com/css?family=Lato:100" rel="stylesheet" type="text/css">
        <link rel="stylesheet" href="{{ asset('bootstrap/css/bootstrap.css')}}">
        <link rel="stylesheet" href="{{ asset('font-awesome/css/font-awesome.css') }}">

        <style>
            /* html, body {
                height: 100%;
            }
            
            body {
                margin: 0;
                padding: 0;
                width: 100%;
                color: blue;
                display: table;
                font-weight: 100;
                font-family: 'Lato';
            }
            
            .container {
                text-align: center;
                display: table-cell;
                vertical-align: middle;
            }
            
            .content {
                text-align: center;
                display: inline-block;
            }
            
            .title {
                font-size: 72px;
                margin-bottom: 40px;
            } */
        </style>
    </head>
    <body>
        <div class="container">
            <div class="content">
                <div class="row">
                    <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12 text-center">
                        <div class="jumbotron">
                          <h1>OPPS, Error 404 !</h1>
                          <p>@if(Session::has('error_message'))
                               {{ Session::get('error_message') }}
                           @endif
                           </p>
                          <p><a class="btn btn-primary btn-lg" href="{{ URL::previous() }}" role="button">
                              <i class="fa fa-arrow-left" style="margin-right: 8%;"></i>Go Back
                          </a></p>
                        </div>
                    </div>
                </div>
                    
                   
            </div>
        </div>
    </body>
</html>
