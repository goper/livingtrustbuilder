<!DOCTYPE html>
<html>
    <head class="Author-GoperLeoZosa">
        <title>Living Trust Builder</title>
         <link rel="shortcut icon" type="image/x-icon" href="{{ asset('img/logo_top.png') }}" />
<link rel="stylesheet" href="{{ asset('bootstrap/css/bootstrap.css')}}">
<link rel="stylesheet" href="{{ asset('bootstrap/css/bootstrap.css')}}">
<link rel="stylesheet" href="{{ asset('font-awesome/css/font-awesome.css') }}">
<link rel="stylesheet" href="{{ asset('css/common.css') }}">
<link rel="stylesheet" href="{{ asset('css/welcome.css') }}">
<script src="{{ asset('js/jquery.js')}}"></script>
<script src="{{ asset('bootstrap/js/bootstrap.js')}}"></script>
<script src="{{ asset('js/welcome.js') }}"></script>

        <style>
            body {
                background-image: url("{{ asset('img/bg.jpg') }}");
                background-size: content;
                background-repeat: no-repeat;
                background-position: center center; 
            }
        </style>
    </head>
    <body>
        <div class="container">
          <!-- Login -->
            <div class="row">
                <div class="col-lg-3 col-md-3"></div>
                <div class="col-lg-6 col-md-6" style="margin-top: 8%;">
                   <div class="loginContainer">
                     <div class="logoContainer text-center">
                       <div class="col-lg-4 col-md-4 loginIcon">
                        <a href="#" id="loginBtn">
                         <img src="{{ asset('img/login_icon.png') }}" height="70" width="70">
                        </a>
                       </div>
                       <div class="col-lg-4 col-md-4 registerIcon">
                        <a href="#" id="registerBtn">
                         <img src="{{ asset('img/register_icon.png') }}" height="70" width="70">
                        </a>
                       </div>
                       <div class="col-lg-4 col-md-4 forgotIcon">
                        <a href="#" id="resetBtn">
                         <img src="{{ asset('img/reset_icon.png') }}" height="70" width="70">
                        </a>
                       </div>
                     </div>
                     <div class="textContainer text-center col-lg-12 col-md-12">
                       <strong id="mainTitle">LOGIN</strong>
                     </div>
                     <div class="inputContainer col-lg-12 col-md-12"><br><br><br>
                      {!! Form::open(['class' => '', 'url' => '/login']) !!}
                      <div class="login">
                       <div class="form-group">
                          <div class="input-group">
                            <div class="input-group-addon" style="background-color: #337ab7">
                              <span class="fa fa-user fa-2x" style="color: #ffffff;"></span></div>
                            <input type="email" name="email" class="form-control" id="exampleInputAmount" placeholder="Email or Username"  style="height: 41px;font-size: 12pt; ">
                          </div>
                        </div><br>
                        <div class="form-group">
                          <div class="input-group">
                            <div class="input-group-addon" style="background-color: #337ab7">
                              <span class="fa fa-lock fa-2x" style="color: #ffffff;"></span></div>
                            <input type="password" name="password" class="form-control" id="exampleInputAmount" placeholder="Password" style="height: 41px;font-size: 12pt;">
                          </div>
                        </div>
                        <div class="text-right margin-top-md">
                          <a href="#" id="forgotPassword" style="color: #939498; font-size: 12pt;">Forgot Password?</a>
                        </div>
                        <br>
                        <div class="text-center margin-top-md">
                          <button type="submit" class="btn btn-default btn-lg" style="background-color: #337ab7; width: 103%; border-radius:0px;">
                              <span style="color: #ffffff;">SIGN IN</span>
                          </button>
                        </div>
                        <br>
                        <div class="text-center" style="color: #939498; font-size: 12pt;">
                          Dont have an account? 
                          <a href="#" id="signUpBtn">Sign Up</a>
                        </div>
                        <br><br>
                        </div>
                        {!! Form::close() !!}
                        <div class="registerContainer" style="margin-top: -6%;display: none;">
                          <div class="form-group">
                          <div class="input-group">
                            <div class="input-group-addon" style="background-color: #662c92">
                              <span class="fa fa-user" style="color: #ffffff;font-size: 17pt"></span></div>
                            <input type="text" name="email" class="form-control" id="exampleInputAmount" placeholder="Full Name"  style="height: 41px;font-size: 12pt; ">
                          </div>
                        </div>
                        <div class="form-group">
                          <div class="input-group">
                            <div class="input-group-addon" style="background-color: #662c92">
                              <span class="fa fa-lock" style="color: #ffffff;font-size: 21pt"></span></div>
                            <input type="password" name="password" class="form-control" id="exampleInputAmount" placeholder="Password" style="height: 41px;font-size: 12pt;">
                          </div>
                        </div>
                        <div class="form-group">
                          <div class="input-group">
                            <div class="input-group-addon" style="background-color: #662c92">
                              <span class="fa fa-lock" style="color: #ffffff;font-size: 21pt"></span></div>
                            <input type="password" name="password" class="form-control" id="exampleInputAmount" placeholder="Confirm Password" style="height: 41px;font-size: 12pt;">
                          </div>
                        </div>
                        <div class="form-group">
                          <div class="input-group">
                            <div class="input-group-addon" style="background-color: #662c92">
                              <span class="fa fa-envelope" style="color: #ffffff;font-size: 14pt"></span></div>
                            <input type="email" name="password" class="form-control" id="exampleInputAmount" placeholder="Email Address" style="height: 41px;font-size: 12pt;">
                          </div>
                        </div>
                        <div>
                          <div class="checkbox myChkbx">
                            <input type="checkbox" style="margin-left: 0%;">
                            <label style="margin-left: 0%; margin-top: 1%">
                              I Agree to <span style="color: #00abed">Terms and Conditions</span>
                            </label>
                          </div>
                        </div>
                        <div class="text-center margin-top-md">
                          <button type="submit" class="btn btn-default btn-lg" style="background-color: #662c92; width: 103%; border-radius:0px;">
                              <span style="color: #ffffff;">SIGN IN</span>
                          </button>
                        </div><br>
                        </div>

                     </div>
                     <div class="footerContainer col-lg-12 col-md-12">
                       
                     </div>
                   </div>
                </div><!-- end -->
                <div class="col-lg-3 col-md-3"></div>
            </div>
        </div>
    </body>
</html>
