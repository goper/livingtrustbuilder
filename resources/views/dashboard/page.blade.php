@extends('layouts.dashboardLayout')

@section('content')
<div class="pageContainer">
  <h1 class="customHeader text-center text-uppercase">
    <strong>
    {{ ucfirst(str_replace("_", " ", $pageName)) }}
    </strong>
  </h1>
  @if(Session::has('flash_message'))
    <div class="alert alert-success">
        {{ Session::get('flash_message') }}
    </div>
  @endif
<br>
{!! Form::open(['class' => 'form-inline', 'url' => '/edit-file']) !!}
    <textarea name="editor1" id="editor1" rows="10" cols="80">
      {{ $page }}
    </textarea>
    <script>
        // Replace the <textarea id="editor1"> with a CKEditor
        // instance, using default configuration.
        CKEDITOR.replace( 'editor1' );
    </script>
    <input type="hidden" name="planId" value="{{ $planId }}">

    <input type="hidden" name="projectName" value="{{ str_replace(" ", "-", $projectData->name) }}">
    <input type="hidden" name="pageName" value="{{ $pageName }}">

    <button type="submit" class="btn btn-primary btn-lg pull-right" style="margin-top: 2%">
      <i class="fa fa-save margin-right-sm"></i>
      Save
    </button>
{!! Form::close() !!}
{!! Form::open(['class' => 'form-inline', 'url' => '/download']) !!}
<input type="hidden" name="planId" value="{{ $planId }}">
<input type="hidden" name="projectId" value="{{ $projectData->id }}">
<input type="hidden" name="projectName" value="{{ str_replace(" ", "-", $projectData->name) }}">
<input type="hidden" name="pageName" value="{{ $pageName }}">
<button type="submit" class="btn btn-primary btn-lg pull-right margin-right-md" style="margin-top: 2%">
  <i class="fa fa-cloud-download margin-right-sm"></i>
  Download
</button>
{!! Form::close() !!}
</div>

@stop