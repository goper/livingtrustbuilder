@extends('layouts.dashboardLayout')

@section('content')
<div class="col-lg-12 col-md-12 text-center">
  <h1 class="pull-left customHeader text-capitalize" style="text-align:center">{{ ucfirst(str_replace("_", " ", $pageName)) }}</h1>
</div> 
@if(Session::has('flash_message'))
  <div class="alert alert-success">
      {{ Session::get('flash_message') }}
  </div>
@endif
@if(Session::has('error_message'))
  <div class="alert alert-danger">
      {{ Session::get('error_message') }}
  </div>
@endif

<div class="navSubPage" style="margin-left: 1%;">
<ul class="nav nav-pills">
  <li role="presentation" class="{{ Request::is('dashboard/' . str_replace(" ", "-", $projectData->name) . '/accounts_and_contracts/sub-intro') ? 'active' : '' }}">
    <a href="/dashboard/{{ str_replace(" ", "-", $projectData->name) }}/{{ $pageName }}/sub-intro">Intro</a>
  </li>
  <li role="presentation" class="{{ Request::is('dashboard/' . str_replace(" ", "-", $projectData->name) . '/accounts_and_contracts/sub-contacts') ? 'active' : '' }}">
    <a href="/dashboard/{{ str_replace(" ", "-", $projectData->name) }}/{{ $pageName }}/sub-contacts">Contacts</a>
  </li>
  <li role="presentation" class="{{ Request::is('dashboard/' . str_replace(" ", "-", $projectData->name) . '/accounts_and_contracts/sub-accounts') ? 'active' : '' }}">
    <a href="/dashboard/{{ str_replace(" ", "-", $projectData->name) }}/{{ $pageName }}/sub-accounts">Accounts</a>
  </li>
  <li role="presentation" class="{{ Request::is('dashboard/' . str_replace(" ", "-", $projectData->name) . '/accounts_and_contracts/sub-agreements') ? 'active' : '' }}">
    <a href="/dashboard/{{ str_replace(" ", "-", $projectData->name) }}/{{ $pageName }}/sub-agreements">Agreements</a>
  </li>
  <li role="presentation" class="{{ Request::is('dashboard/' . str_replace(" ", "-", $projectData->name) . '/accounts_and_contracts/sub-affiliates') ? 'active' : '' }}">
    <a href="/dashboard/{{ str_replace(" ", "-", $projectData->name) }}/{{ $pageName }}/sub-affiliates">Affiliates</a>
  </li>
  <li role="presentation" class="{{ Request::is('dashboard/' . str_replace(" ", "-", $projectData->name) . '/accounts_and_contracts/sub-funeral_instructions') ? 'active' : '' }}">
    <a href="/dashboard/{{ str_replace(" ", "-", $projectData->name) }}/{{ $pageName }}/sub-funeral_instructions">Funeral Instruction</a>
  </li>
</ul>
</div>
<div class="accountContainer col-lg-12 col-md-12 margin-top-sm" style="" >
  {!! Form::open(['class' => 'form-inline', 'url' => '/edit-accounts']) !!}
    <textarea name="content" id="editor1" rows="10" cols="80">
      {{ $content }}
    </textarea>
    <script>
        // Replace the <textarea id="editor1"> with a CKEditor
        // instance, using default configuration.
        CKEDITOR.replace( 'editor1' );
    </script>
    <input type="hidden" name="planId" value="{{ $planId }}">
    
    <input type="hidden" name="projectName" value="{{ str_replace(" ", "-", $projectData->name) }}">
    <input type="hidden" name="pageName" value="{{ $pageName }}">
    <input type="hidden" name="subPageName" value="{{ $subPage }}">

    <button type="submit" class="btn btn-primary btn-lg pull-right" style="margin-top: 2%">
      <i class="fa fa-save margin-right-sm"></i>
      Save
    </button>
{!! Form::close() !!}
{!! Form::open(['class' => 'form-inline', 'url' => '/download']) !!}
<textarea class="hidden" name="downloadContent">
  {{ $content }}
</textarea>
<input type="hidden" name="planId" value="{{ $planId }}">
<input type="hidden" name="projectId" value="{{ $projectData->id }}">
<input type="hidden" name="projectName" value="{{ str_replace(" ", "-", $projectData->name) }}">
    <input type="hidden" name="pageName" value="{{ $pageName }}">
    <input type="hidden" name="subPageName" value="{{ $subPage }}">
<button type="submit" class="btn btn-primary btn-lg pull-right margin-right-md" style="margin-top: 2%">
      <i class="fa fa-cloud-download margin-right-sm"></i>
      Download
    </button>
{!! Form::close() !!}
</div>
@stop