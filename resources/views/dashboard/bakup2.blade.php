 {!! Form::open(['class' => '', 'url' => '/save-net-estate']) !!}
  <input type="hidden" name="planId" value="{{ $planId }}">
  <input type="hidden" name="projectName" value="{{ str_replace(" ", "-", $projectData->name) }}">
  
  <div class="calculationContainer">
    <div class="col-lg-12 col-md-12">
      <div class="col-lg-8 col-md-8">
        <h4 style="margin-left: -4%"><strong>Exhibit A - Trustor's Property List</strong></h4>
      </div>
      <div class="col-lg-4 col-md-4">
        <button class="btn btn-md btn-primary  pull-right" type="submit">
          <i class="fa fa-upload"></i>
          Calculate / Save
        </button>
      </div>
    </div>
    <div class="col-lg-12 col-md-12 asd">
      <table class="table table-striped" border="0" id="bankAccountsTable" id="myTable">
      <tr class="tableHeader" style="background-color: #4A65CF;">
        <th colspan="8"><strong>Bank Accounts</strong></th>
      </tr>
        <tr>
          <td width="190"></td>
          <td width=""></td>
          <td width=""></td>
          <td width=""></td>
          <td colspan="3" class="text-center">Allocation of Estate</td>
          <td width=""></td>
        </tr>
        <tr class="text-center" style="font-weight: bold">
          <td>Financial Institution</td>
          <td style="font-size: 8pt" width="110">Type of Account</td>
          <td width="130">Value</td>
          <td width="152">Property of</td>
          <td width="70">Child 1</td>
          <td width="70">Child 2</td>
          <td width="70">Child 3</td>
          <td >100%</td>
        </tr>
        <tr>
          <td><input name="baFinancialInstitution1" type="text" value="" id="baBankName" class="form-control input-sm" style="width: 100%"></td>
          <td>
            <input name="baTypeOfAccount1" type="text" id="" class="form-control input-sm">
          </td>
          <td><input name="baValue1" type="number" value="" id="baValue" class="form-control input-sm" style="width: 100%"></td>
          <td><select name="baPropertyOf1" id="baPropertyOf" class="form-control input-sm">
            <option value="H">Husband/Single</option>
            <option value="W">Wife</option>
            <option value="J">Joinly Held</option>
          </select></td>
          <td><input name="baChild11" min="0" max="100" type="number" value="" id="baChild1" class="form-control input-sm" style="width: 100%"></td>
          <td><input name="baChild21" min="0" max="100" type="number" value="" id="baChild2" class="form-control input-sm" style="width: 100%"></td>
          <td><input name="baChild31" min="0" max="100" type="number" value="" id="baChild3" class="form-control input-sm" style="width: 100%"></td>
          <td><input name="baChildTotal1" type="number" value="" id="baChildTotal" disabled class="form-control input-sm" style="width: 100%"></td>
        </tr>
        <tr>
          <td><input name="baFinancialInstitution2" type="text" value="" id="baBankName" class="form-control input-sm" style="width: 100%"></td>
          <td>
            <input name="baTypeOfAccount2" type="text" id="" class="form-control input-sm">
          </td>
          <td><input name="baValue2" type="number" value="" id="baValue" class="form-control input-sm" style="width: 100%"></td>
          <td><select name="baPropertyOf2" id="baPropertyOf" class="form-control input-sm">
            <option value="H">Husband/Single</option>
            <option value="W">Wife</option>
            <option value="J">Joinly Held</option>
          </select></td>
          <td><input name="baChild12" type="number" value="" id="baChild1" class="form-control input-sm" style="width: 100%"></td>
          <td><input name="baChild22" type="number" value="" id="baChild2" class="form-control input-sm" style="width: 100%"></td>
          <td><input name="baChild32" type="number" value="" id="baChild3" class="form-control input-sm" style="width: 100%"></td>
          <td><input name="baChildTotal2" type="number" value="" disabled id="baChildTotal" class="form-control input-sm" style="width: 100%"></td>
        </tr>
        <tr>
          <td><input name="baFinancialInstitution3" type="text" value="" id="baBankName" class="form-control input-sm" style="width: 100%"></td>
          <td>
            <input name="baTypeOfAccount3" type="text" id="" class="form-control input-sm">
          </td>
          <td><input name="baValue3" type="number" value="" id="baValue" class="form-control input-sm" style="width: 100%"></td>
          <td><select name="baPropertyOf3" id="baPropertyOf" class="form-control input-sm">
            <option value="H">Husband/Single</option>
            <option value="W">Wife</option>
            <option value="J">Joinly Held</option>
          </select></td>
          <td><input name="baChild13" type="number" value="" id="baChild1" class="form-control input-sm" style="width: 100%"></td>
          <td><input name="baChild23" type="number" value="" id="baChild2" class="form-control input-sm" style="width: 100%"></td>
          <td><input name="baChild33" type="number" value="" id="baChild3" class="form-control input-sm" style="width: 100%"></td>
          <td><input name="baChildTotal3" type="number" value="" disabled id="baChildTotal" class="form-control input-sm" style="width: 100%"></td>
        </tr>
        <!-- <tr id="addRow">
          <td><a href="#" id="addRowBankAccount">
            Add Field
            <i class="fa fa-plus-circle"></i>
          </a></td>
        </tr> -->
      </table>
      <!-- Cash Accounts -->     
      <table class="table table-striped" border="0" id="bankAccountsTable">
      <tr style="background-color: #4A65CF;">
        <th colspan="8"><strong>Cash Accounts</strong><span><small> (Money, Market, CD's, Other)</small></span></th>
      </tr>
        <tr>
          <td width="190"></td>
          <td width=""></td>
          <td width=""></td>
          <td width=""></td>
          <td colspan="3" class="text-center">Allocation of Estate</td>
          <td width=""></td>
        </tr>
        <tr class="text-center" style="font-weight: bold">
          <td>Financial Institution</td>
          <td style="font-size: 8pt" width="130">Type of Account</td>
          <td>Value</td>
          <td width="152">Property of</td>
          <td width="70">Child 1</td>
          <td width="70">Child 2</td>
          <td width="70">Child 3</td>
          <td width="">100%</td>
        </tr>
        <tr>
          <td><input name="caFinancialInstitution1" type="text" value="" id="caFinancialInstitution" class="form-control input-sm" style="width: 100%"></td>
          <td>
            <input name="caTypeOfAccount1" type="text" id="caTypeOfAccount" value="" class="form-control input-sm">
          </td>
          <td><input name="caValue1" type="text" value="" id="bankName" class="form-control input-sm" style="width: 100%"></td>
          <td><select name="caPropertyOf1" class="form-control input-sm">
            <option>Husband/Single</option>
            <option>Wife</option>
            <option>Joinly Held</option>
          </select></td>
          <td><input name-"caChild11" type="text" value="" id="bankName" class="form-control input-sm" style="width: 100%"></td>
          <td><input  name-"caChild21" type="text" value="" id="bankName" class="form-control input-sm" style="width: 100%"></td>
          <td><input  name-"caChild31" type="text" value="" id="bankName" class="form-control input-sm" style="width: 100%"></td>
          <td><input  name-"caChildTotal1" type="text" value="" id="bankName" class="form-control input-sm" style="width: 100%"></td>
        </tr>
        <tr>
          <td><input name="caFinancialInstitution2" type="text" value="" id="caFinancialInstitution" class="form-control input-sm" style="width: 100%"></td>
          <td>
            <input name="caTypeOfAccount2" type="text" id="caTypeOfAccount" value="" class="form-control input-sm">
          </td>
          <td><input name="caValue2" type="text" value="" id="bankName" class="form-control input-sm" style="width: 100%"></td>
          <td><select name="caPropertyOf2" class="form-control input-sm">
            <option>Husband/Single</option>
            <option>Wife</option>
            <option>Joinly Held</option>
          </select></td>
          <td><input name-"caChild12" type="text" value="" id="bankName" class="form-control input-sm" style="width: 100%"></td>
          <td><input  name-"caChild22" type="text" value="" id="bankName" class="form-control input-sm" style="width: 100%"></td>
          <td><input  name-"caChild32" type="text" value="" id="bankName" class="form-control input-sm" style="width: 100%"></td>
          <td><input  name-"caChildTotal2" type="text" value="" id="bankName" class="form-control input-sm" style="width: 100%"></td>
        </tr>
        <tr>
          <td><input name="caFinancialInstitution3" type="text" value="" id="caFinancialInstitution" class="form-control input-sm" style="width: 100%"></td>
          <td>
            <input name="caTypeOfAccount3" type="text" id="caTypeOfAccount" value="" class="form-control input-sm">
          </td>
          <td><input name="caValue3" type="text" value="" id="bankName" class="form-control input-sm" style="width: 100%"></td>
          <td><select name="caPropertyOf3" class="form-control input-sm">
            <option>Husband/Single</option>
            <option>Wife</option>
            <option>Joinly Held</option>
          </select></td>
          <td><input name-"caChild13" type="text" value="" id="bankName" class="form-control input-sm" style="width: 100%"></td>
          <td><input  name-"caChild23" type="text" value="" id="bankName" class="form-control input-sm" style="width: 100%"></td>
          <td><input  name-"caChild33" type="text" value="" id="bankName" class="form-control input-sm" style="width: 100%"></td>
          <td><input  name-"caChildTotal3" type="text" value="" id="bankName" class="form-control input-sm" style="width: 100%"></td>
        </tr>

        
      </table>
      <!-- Investments -->
      <table class="table table-striped" border="0" id="bankAccountsTable">
      <tr style="background-color: #4A65CF;">
        <th colspan="8"><strong>Investments</strong><span><small> (Stocks, Bonds, Mutual Funds, Other)</small></span></th>
      </tr>
        <tr>
          <td width="190"></td>
          <td width=""></td>
          <td width=""></td>
          <td width=""></td>
          <td colspan="3" class="text-center">Allocation of Estate</td>
          <td width=""></td>
        </tr>
        <tr class="text-center" style="font-weight: bold">
          <td>Description Investments</td>
          <td style="font-size: 8pt" width="130"># of Shares/Units</td>
          <td>Value</td>
          <td width="152">Property of</td>
          <td width="70">Child 1</td>
          <td width="70">Child 2</td>
          <td width="70">Child 3</td>
          <td width="">100%</td>
        </tr>
        <tr>
          <td><input name="iDescription1" type="text" value="" id="bankName" class="form-control input-sm" style="width: 100%"></td>
          <td>
            <input name="iNoOfSharedUnits1"  type="text" name="" value="" class="form-control input-sm">
          </td>
          <td><input  name="iValue1" type="text" value="" id="bankName" class="form-control input-sm" style="width: 100%"></td>
          <td><select name="iPropertyOf1" class="form-control input-sm">
            <option>Husband/Single</option>
            <option>Wife</option>
            <option>Joinly Held</option>
          </select></td>
          <td><input  name="iChild11" type="text" value="" id="bankName" class="form-control input-sm" style="width: 100%"></td>
          <td><input  name="iChild21" type="text" value="" id="bankName" class="form-control input-sm" style="width: 100%"></td>
          <td><input  name="iChild31" type="text" value="" id="bankName" class="form-control input-sm" style="width: 100%"></td>
          <td><input  name="iChildTotal1" type="text" value="" id="bankName" class="form-control input-sm" style="width: 100%"></td>
        </tr>
        <tr>
          <td><input name="iDescription2" type="text" value="" id="bankName" class="form-control input-sm" style="width: 100%"></td>
          <td>
            <input name="iNoOfSharedUnits2"  type="text" name="" value="" class="form-control input-sm">
          </td>
          <td><input  name="iValue2" type="text" value="" id="bankName" class="form-control input-sm" style="width: 100%"></td>
          <td><select name="iPropertyOf2" class="form-control input-sm">
            <option>Husband/Single</option>
            <option>Wife</option>
            <option>Joinly Held</option>
          </select></td>
          <td><input  name="iChild12" type="text" value="" id="bankName" class="form-control input-sm" style="width: 100%"></td>
          <td><input  name="iChild22" type="text" value="" id="bankName" class="form-control input-sm" style="width: 100%"></td>
          <td><input  name="iChild32" type="text" value="" id="bankName" class="form-control input-sm" style="width: 100%"></td>
          <td><input  name="iChildTotal2" type="text" value="" id="bankName" class="form-control input-sm" style="width: 100%"></td>
        </tr>
        <tr>
          <td><input name="iDescription3" type="text" value="" id="bankName" class="form-control input-sm" style="width: 100%"></td>
          <td>
            <input name="iNoOfSharedUnits3"  type="text" name="" value="" class="form-control input-sm">
          </td>
          <td><input  name="iValue3" type="text" value="" id="bankName" class="form-control input-sm" style="width: 100%"></td>
          <td><select name="iPropertyOf3" class="form-control input-sm">
            <option>Husband/Single</option>
            <option>Wife</option>
            <option>Joinly Held</option>
          </select></td>
          <td><input  name="iChild13" type="text" value="" id="bankName" class="form-control input-sm" style="width: 100%"></td>
          <td><input  name="iChild23" type="text" value="" id="bankName" class="form-control input-sm" style="width: 100%"></td>
          <td><input  name="iChild33" type="text" value="" id="bankName" class="form-control input-sm" style="width: 100%"></td>
          <td><input  name="iChildTotal3" type="text" value="" id="bankName" class="form-control input-sm" style="width: 100%"></td>
        </tr>
        
      </table>

      <!-- Business Interests -->
      <table class="table table-striped" border="0" id="bankAccountsTable">
      <tr style="background-color: #4A65CF;">
        <th colspan="8"><strong>Business Interests</strong><span><small> (Stocks, Bonds, Mutual Funds, Other)</small></span></th>
      </tr>
        <tr>
          <td width="190"></td>
          <td width=""></td>
          <td width=""></td>
          <td width=""></td>
          <td colspan="3" class="text-center">Allocation of Estate</td>
          <td width=""></td>
        </tr>
        <tr class="text-center" style="font-weight: bold">
          <td>Name of Business</td>
          <td style="font-size: 8pt" width="130">Type of Interest</td>
          <td>Value</td>
          <td width="152">Property of</td>
          <td width="70">Child 1</td>
          <td width="70">Child 2</td>
          <td width="70">Child 3</td>
          <td width="">100%</td>
        </tr>
        <tr>
          <td><input name="biNameOfBusiness1" type="text" value="" id="bankName" class="form-control input-sm" style="width: 100%"></td>
          <td>
            <input name="biTypeOfInterest1" type="text" value="" class="form-control input-sm">
          </td>
          <td><input name="biValue1" type="text" value="" id="bankName" class="form-control input-sm" style="width: 100%"></td>
          <td><select  name="biPropertyOf1" class="form-control input-sm">
            <option>Husband/Single</option>
            <option>Wife</option>
            <option>Joinly Held</option>
          </select></td>
          <td><input  name="biChild11" type="text" value="" id="bankName" class="form-control input-sm" style="width: 100%"></td>
          <td><input  name="biChild21" type="text" value="" id="bankName" class="form-control input-sm" style="width: 100%"></td>
          <td><input  name="biChild31" type="text" value="" id="bankName" class="form-control input-sm" style="width: 100%"></td>
          <td><input  name="biChildTotal1" type="text" value="" id="bankName" class="form-control input-sm" style="width: 100%"></td>
        </tr>
        <tr>
          <td><input name="biNameOfBusiness2" type="text" value="" id="bankName" class="form-control input-sm" style="width: 100%"></td>
          <td>
            <input name="biTypeOfInterest2" type="text" value="" class="form-control input-sm">
          </td>
          <td><input name="biValue2" type="text" value="" id="bankName" class="form-control input-sm" style="width: 100%"></td>
          <td><select  name="biPropertyOf2" class="form-control input-sm">
            <option>Husband/Single</option>
            <option>Wife</option>
            <option>Joinly Held</option>
          </select></td>
          <td><input  name="biChild12" type="text" value="" id="bankName" class="form-control input-sm" style="width: 100%"></td>
          <td><input  name="biChild22" type="text" value="" id="bankName" class="form-control input-sm" style="width: 100%"></td>
          <td><input  name="biChild32" type="text" value="" id="bankName" class="form-control input-sm" style="width: 100%"></td>
          <td><input  name="biChildTotal2" type="text" value="" id="bankName" class="form-control input-sm" style="width: 100%"></td>
        </tr>
        <tr>
          <td><input name="biNameOfBusiness3" type="text" value="" id="bankName" class="form-control input-sm" style="width: 100%"></td>
          <td>
            <input name="biTypeOfInterest3" type="text" value="" class="form-control input-sm">
          </td>
          <td><input name="biValue3" type="text" value="" id="bankName" class="form-control input-sm" style="width: 100%"></td>
          <td><select  name="biPropertyOf3" class="form-control input-sm">
            <option>Husband/Single</option>
            <option>Wife</option>
            <option>Joinly Held</option>
          </select></td>
          <td><input  name="biChild13" type="text" value="" id="bankName" class="form-control input-sm" style="width: 100%"></td>
          <td><input  name="biChild23" type="text" value="" id="bankName" class="form-control input-sm" style="width: 100%"></td>
          <td><input  name="biChild33" type="text" value="" id="bankName" class="form-control input-sm" style="width: 100%"></td>
          <td><input  name="biChildTotal3" type="text" value="" id="bankName" class="form-control input-sm" style="width: 100%"></td>
        </tr>
        
        
      </table>

      <!-- Real Property -->
      <table class="table table-striped" border="0" id="bankAccountsTable">
      <tr style="background-color: #4A65CF;">
        <th colspan="8"><strong>Real Property</strong><span><small> (Home, Vacation Home, etc. )</small></span></th>
      </tr>
        <tr>
          <td width="190"></td>
          <td width=""></td>
          <td width=""></td>
          <td width=""></td>
          <td colspan="3" class="text-center">Allocation of Estate</td>
          <td width=""></td>
        </tr>
        <tr class="text-center" style="font-weight: bold">
          <td>Location</td>
          <td style="font-size: 8pt" width="100">Market Value</td>
          <td width="100">Mortgage</td>
          <td width="132">Net Value</td>
          <td >Property of</td>
          <td width="70">Child 1</td>
          <td width="70">Child 2</td>
          <td width="70">Child 3</td>
          <td width="70">100%</td>
        </tr>
        <tr>
          <td><input name="rpLocation1" type="text" value="" id="bankName" class="form-control input-sm" style="width: 100%"></td>
          <td>
            <input  name="rpMarketValue1" type="text" value="" class="form-control input-sm">
          </td>
          <td><input  name="rpMortgage1" type="text" value="" id="bankName" class="form-control input-sm" style="width: 100%"></td>
          <td><input  name="rpNetValue1" type="text" value="" id="bankName" class="form-control input-sm" style="width: 100%"></td>
          <td><select  name="rpPropertyOf1" class="form-control input-sm">
            <option>Husband/Single</option>
            <option>Wife</option>
            <option>Joinly Held</option>
          </select></td>
          <td><input  name="rpChild11" type="text" value="" id="bankName" class="form-control input-sm" style="width: 100%"></td>
          
          <td><input  name="rpChild21" type="text" value="" id="bankName" class="form-control input-sm" style="width: 100%"></td>
          <td><input  name="rpChild31" type="text" value="" id="bankName" class="form-control input-sm" style="width: 100%"></td>
          <td><input  name="rpChildTotal1" type="text" value="" id="bankName" class="form-control input-sm" style="width: 100%"></td>
        </tr>
        <tr>
          <td><input name="rpLocation2" type="text" value="" id="bankName" class="form-control input-sm" style="width: 100%"></td>
          <td>
            <input  name="rpMarketValue2" type="text" value="" class="form-control input-sm">
          </td>
          <td><input  name="rpMortgage2" type="text" value="" id="bankName" class="form-control input-sm" style="width: 100%"></td>
          <td><input  name="rpNetValue2" type="text" value="" id="bankName" class="form-control input-sm" style="width: 100%"></td>
          <td><select  name="rpPropertyOf2" class="form-control input-sm">
            <option>Husband/Single</option>
            <option>Wife</option>
            <option>Joinly Held</option>
          </select></td>
          <td><input  name="rpChild12" type="text" value="" id="bankName" class="form-control input-sm" style="width: 100%"></td>
          
          <td><input  name="rpChild22" type="text" value="" id="bankName" class="form-control input-sm" style="width: 100%"></td>
          <td><input  name="rpChild32" type="text" value="" id="bankName" class="form-control input-sm" style="width: 100%"></td>
          <td><input  name="rpChildTotal2" type="text" value="" id="bankName" class="form-control input-sm" style="width: 100%"></td>
        </tr>
        <tr>
          <td><input name="rpLocation3" type="text" value="" id="bankName" class="form-control input-sm" style="width: 100%"></td>
          <td>
            <input  name="rpMarketValue3" type="text" value="" class="form-control input-sm">
          </td>
          <td><input  name="rpMortgage3" type="text" value="" id="bankName" class="form-control input-sm" style="width: 100%"></td>
          <td><input  name="rpNetValue3" type="text" value="" id="bankName" class="form-control input-sm" style="width: 100%"></td>
          <td><select  name="rpPropertyOf3" class="form-control input-sm">
            <option>Husband/Single</option>
            <option>Wife</option>
            <option>Joinly Held</option>
          </select></td>
          <td><input  name="rpChild13" type="text" value="" id="bankName" class="form-control input-sm" style="width: 100%"></td>
          
          <td><input  name="rpChild23" type="text" value="" id="bankName" class="form-control input-sm" style="width: 100%"></td>
          <td><input  name="rpChild33" type="text" value="" id="bankName" class="form-control input-sm" style="width: 100%"></td>
          <td><input  name="rpChildTotal3" type="text" value="" id="bankName" class="form-control input-sm" style="width: 100%"></td>
        </tr>
        
      </table>

      <!-- Retired Accounts -->
      <table class="table table-striped" border="0" id="bankAccountsTable">
      <tr style="background-color: #4A65CF;">
        <th colspan="8"><strong>Retirement Accounts</strong><span>
          <small> (401K's, IRA's, Profit-Sharing Plans, Other)</small>
        </span></th>
      </tr>
        <tr>
          <td width="190"></td>
          <td width=""></td>
          <td width=""></td>
          <td width=""></td>
          <td colspan="3" class="text-center">Allocation of Estate</td>
          <td width=""></td>
        </tr>
        <tr class="text-center" style="font-weight: bold">
          <td>Financial Institution</td>
          <td style="font-size: 8pt" width="130">Type of Account</td>
          <td>Value</td>
          <td width="152">Property of</td>
          <td width="70">Child 1</td>
          <td width="70">Child 2</td>
          <td width="70">Child 3</td>
          <td width="">100%</td>
        </tr>
        <tr>
          <td><input name="raFinancialInstitution1" type="text" value="" id="bankName" class="form-control input-sm" style="width: 100%"></td>
          <td>
            <input name="raTypeOfAccount1"  type="text" value="" class="form-control input-sm">
          </td>
          <td><input  name="raValue1" type="text" value="" id="bankName" class="form-control input-sm" style="width: 100%"></td>
          <td><select  name="raPropertyOf1" class="form-control input-sm">
            <option>Husband/Single</option>
            <option>Wife</option>
            <option>Joinly Held</option>
          </select></td>
          <td><input  name="raChild11" type="text" value="" id="bankName" class="form-control input-sm" style="width: 100%"></td>
          <td><input name="raChild21" type="text" value="" id="bankName" class="form-control input-sm" style="width: 100%"></td>
          <td><input name="raChild31" type="text" value="" id="bankName" class="form-control input-sm" style="width: 100%"></td>
          <td><input name="raChildTotal1" type="text" value="" id="bankName" class="form-control input-sm" style="width: 100%"></td>
        </tr>
        <tr>
          <td><input name="raFinancialInstitution2" type="text" value="" id="bankName" class="form-control input-sm" style="width: 100%"></td>
          <td>
            <input name="raTypeOfAccount2"  type="text" value="" class="form-control input-sm">
          </td>
          <td><input  name="raValue2" type="text" value="" id="bankName" class="form-control input-sm" style="width: 100%"></td>
          <td><select  name="raPropertyOf2" class="form-control input-sm">
            <option>Husband/Single</option>
            <option>Wife</option>
            <option>Joinly Held</option>
          </select></td>
          <td><input  name="raChild12" type="text" value="" id="bankName" class="form-control input-sm" style="width: 100%"></td>
          <td><input name="raChild22" type="text" value="" id="bankName" class="form-control input-sm" style="width: 100%"></td>
          <td><input name="raChild32" type="text" value="" id="bankName" class="form-control input-sm" style="width: 100%"></td>
          <td><input name="raChildTotal2" type="text" value="" id="bankName" class="form-control input-sm" style="width: 100%"></td>
        </tr>
        <tr>
          <td><input name="raFinancialInstitution3" type="text" value="" id="bankName" class="form-control input-sm" style="width: 100%"></td>
          <td>
            <input name="raTypeOfAccount3"  type="text" value="" class="form-control input-sm">
          </td>
          <td><input  name="raValue3" type="text" value="" id="bankName" class="form-control input-sm" style="width: 100%"></td>
          <td><select  name="raPropertyOf3" class="form-control input-sm">
            <option>Husband/Single</option>
            <option>Wife</option>
            <option>Joinly Held</option>
          </select></td>
          <td><input  name="raChild13" type="text" value="" id="bankName" class="form-control input-sm" style="width: 100%"></td>
          <td><input name="raChild23" type="text" value="" id="bankName" class="form-control input-sm" style="width: 100%"></td>
          <td><input name="raChild33" type="text" value="" id="bankName" class="form-control input-sm" style="width: 100%"></td>
          <td><input name="raChildTotal3" type="text" value="" id="bankName" class="form-control input-sm" style="width: 100%"></td>
        </tr>
        
      </table>
      
      <!-- Collectibles -->
      <table class="table table-striped" border="0" id="bankAccountsTable">
      <tr style="background-color: #4A65CF;">
        <th colspan="8"><strong>Collectibles</strong><span>
          <small> (Fine Art, Coins, Other)</small>
        </span></th>
      </tr>
        <tr>
          <td width="190"></td>
          <td width=""></td>
          <td width=""></td>
          <td width=""></td>
          <td colspan="3" class="text-center">Allocation of Estate</td>
          <td width=""></td>
        </tr>
        <tr class="text-center" style="font-weight: bold">
          <td>Item</td>
          
          <td>Value</td>
          <td width="200">Property of</td>
          <td width="70">Child 1</td>
          <td width="70">Child 2</td>
          <td width="70">Child 3</td>
          <td width="">100%</td>
        </tr>
        <tr>
          <td><input name="cItem1" type="text" value="" id="bankName" class="form-control input-sm" style="width: 100%"></td>
          
          <td><input name="cValue1" type="text" value="" id="bankName" class="form-control input-sm" style="width: 100%"></td>
          <td><select  name="cPropertyOf1" class="form-control input-sm">
            <option>Husband/Single</option>
            <option>Wife</option>
            <option>Joinly Held</option>
          </select></td>
          <td><input  name="cChild11" type="text" value="" id="bankName" class="form-control input-sm" style="width: 100%"></td>
          <td><input  name="cChild21" type="text" value="" id="bankName" class="form-control input-sm" style="width: 100%"></td>
          <td><input name="cChild31" type="text" value="" id="bankName" class="form-control input-sm" style="width: 100%"></td>
          <td><input name="cChildTotal1" type="text" value="" id="bankName" class="form-control input-sm" style="width: 100%"></td>
        </tr>
        <tr>
          <td><input name="cItem2" type="text" value="" id="bankName" class="form-control input-sm" style="width: 100%"></td>
          
          <td><input name="cValue2" type="text" value="" id="bankName" class="form-control input-sm" style="width: 100%"></td>
          <td><select  name="cPropertyOf2" class="form-control input-sm">
            <option>Husband/Single</option>
            <option>Wife</option>
            <option>Joinly Held</option>
          </select></td>
          <td><input  name="cChild12" type="text" value="" id="bankName" class="form-control input-sm" style="width: 100%"></td>
          <td><input  name="cChild22" type="text" value="" id="bankName" class="form-control input-sm" style="width: 100%"></td>
          <td><input name="cChild32" type="text" value="" id="bankName" class="form-control input-sm" style="width: 100%"></td>
          <td><input name="cChildTotal2" type="text" value="" id="bankName" class="form-control input-sm" style="width: 100%"></td>
        </tr>
        <tr>
          <td><input name="cItem3" type="text" value="" id="bankName" class="form-control input-sm" style="width: 100%"></td>
          
          <td><input name="cValue3" type="text" value="" id="bankName" class="form-control input-sm" style="width: 100%"></td>
          <td><select  name="cPropertyOf3" class="form-control input-sm">
            <option>Husband/Single</option>
            <option>Wife</option>
            <option>Joinly Held</option>
          </select></td>
          <td><input  name="cChild13" type="text" value="" id="bankName" class="form-control input-sm" style="width: 100%"></td>
          <td><input  name="cChild23" type="text" value="" id="bankName" class="form-control input-sm" style="width: 100%"></td>
          <td><input name="cChild33" type="text" value="" id="bankName" class="form-control input-sm" style="width: 100%"></td>
          <td><input name="cChildTotal3" type="text" value="" id="bankName" class="form-control input-sm" style="width: 100%"></td>
        </tr>
        
      </table>

      <!-- Personal Property -->
      <table class="table table-striped" border="0" id="bankAccountsTable">
      <tr style="background-color: #4A65CF;">
        <th colspan="8"><strong>Personal Property</strong><span>
          <small> (Furniture, Household Goods, Jewelry)</small>
        </span></th>
      </tr>
        <tr>
          <td width="190"></td>
          <td width=""></td>
          <td width=""></td>
          <td width=""></td>
          <td colspan="3" class="text-center">Allocation of Estate</td>
          <td width=""></td>
        </tr>
        <tr class="text-center" style="font-weight: bold">
          <td>Item</td>
          
          <td>Value</td>
          <td width="200">Property of</td>
          <td width="70">Child 1</td>
          <td width="70">Child 2</td>
          <td width="70">Child 3</td>
          <td width="">100%</td>
        </tr>
        <tr>
          <td><input name="ppItem1" type="text" value="" id="bankName" class="form-control input-sm" style="width: 100%"></td>
          
          <td><input  name="ppValue1" type="text" value="" id="bankName" class="form-control input-sm" style="width: 100%"></td>
          <td><select  name="ppPropertyOf1" class="form-control input-sm">
            <option>Husband/Single</option>
            <option>Wife</option>
            <option>Joinly Held</option>
          </select></td>
          <td><input name="ppChild11" type="text" value="" id="bankName" class="form-control input-sm" style="width: 100%"></td>
          <td><input name="ppChild21" type="text" value="" id="bankName" class="form-control input-sm" style="width: 100%"></td>
          <td><input name="ppChild31" type="text" value="" id="bankName" class="form-control input-sm" style="width: 100%"></td>
          <td><input name="ppChildTotal1" type="text" value="" id="bankName" class="form-control input-sm" style="width: 100%"></td>
        </tr>
        <tr>
          <td><input name="ppItem2" type="text" value="" id="bankName" class="form-control input-sm" style="width: 100%"></td>
          
          <td><input  name="ppValue2" type="text" value="" id="bankName" class="form-control input-sm" style="width: 100%"></td>
          <td><select  name="ppPropertyOf2" class="form-control input-sm">
            <option>Husband/Single</option>
            <option>Wife</option>
            <option>Joinly Held</option>
          </select></td>
          <td><input name="ppChild12" type="text" value="" id="bankName" class="form-control input-sm" style="width: 100%"></td>
          <td><input name="ppChild22" type="text" value="" id="bankName" class="form-control input-sm" style="width: 100%"></td>
          <td><input name="ppChild32" type="text" value="" id="bankName" class="form-control input-sm" style="width: 100%"></td>
          <td><input name="ppChildTotal2" type="text" value="" id="bankName" class="form-control input-sm" style="width: 100%"></td>
        </tr>
        <tr>
          <td><input name="ppItem3" type="text" value="" id="bankName" class="form-control input-sm" style="width: 100%"></td>
          
          <td><input  name="ppValue3" type="text" value="" id="bankName" class="form-control input-sm" style="width: 100%"></td>
          <td><select  name="ppPropertyOf3" class="form-control input-sm">
            <option>Husband/Single</option>
            <option>Wife</option>
            <option>Joinly Held</option>
          </select></td>
          <td><input name="ppChild13" type="text" value="" id="bankName" class="form-control input-sm" style="width: 100%"></td>
          <td><input name="ppChild23" type="text" value="" id="bankName" class="form-control input-sm" style="width: 100%"></td>
          <td><input name="ppChild33" type="text" value="" id="bankName" class="form-control input-sm" style="width: 100%"></td>
          <td><input name="ppChildTotal3" type="text" value="" id="bankName" class="form-control input-sm" style="width: 100%"></td>
        </tr>
        
      </table>

      <!-- Trust and Annuities -->
      <table class="table table-striped" border="0" id="bankAccountsTable">
      <tr style="background-color: #4A65CF;">
        <th colspan="8"><strong>Trust and Annuities</strong><span>
          
        </span></th>
      </tr>
        <tr>
          <td width="190"></td>
          <td width=""></td>
          <td width=""></td>
          <td width=""></td>
          <td colspan="3" class="text-center">Allocation of Estate</td>
          <td width=""></td>
        </tr>
        <tr class="text-center" style="font-weight: bold">
          <td>Name</td>
          <td style="font-size: 8pt" width="130">Type</td>
          <td>Value</td>
          <td width="152">Property of</td>
          <td width="70">Child 1</td>
          <td width="70">Child 2</td>
          <td width="70">Child 3</td>
          <td width="">100%</td>
        </tr>
        <tr>
          <td><input name="taName1" type="text" value="" id="bankName" class="form-control input-sm" style="width: 100%"></td>
          <td>
            <input name="taType1" type="text" name="" value="" class="form-control input-sm">
          </td>
          <td><input name="taValue1" type="text" value="" id="bankName" class="form-control input-sm" style="width: 100%"></td>
          <td><select name="taPropertyOf1" class="form-control input-sm">
            <option>Husband/Single</option>
            <option>Wife</option>
            <option>Joinly Held</option>
          </select></td>
          <td><input name="taChild11" type="text" value="" id="bankName" class="form-control input-sm" style="width: 100%"></td>
          <td><input name="taChild21" type="text" value="" id="bankName" class="form-control input-sm" style="width: 100%"></td>
          <td><input name="taChild31" type="text" value="" id="bankName" class="form-control input-sm" style="width: 100%"></td>
          <td><input name="taChildTotal1" type="text" value="" id="bankName" class="form-control input-sm" style="width: 100%"></td>
        </tr>
        <tr>
          <td><input name="taName2" type="text" value="" id="bankName" class="form-control input-sm" style="width: 100%"></td>
          <td>
            <input name="taType2" type="text" name="" value="" class="form-control input-sm">
          </td>
          <td><input name="taValue2" type="text" value="" id="bankName" class="form-control input-sm" style="width: 100%"></td>
          <td><select name="taPropertyOf2" class="form-control input-sm">
            <option>Husband/Single</option>
            <option>Wife</option>
            <option>Joinly Held</option>
          </select></td>
          <td><input name="taChild12" type="text" value="" id="bankName" class="form-control input-sm" style="width: 100%"></td>
          <td><input name="taChild22" type="text" value="" id="bankName" class="form-control input-sm" style="width: 100%"></td>
          <td><input name="taChild32" type="text" value="" id="bankName" class="form-control input-sm" style="width: 100%"></td>
          <td><input name="taChildTotal2" type="text" value="" id="bankName" class="form-control input-sm" style="width: 100%"></td>
        </tr>
        <tr>
          <td><input name="taName3" type="text" value="" id="bankName" class="form-control input-sm" style="width: 100%"></td>
          <td>
            <input name="taType3" type="text" name="" value="" class="form-control input-sm">
          </td>
          <td><input name="taValue3" type="text" value="" id="bankName" class="form-control input-sm" style="width: 100%"></td>
          <td><select name="taPropertyOf3" class="form-control input-sm">
            <option>Husband/Single</option>
            <option>Wife</option>
            <option>Joinly Held</option>
          </select></td>
          <td><input name="taChild13" type="text" value="" id="bankName" class="form-control input-sm" style="width: 100%"></td>
          <td><input name="taChild23" type="text" value="" id="bankName" class="form-control input-sm" style="width: 100%"></td>
          <td><input name="taChild33" type="text" value="" id="bankName" class="form-control input-sm" style="width: 100%"></td>
          <td><input name="taChildTotal3" type="text" value="" id="bankName" class="form-control input-sm" style="width: 100%"></td>
        </tr>
        
      </table>

      <!-- Life Insurance Death Benefits -->
      <table class="table table-striped" border="0" id="bankAccountsTable">
      <tr style="background-color: #4A65CF;">
        <th colspan="8"><strong>Life Insurance Death Benefits</strong><span>
          
        </span></th>
      </tr>
        <tr>
          <td width="190"></td>
          <td width=""></td>
          <td width=""></td>
          <td width=""></td>
          <td colspan="3" class="text-center">Allocation of Estate</td>
          <td width=""></td>
        </tr>
        <tr class="text-center" style="font-weight: bold">
          <td>Insurance Group</td>
          <td style="font-size: 8pt" width="130">Policy Number</td>
          <td>Value</td>
          <td width="152">Property of</td>
          <td width="70">Child 1</td>
          <td width="70">Child 2</td>
          <td width="70">Child 3</td>
          <td width="">100%</td>
        </tr>
        <tr>
          <td><input name="lfInsuranceGroup1" type="text" value="" id="bankName" class="form-control input-sm" style="width: 100%"></td>
          <td>
            <input name="lfPolicyNo1" type="text" value="" class="form-control input-sm">
          </td>
          <td><input name="lfValue1" type="text" value="" id="bankName" class="form-control input-sm" style="width: 100%"></td>
          <td><select name="lfPropertyOf1" class="form-control input-sm">
            <option>Husband/Single</option>
            <option>Wife</option>
            <option>Joinly Held</option>
          </select></td>
          <td><input name="lfChild11" type="text" value="" id="bankName" class="form-control input-sm" style="width: 100%"></td>
          <td><input name="lfChild21" type="text" value="" id="bankName" class="form-control input-sm" style="width: 100%"></td>
          <td><input name="lfChild31" type="text" value="" id="bankName" class="form-control input-sm" style="width: 100%"></td>
          <td><input name="lfChildTotal1" type="text" value="" id="bankName" class="form-control input-sm" style="width: 100%"></td>
        </tr>
        <tr>
          <td><input name="lfInsuranceGroup2" type="text" value="" id="bankName" class="form-control input-sm" style="width: 100%"></td>
          <td>
            <input name="lfPolicyNo2" type="text" value="" class="form-control input-sm">
          </td>
          <td><input name="lfValue2" type="text" value="" id="bankName" class="form-control input-sm" style="width: 100%"></td>
          <td><select name="lfPropertyOf2" class="form-control input-sm">
            <option>Husband/Single</option>
            <option>Wife</option>
            <option>Joinly Held</option>
          </select></td>
          <td><input name="lfChild12" type="text" value="" id="bankName" class="form-control input-sm" style="width: 100%"></td>
          <td><input name="lfChild22" type="text" value="" id="bankName" class="form-control input-sm" style="width: 100%"></td>
          <td><input name="lfChild32" type="text" value="" id="bankName" class="form-control input-sm" style="width: 100%"></td>
          <td><input name="lfChildTotal2" type="text" value="" id="bankName" class="form-control input-sm" style="width: 100%"></td>
        </tr>
        <tr>
          <td><input name="lfInsuranceGroup3" type="text" value="" id="bankName" class="form-control input-sm" style="width: 100%"></td>
          <td>
            <input name="lfPolicyNo3" type="text" value="" class="form-control input-sm">
          </td>
          <td><input name="lfValue3" type="text" value="" id="bankName" class="form-control input-sm" style="width: 100%"></td>
          <td><select name="lfPropertyOf3" class="form-control input-sm">
            <option>Husband/Single</option>
            <option>Wife</option>
            <option>Joinly Held</option>
          </select></td>
          <td><input name="lfChild13" type="text" value="" id="bankName" class="form-control input-sm" style="width: 100%"></td>
          <td><input name="lfChild23" type="text" value="" id="bankName" class="form-control input-sm" style="width: 100%"></td>
          <td><input name="lfChild33" type="text" value="" id="bankName" class="form-control input-sm" style="width: 100%"></td>
          <td><input name="lfChildTotal3" type="text" value="" id="bankName" class="form-control input-sm" style="width: 100%"></td>
        </tr>
        
      </table>

      <!-- Other Assets -->
      <table class="table table-striped" border="0" id="bankAccountsTable">
      <tr style="background-color: #4A65CF;">
        <th colspan="8"><strong>Other Assets</strong><span>
          
        </span></th>
      </tr>
        <tr>
          <td width="190"></td>
          <td width=""></td>
          <td width=""></td>
          <td width=""></td>
          <td colspan="3" class="text-center">Allocation of Estate</td>
          <td width=""></td>
        </tr>
        <tr class="text-center" style="font-weight: bold">
          <td>Description</td>
          <td>Value</td>
          <td width="152">Property of</td>
          <td width="70">Child 1</td>
          <td width="70">Child 2</td>
          <td width="70">Child 3</td>
          <td width="">100%</td>
        </tr>
        <tr>
          <td><input name="oaDescription1" type="text" value="" id="bankName" class="form-control input-sm" style="width: 100%"></td>
          
          <td><input name="oaValue1" type="text" value="" id="bankName" class="form-control input-sm" style="width: 100%"></td>
          <td><select name="oaPropertyOf1" class="form-control input-sm">
            <option>Husband/Single</option>
            <option>Wife</option>
            <option>Joinly Held</option>
          </select></td>
          <td><input name="oaChild11" type="text" value="" id="bankName" class="form-control input-sm" style="width: 100%"></td>
          <td><input name="oaChild21" type="text" value="" id="bankName" class="form-control input-sm" style="width: 100%"></td>
          <td><input name="oaChild31" type="text" value="" id="bankName" class="form-control input-sm" style="width: 100%"></td>
          <td><input name="oaChildTotal1" type="text" value="" id="bankName" class="form-control input-sm" style="width: 100%"></td>
        </tr>
        <tr>
          <td><input name="oaDescription2" type="text" value="" id="bankName" class="form-control input-sm" style="width: 100%"></td>
          
          <td><input name="oaValue2" type="text" value="" id="bankName" class="form-control input-sm" style="width: 100%"></td>
          <td><select name="oaPropertyOf2" class="form-control input-sm">
            <option>Husband/Single</option>
            <option>Wife</option>
            <option>Joinly Held</option>
          </select></td>
          <td><input name="oaChild12" type="text" value="" id="bankName" class="form-control input-sm" style="width: 100%"></td>
          <td><input name="oaChild22" type="text" value="" id="bankName" class="form-control input-sm" style="width: 100%"></td>
          <td><input name="oaChild32" type="text" value="" id="bankName" class="form-control input-sm" style="width: 100%"></td>
          <td><input name="oaChildTotal2" type="text" value="" id="bankName" class="form-control input-sm" style="width: 100%"></td>
        </tr>
        <tr>
          <td><input name="oaDescription3" type="text" value="" id="bankName" class="form-control input-sm" style="width: 100%"></td>
          
          <td><input name="oaValue3" type="text" value="" id="bankName" class="form-control input-sm" style="width: 100%"></td>
          <td><select name="oaPropertyOf3" class="form-control input-sm">
            <option>Husband/Single</option>
            <option>Wife</option>
            <option>Joinly Held</option>
          </select></td>
          <td><input name="oaChild13" type="text" value="" id="bankName" class="form-control input-sm" style="width: 100%"></td>
          <td><input name="oaChild23" type="text" value="" id="bankName" class="form-control input-sm" style="width: 100%"></td>
          <td><input name="oaChild33" type="text" value="" id="bankName" class="form-control input-sm" style="width: 100%"></td>
          <td><input name="oaChildTotal3" type="text" value="" id="bankName" class="form-control input-sm" style="width: 100%"></td>
        </tr>
       
      </table>
    </div>
    
    </div>
  </div>

  {!! Form::close() !!}