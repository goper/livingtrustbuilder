@extends('layouts.dashboardLayout')

@section('content')
<div class="col-lg-6 col-md-6">
  <h1 class="pull-left customHeader text-capitalize">{{ ucfirst(str_replace("_", " ", $pageName)) }}</h1>
</div> 
<div class="col-lg-6 col-md-6">
  <br><br>
  <a class="btn btn-success btn-lg pull-right" href="/dashboard/{{ str_replace(' ', '-',$projectData->name) }}/net_estate_calculator/">
    <i class="fa fa-arrow-circle-o-left"></i>
    Go Back
  </a>
</div>
@if(Session::has('flash_message'))
  <div class="alert alert-success">
      {{ Session::get('flash_message') }}
  </div>
@endif
@if(Session::has('error_message'))
  <div class="alert alert-danger">
      {{ Session::get('error_message') }}
  </div>
@endif

<div class="excelContainer col-lg-12 col-md-12 margin-top-sm" style="" >
  {!! Form::open(['class' => 'form-inline', 'url' => '/edit-file']) !!}
    <input type="hidden" name="planId" value="{{ $planId }}">
    <input type="hidden" name="projectName" value="{{ str_replace(" ", "-", $projectData->name) }}">
    <input type="hidden" name="pageName" value="{{ $pageName }}">

    <textarea name="editor1" id="editor1" rows="10" cols="80" style="border: 1px solid red">
      {{ $content }}
  
    </textarea>
    <script>
        // Replace the <textarea id="editor1"> with a CKEditor
        // instance, using default configuration.
        CKEDITOR.replace( 'editor1' );
    </script>
    

    <button type="submit" class="btn btn-primary btn-lg pull-right" style="margin-top: 2%">
      <i class="fa fa-save margin-right-sm"></i>
      Save
    </button>

{!! Form::close() !!}
{!! Form::open(['class' => 'form-inline', 'url' => '/download']) !!}
<textarea class="hidden" name="downloadContent">
  {{ $content }}
</textarea>
<input type="hidden" name="planId" value="{{ $planId }}">
<input type="hidden" name="projectId" value="{{ $projectData->id }}">
<input type="hidden" name="projectName" value="{{ str_replace(" ", "-", $projectData->name) }}">
    <input type="hidden" name="pageName" value="{{ $pageName }}">
<button type="submit" class="btn btn-primary btn-lg pull-right margin-right-md" style="margin-top: 2%">
      <i class="fa fa-cloud-download margin-right-sm"></i>
      Download
    </button>
{!! Form::close() !!}
</div>
@stop