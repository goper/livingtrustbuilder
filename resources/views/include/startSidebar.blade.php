<script src="{{ asset('js/calendar.min.js') }}"></script>
<link rel="stylesheet" type="text/css" href="{{ asset('css/calendar.css') }}">
<div class="customSideBar">
	<div class="userImage text-center">
		<img src="{{ asset('img/avatar/default.png')}}" class="img-circle" width="180" height="180">
		<br>
		<div class="margin-top-sm">
			<strong>
				<h4>{{ ucfirst(Auth::user()->first_name) . ' ' . ucfirst(Auth::user()->last_name) }}</h4>
			</strong>
		</div>
	</div>

	<div class="calendarContainer">
		<div id="my-calendar" style="height: 30%;">
			
		</div>
    <script>
        $(document).ready(function () {
            $("#my-calendar").zabuto_calendar({
            	language: "en",
            	today: true,
            });
        });
    </script>
	</div>
</div>