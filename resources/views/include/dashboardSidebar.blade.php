
<?php $planUrl = preg_replace("/[\s_]/", "-", $projectData->name); ?>
<div class="projectSidebarContainer" style="" id="menuLeft">
  <div class="projectName">
    <h3 style="margin-left: 4%">
      <strong>{{ ucfirst($projectData->name) }} 
        <small class="margin-left-md" style="color: #ffffff;">{{ '(' . ucfirst($planData->base_plan) . ')'}}</small>
      </strong>
       <i class="fa fa-angle-double-left pull-right margin-right-md" id="toggleLeft"
      style="font-size: 19pt"></i>
    </h3>
  </div>
  <div class="fileList">
    <img src="{{ asset('img/word_icon.png')}}" width="12" height="14">
    <a href="/dashboard/{{ $planUrl }}/cover_page">
      Cover Page
    </a>
    <i class="{{ Request::is('dashboard/' . $planUrl . '/cover_page') ? 'fa fa-angle-double-right pull-right' : '' }}"></i>
    </span>
  </div>
  <div class="fileList">
    <img src="{{ asset('img/word_icon.png')}}" width="12" height="14">
    <a href="/dashboard/{{ $planUrl }}/table_of_contents">
      Table of Contents
    </a>
    <i class="{{ Request::is('dashboard/' . $planUrl . '/table_of_contents') ? 'fa fa-angle-double-right pull-right' : '' }}"></i>
    </span>
  </div>
  <div class="fileList">
    <img src="{{ asset('img/word_icon.png')}}" width="12" height="14">
    <a href="/dashboard/{{ $planUrl }}/net_estate_calculator">
      Net Estate Calculator
    </a>
  </div>
  <div class="fileList">
   <!--  <i class="fa fa-file-excel-o margin-right-xs" style="color: #dd5555"></i> -->
    
   <img src="{{ asset('img/word_icon.png')}}" width="12" height="14">
    <a href="/dashboard/{{ $planUrl }}/accounts_and_contracts">
      Accounts & Contacts
    </a>
  </div>
  @if ($planData->base_plan == 'single')
    <div class="fileList">
      <img src="{{ asset('img/word_icon.png')}}" width="12" height="14">
      <a href="/dashboard/{{ $planUrl }}/single_person_living_trust">
        Single Person Living Trust
        </a>
        <i class="{{ Request::is('dashboard/' . $planUrl . '/single_person_living_trust') ? 'fa fa-angle-double-right pull-right' : '' }}"></i>
    </div>
  @endif
  @if ($planData->base_plan == 'married')
    <div class="fileList">
      <img src="{{ asset('img/word_icon.png')}}" width="12" height="14">
      <a href="/dashboard/{{ $planUrl }}/simple_joint_married_living_trust">
        Simple Joint - Married Living Trust
        </a>
        <i class="{{ Request::is('dashboard/' . $planUrl . '/simple_joint_married_living_trust') ? 'fa fa-angle-double-right pull-right' : '' }}"></i>
    </div>
    <div class="fileList">
      <img src="{{ asset('img/word_icon.png')}}" width="12" height="14">
      <a href="/dashboard/{{ $planUrl }}/trust_with_a_b_provisions">
        Trust with A-B Provisions
        </a>
        <i class="{{ Request::is('dashboard/' . $planUrl . '/trust_with_a_b_provisions') ? 'fa fa-angle-double-right pull-right' : '' }}"></i>
    </div>
    <div class="fileList">
      <img src="{{ asset('img/word_icon.png')}}" width="12" height="14">
      <a href="/dashboard/{{ $planUrl }}/childrens_trust">
        Children's Trust
        </a>
        <i class="{{ Request::is('dashboard/' . $planUrl . '/childrens_trust') ? 'fa fa-angle-double-right pull-right' : '' }}"></i>
    </div>
  @endif
  <div class="fileList">
    <img src="{{ asset('img/word_icon.png')}}" width="12" height="14">
    <a href="/dashboard/{{ $planUrl }}/pour_over_will">
      Pour Over Will
      </a>
      <i class="{{ Request::is('dashboard/' . $planUrl . '/pour_over_will') ? 'fa fa-angle-double-right pull-right' : '' }}"></i>
  </div>
  @if ($planData->base_plan == 'married')
    <div class="fileList">
      <img src="{{ asset('img/word_icon.png')}}" width="12" height="14">
      <a href="/dashboard/{{ $planUrl }}/quitclaim_deed_community_real_estate">
        Quitclaim Deed - Community Real Estate
        </a>
        <i class="{{ Request::is('dashboard/' . $planUrl . '/quitclaim_deed_community_real_estate') ? 'fa fa-angle-double-right pull-right' : '' }}"></i>
    </div>
    <div class="fileList">
      <img src="{{ asset('img/word_icon.png')}}" width="12" height="14">
      <a href="/dashboard/{{ $planUrl }}/quit_claim_deed_separate_real_estate">
        Quitclaim Deed - Separate Real Estate
        </a>
        <i class="{{ Request::is('dashboard/' . $planUrl . '/quit_claim_deed_separate_real_estate') ? 'fa fa-angle-double-right pull-right' : '' }}"></i>
    </div>
  @endif
  @if ($planData->base_plan == 'single')
    <div class="fileList">
      <img src="{{ asset('img/word_icon.png')}}" width="12" height="14">
      <a href="/dashboard/{{ $planUrl }}/quitclaim_deed">
        Quitclaim Deed
      </a>
      <i class="{{ Request::is('dashboard/' . $planUrl . '/quitclaim_deed') ? 'fa fa-angle-double-right pull-right' : '' }}"></i>
    </div>
  @endif
  <div class="fileList">
    <img src="{{ asset('img/word_icon.png')}}" width="12" height="14">
    <a href="/dashboard/{{ $planUrl }}/transfer_of_assets">
      Transfer of Assets
    </a>
    <i class="{{ Request::is('dashboard/' . $planUrl . '/transfer_of_assets') ? 'fa fa-angle-double-right pull-right' : '' }}"></i>
  </div>
  <div class="fileList">
    <img src="{{ asset('img/word_icon.png')}}" width="12" height="14">
    <a href="/dashboard/{{ $planUrl }}/successor_trustee">
      Successor Trustee
    </a>
    <i class="{{ Request::is('dashboard/' . $planUrl . '/successor_trustee') ? 'fa fa-angle-double-right pull-right' : '' }}"></i>
  </div>
  @if ($planData->base_plan == 'married')
    <div class="fileList">
      <img src="{{ asset('img/word_icon.png')}}" width="12" height="14">
      <a href="/dashboard/{{ $planUrl }}/abc_trust">
        ABC Trust
      </a>
      <i class="{{ Request::is('dashboard/' . $planUrl . '/abc_trust') ? 'fa fa-angle-double-right pull-right' : '' }}"></i>
    </div>
    <div class="fileList">
      <img src="{{ asset('img/word_icon.png')}}" width="12" height="14">
      <a href="/dashboard/{{ $planUrl }}/basic_will">
        Basic Will
      </a>
      <i class="{{ Request::is('dashboard/' . $planUrl . '/basic_will') ? 'fa fa-angle-double-right pull-right' : '' }}"></i>
    </div>
    <div class="fileList">
      <img src="{{ asset('img/word_icon.png')}}" width="12" height="14">
      <a href="/dashboard/{{ $planUrl }}/california_statutory_will">
        California Statutory Will
      </a>
      <i class="{{ Request::is('dashboard/' . $planUrl . '/california_statutory_will') ? 'fa fa-angle-double-right pull-right' : '' }}"></i>
    </div>
  @endif
  <div class="fileList">
    <img src="{{ asset('img/word_icon.png')}}" width="12" height="14">
    <a href="/dashboard/{{ $planUrl }}/durable_power_of_attorney">
      Durable Power of Attorney
    </a>
    <i class="{{ Request::is('dashboard/' . $planUrl . '/durable_power_of_attorney') ? 'fa fa-angle-double-right pull-right' : '' }}"></i>
  </div>
  <div class="fileList">
    <img src="{{ asset('img/word_icon.png')}}" width="12" height="14">
    <a href="/dashboard/{{ $planUrl }}/amendment_to_declaration_of_trust">
      Amendment to Declaration of Trust
    </a>
    <i class="{{ Request::is('dashboard/' . $planUrl . '/amendment_to_declaration_of_trust') ? 'fa fa-angle-double-right pull-right' : '' }}"></i>
  </div>
  @if ($planData->base_plan == 'single')
  <div class="fileList">
    <img src="{{ asset('img/word_icon.png')}}" width="12" height="14">
    <a href="/dashboard/{{ $planUrl }}/advance_health_care_directive">
        Advance Health Care Directive
    </a>
    <i class="{{ Request::is('dashboard/' . $planUrl . '/advance_health_care_directive') ? 'fa fa-angle-double-right pull-right' : '' }}"></i>
  </div>
  @endif
  @if ($planData->base_plan == 'married')
    <div class="fileList">
      <img src="{{ asset('img/word_icon.png')}}" width="12" height="14">
      <a href="/dashboard/{{ $planUrl }}/advance_health_care_directive_husband" class="">
          Advance Health Care Directive - Husband
      </a>
      <i class="{{ Request::is('dashboard/' . $planUrl . '/advance_health_care_directive_husband') ? 'fa fa-angle-double-right pull-right' : '' }}"></i>
    </div>
    <div class="fileList">
      <img src="{{ asset('img/word_icon.png')}}" width="12" height="14">
      <a href="/dashboard/{{ $planUrl }}/advance_health_care_directive_wife">
          Advance Health Care Directive - Wife
      </a>
      <i class="{{ Request::is('dashboard/' . $planUrl . '/advance_health_care_directive_wife') ? 'fa fa-angle-double-right pull-right' : '' }}"></i>
    </div>
  @endif
  <div class="fileList">
    <img src="{{ asset('img/word_icon.png')}}" width="12" height="14">
    <a href="/dashboard/{{ $planUrl }}/living_will">
        Living Will
    </a>
    <i class="{{ Request::is('dashboard/' . $planUrl . '/living_will') ? 'fa fa-angle-double-right pull-right' : '' }}"></i>
  </div>
  <div class="fileList">
    <img src="{{ asset('img/word_icon.png')}}" width="12" height="14">
    <a href="/dashboard/{{ $planUrl }}/hipaa_privacy_authorization">
      HIPAA Privacy Authorization
    </a>
    <i class="{{ Request::is('dashboard/' . $planUrl . '/hipaa_privacy_authorization') ? 'fa fa-angle-double-right pull-right' : '' }}"></i>
  </div>
  <div class="fileList">
    <img src="{{ asset('img/word_icon.png')}}" width="12" height="14">
    <a href="/dashboard/{{ $planUrl }}/california_directive_to_physician">
        California Directive to Physician
    </a>
    <i class="{{ Request::is('dashboard/' . $planUrl . '/california_directive_to_physician') ? 'fa fa-angle-double-right pull-right' : '' }}"></i>
  </div>
  @if ($planData->base_plan == 'married')
    <div class="fileList">
      <img src="{{ asset('img/word_icon.png')}}" width="12" height="14">
      <a href="/dashboard/{{ $planUrl }}/asset_power_of_attorney_wife">
          Asset Power of Attorney - Wife
      </a>
      <i class="{{ Request::is('dashboard/' . $planUrl . '/asset_power_of_attorney_wife') ? 'fa fa-angle-double-right pull-right' : '' }}"></i>
    </div>
    <div class="fileList">
      <img src="{{ asset('img/word_icon.png')}}" width="12" height="14">
      <a href="/dashboard/{{ $planUrl }}/asset_power_of_attorney_husband">
          Asset Power of Attorney - Husband
      </a>
      <i class="{{ Request::is('dashboard/' . $planUrl . '/asset_power_of_attorney_husband') ? 'fa fa-angle-double-right pull-right' : '' }}"></i>
    </div>
  @endif
  <div class="fileList">
    <img src="{{ asset('img/word_icon.png')}}" width="12" height="14">
    <a href="/dashboard/{{ $planUrl }}/after_death_checklist">
        After Death Checklist
    </a>
    <i class="{{ Request::is('dashboard/' . $planUrl . '/after_death_checklist') ? 'fa fa-angle-double-right pull-right' : '' }}"></i>
  </div>
  <div class="fileList">
    <img src="{{ asset('img/word_icon.png')}}" width="12" height="14">
    <a href="/dashboard/{{ $planUrl }}/revocation_of_declaration_of_trust">
        Revocation of Declaration of Trust
    </a>
    <i class="{{ Request::is('dashboard/' . $planUrl . '/revocation_of_declaration_of_trust') ? 'fa fa-angle-double-right pull-right' : '' }}"></i>
  </div>
  <div class="fileList">
    <img src="{{ asset('img/word_icon.png')}}" width="12" height="14">
    <a href="/dashboard/{{ $planUrl }}/business_general_partnership_agreement">
        Business - General Partnership Agreement
      </a>
      <i class="{{ Request::is('dashboard/' . $planUrl . '/business_general_partnership_agreement') ? 'fa fa-angle-double-right pull-right' : '' }}"></i>
  </div>
  <div class="fileList">
    <img src="{{ asset('img/word_icon.png')}}" width="12" height="14">
    <a href="/dashboard/{{ $planUrl }}/business_buy_sell_agreement">
        Business Buy-Sell Agreement
    </a>
    <i class="{{ Request::is('dashboard/' . $planUrl . '/business_buy_sell_agreement') ? 'fa fa-angle-double-right pull-right' : '' }}"></i>
  </div>
  
</div>
<div class="tooggler" style="display: none;" id="tooggler">
  <button class="btn btn-md btn-primary">
    <i class="fa fa-angle-double-right"></i>
  </button>
</div>