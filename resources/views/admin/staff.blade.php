@extends('layouts.master')

@section('content')

<h1 class="page-header" style="margin-top: 2%;">Staff Manager</h1>
<div class="staffContainer">
<table class="table table-hover" style="width: 90%;">
	<tr>
		<th>Id</th>
		<th>Name</th>
		<th>Role</th>
		<th>Date Register</th>
	</tr>
	<tr class="clickStaff" id="1 Dummy Name">
		<td>1</td>
		<td>Dummy Name</td>
		<td><strong>admin</strong></td>
		<td><small>July 10, 2015</small></td>
	</tr>
	<tr class="clickStaff" id="2 Dummy Name 2">
		<td>2</td>
		<td>Dummy Name2</strong></td>
		<td><strong>admin</td>
		<td><small>July 10, 2015</small></td>
	</tr>
	<tr class="clickStaff" id="3 Dummy Name 3">
		<td>3</td>
		<td>Dummy Name3</strong></td>
		<td><strong>admin</td>
		<td><small>July 10, 2015</small></td>
	</tr>
</table>
<br><br>
<div class="staffButtonContainer">
	<button class="btn btn-md btn-primary"><i class="fa fa-user-plus"></i>
		Add New Staff
	</button>
	<button disabled id="removeStaffBtn" type="button" class="btn btn-md btn-danger margin-left-md" data-toggle="modal" data-target="#removeStaff"><i class="fa fa-user-times"></i>
		Remove Staff
	</button>
	<input type="text" id="choosenId" hidden>
</div>
</div>

<!-- Modal Remove Staff -->
<div class="modal fade" id="removeStaff" tabindex="-1" role="dialog" aria-labelledby="mySmallModalLabel">
  <div class="modal-dialog modal-sm">
    <div class="modal-content panel-danger">
    	<div class="modal-header panel-heading">
        <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
        <h4 class="modal-title" id="myModalLabel">Remove Staff</h4>
      </div>
      <div class="modal-body text-center">
        Are you sure to remove <strong>"<span id="removeStaffName"></span>"</strong> as a staff?
      </div>
       <div class="modal-footer">
        <button type="button" class="btn btn-default" data-dismiss="modal">Cancel</button>
        <button type="button" class="btn btn-primary">Yes</button>
      </div>
    </div>
  </div>
</div>

@stop