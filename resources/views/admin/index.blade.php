@extends('layouts.master')

@section('content')

<h1 class="page-header" style="margin-top: 2%;">Dashboard</h1>
<div class="dashboardContainer">
	<div class="col-lg-6 col-md-6">
		<div class="panel panel-default" style="border: 1px solid #1e9751;">
		  <div class="panel-heading panel-green" style="background-color: #2ecc71;">
		  	<h3>	  		
		  			<span class="fa fa-files-o fa-3x"></span>
		  			<span class="pull-right" style="font-size: 55pt;"><strong>10</strong></span>
		  	</h3>
		  </div>
		  <div class="panel-body">
		    	<a href="/admin/files" class="pull-right">
		    		See All Files
		    		<span class="fa fa-arrow-circle-right"></span>
		    	</a>
		  </div>
		</div>
	</div>
	<div class="col-lg-6 col-md-6">
		<div class="panel panel-danger">
		  <div class="panel-heading panel-green">
		  	<h3>	  		
		  			<span class="fa fa-cloud-upload fa-3x"></span>
		  			<span class="pull-right" style="font-size: 55pt;"><strong>10</strong></span>
		  	</h3>
		  </div>
		  <div class="panel-body">
		    	<a href="/admin/back-up" class="pull-right">
		    		See All Backup Files
		    		<span class="fa fa-arrow-circle-right"></span>
		    	</a>
		  </div>
		</div>
	</div>
</div>
@stop