@extends('layouts.master')

@section('content')

<h1 class="page-header" style="margin-top: 2%;">Backup File Manager</h1>
<div class="fileContainer">
@if(Session::has('message'))
	<div class="alert {{ Session::get('alert-class') }}">
		<strong>Success!</strong>
		{{ Session::get('message') }}
	</div>
@endif
<table class="table">
	<tr class="text-center table-bordered">
		<th style="width: 40%;">File Name</th>
		<th>Note</th>
		<th>Owner</th>
		<th>Date Added</th>
		<th></th>
	</tr>
		@forelse ($cloudList as $cloud)
			<tr>
				<td>{{ $cloud->file_name }}</td>
				<td><small>{{ (isset($cloud->note) ? $cloud->note : 'None.' )}}</small></td>
				<td><strong>{{ Auth::user()->name }}</strong><small class="margin-left-sm" style="font-size: 7pt">Admin</small></td>
				<td>{{ $cloud->created_at}}</td>
				<td>
					<a href="/admin/download-{{ $cloud->file_name }}" class="margin-right-md">
						<span class="fa fa-download"></span></a> 
					| 
					<a href="#" class="margin-left-md"><span class="fa fa-remove text-danger"></span><a/>
					</td>
			</tr>
		@empty
		<tr>
			<td class="text-danger"> No files uploaded.</td>
		</tr>
	@endforelse
</table>
<br><br>
<div class="fileBtnContainer">
	<button class="btn btn-md btn-primary" data-toggle="modal" data-target="#addNewFile"><i class="fa fa-file-o"></i>
		Add New File
	</button>
</div>
</div>

<!-- Modal Add New File -->
{!! Form::open(['class' => 'form-inline', 
	'url' => '/admin/back-up/save',
	'method' => 'POST',
	'files' => true,
	]) !!}
<div class="modal fade" id="addNewFile" tabindex="-1" role="dialog" aria-labelledby="mySmallModalLabel">
  <div class="modal-dialog modal-lg">
    <div class="modal-content panel-primary">
    	<div class="modal-header panel-heading">
        <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
        <h4 class="modal-title" id="myModalLabel">Add New File</h4>
      </div>
      <div class="modal-body">
      	<div class="container">
					
						<div class="form-group">
					    <label for="fileInput">Select File</label>
					    <div class="fileUpload btn btn-default">
					    	<span class="fa fa-folder-open"></span>
						    <span>Browse</span>
						    {!! Form::file('file_name') !!}
						</div>
					  </div><br>
					  <div class="form-group">
					  	<br>
					    <label>Note</label><br>
					    <textarea name="note" class="form-control col-md-8" rows="3" id="noteFile" style="width: 200%"></textarea>
					  </div>
					
				</div>
      </div>
       <div class="modal-footer">
        <button type="submit" class="btn btn-primary">
        	<span class="fa fa-save"></span>
        	Save
        </button>
        <button type="button" class="btn btn-default" data-dismiss="modal">Cancel</button>
      </div>
    </div>
  </div>
</div>
{!! Form::close() !!}
@stop