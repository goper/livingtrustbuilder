@extends('layouts.master')

@section('content')

<h1 class="page-header" style="margin-top: 2%;">File Manager</h1>
<div class="cloudContainer">
<table class="table">
	<tr>
		<th>All Files</th>
		<th>Note</th>
		<th>Uploaded At</th>
		<th></th>
	</tr>
	@forelse ($cloudList as $cloud)
		<tr>
			<td>{{ $cloud->file_name }}</td>
			<td><small>{{ isset($cloud->note) ? $cloud->note : 'None.' }}</small></td>
			<td>July 10, 2015</td>
			<td>
				<a href="/admin/download-{{ $cloud->file_name }}" class="margin-right-md"><span class="fa fa-download"></span></a>
				 | 
				<a href="#" class="margin-left-md"><span class="fa fa-remove text-danger"></span></a>
			</td>
		</tr>
	@empty
			<tr>
				<td class="text-danger">No file available.</td>
			</tr>
	@endforelse
</table>
<br><br>
<div class="cloudButtonContainer">
</div>
</div>
@stop