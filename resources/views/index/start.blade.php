@extends('layouts.started')

@section('content')

<h1 class="page-header" style="color: #337ab7;">
	<strong><u>Let's Get Started On Your Plan</u>
	</strong>
</h1>
<div class="startContainer">
	<div class="col-lg-5 col-md-5" style="margin-left: -1.5%;">
		<h3 style="color: #337ab7"><strong>Open an Existing Project:</strong></h3>
		<br>
		<div class="projectLister">

				<ul class="list-group" style="width: 70%;">
				@forelse($planList as $plan)
					<?php $planUrl = preg_replace("/[\s_]/", "-", $plan->name); ?>
					<li class="list-group-item"  id="projectId">
						<a href="/dashboard/{{ $planUrl }}" style="font-size: 12pt;color:#807777">
							<strong>{{ $plan->name }}</strong>
						</a>
					</li>
				@empty
				<li class="list-group-item">
					<div class="aler alert-danger">
						No projects found.
					</div>
				</li>
				@endforelse
				</ul>
		</div>
		</div>
	
	<div class="col-lg-6 col-md-6" style=" ">
		<a href="/new-project">
		<button class="btn btn-lg btn-primary" style="margin-top: 19%;margin-left: -3%;">
			<img src="{{ asset('img/add_folder.png') }}">
			<strong>Start a New Project</strong>
		</button>
		</a>
	</div>
	</div>
</div>
@stop