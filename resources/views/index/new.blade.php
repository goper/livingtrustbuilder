@extends('layouts.started')

@section('content')
{!! Form::open(['class' => '', 'url' => '/create-project']) !!}
<link rel="stylesheet" href="{{ asset('css/datepicker.css') }}">
<script src="{{ asset('js/bootstrap-datepicker.js')}}"></script>
<script type="text/javascript">
    $(function () {

        $('#datetimepicker1').datepicker();
    });
</script>
<h2 class="firstPageheader" id="">
	<strong>New Project Configuration</strong>
</h2>
<div class="startContainer" id="firstPage">
	@if(Session::has('flash_message'))
    <div class="alert alert-success">
        {{ Session::get('flash_message') }}
    </div>
	@endif
	@if(Session::has('error_message'))
		<div class="alert alert-danger">
			<strong>Failed!</strong>
			{{ Session::get('error_message') }}
		</div>
	@endif
	<div class="col-lg-12 col-md-12" style="">	
		<div class="col-lg-12 col-md-12" id="singlePersonHeader">
			<h4 id="basePlanheader">Single Person</h4>
			<p class="lead" style="font-size: 8pt;">
				Here we offer you a variety of templates to choose from. First choose which applies
				best of your situation. Later, you can mix match, sort and select from all of our templates
				you have licensed - and assenble your project exactly the way you want it. Remember to look in
				the additional folders where you find supporting templates - as they to your project.
			</p>
		</div>
	</div>
	<div class="col-lg-12 col-md-12" style="">
		<div class="">
				<div class="col-lg-3 col-md-3 text-right" style="">
					<label for="" style="margin-top: 3%;">Product</label>
					<br><br>
					<label for="">Select base plan:</label>
					<br><br>
					<label class="margin-top-sm">Select the items to incude:</label><br><br><br><br>
					<br><br><br><br><br><br>
					<br><br>
					<label class="margin-top-xs" id="singleChoice" style="margin-top: 3%;">
						Select All:
					</label>
				</div>
				<div class="col-lg-6 col-md-6" style="padding-left: 0%;">
					<input type="text" name="productName" class="form-control" id="product" disabled value="Living Trust Builder">
					
					<select name="basePlan" class="form-control margin-top-sm" id="selectBasePlan" class="selectBasePlan">
					  <option value="single">Single Person</option>
					  <option value="married">Married Couple</option>
					</select><br>
					<!-- Single User Choice -->
					<div class="margin-left-xs choicesContainer" id="forSingleChoices">
					  <label>
					    <input name="singleCoverPage" type="checkbox" value="" class="chkBx">
					    Cover Page
					  </label><br>
					  <label>
					    <input name="tableOfContents" type="checkbox" value="" class="chkBx">
					    Table of Contents
					  </label><br>
					  <label>
					    <input type="checkbox" value="" class="chkBx">
					    Net Estate Calculator
					  </label><br>
					  <label>
					    <input type="checkbox" value="" class="chkBx">
					    Account & Contracts
					  </label><br>
					  <label>
					    <input type="checkbox" value="" class="chkBx">
					    Pour Over Will
					  </label><br>
					  <label>
					    <input type="checkbox" value="" class="chkBx">
					    Quitclaim Deed
					  </label><br>
					  <label>
					    <input type="checkbox" value="" class="chkBx">
					    Transfer of Assets
					  </label><br>
					  <label>
					    <input type="checkbox" value="" class="chkBx">
					    Successor Trustee
					  </label><br>
					  <label>
					    <input type="checkbox" value="" class="chkBx">
					    Durable Power of Attorney
					  </label><br>
					  <label>
					    <input type="checkbox" value="" class="chkBx">
					    Amendment of Declaration of Trust
					  </label><br>
					  <label>
					    <input type="checkbox" value="" class="chkBx">
					    Advance Health Care Directive
					  </label><br>
					  <label>
					    <input type="checkbox" value="" class="chkBx">
					    Living Will
					  </label><br>
						<label>
					    <input type="checkbox" value="" class="chkBx">
					    HIPAA Privacy Authorization
					  </label><br>
					  <label>
					    <input type="checkbox" value="" class="chkBx">
					    California Directive to Physician
					  </label><br>
					  <label>
					    <input type="checkbox" value="" class="chkBx">
					    After Death Checklist
					  </label><br>
					  <label>
					    <input type="checkbox" value="" class="chkBx">
					    Revocation of Declaration of Trust
					  </label><br>
					  <label>
					    <input type="checkbox" value="" class="chkBx">
					    Business - General Partnership Agreement
					  </label><br>
					  <label>
					    <input type="checkbox" value="" class="chkBx">
					    Business Buy-Sell Agreement
					  </label>
				  </div>

				  <!-- Married Couple Choices -->
				  <div class="margin-left-xs choicesContainer" id="forMarriedChoices" class="" style="display: none;">
					  <label>
					    <input type="checkbox" value="" class="chkBx">
					    Cover Page
					  </label><br>
					  <label>
					    <input type="checkbox" value="" class="chkBx">
					    Table of Contents
					  </label><br>
					  <label>
					    <input type="checkbox" value="" class="chkBx">
					    Net Estate Calculator
					  </label><br>
					  <label>
					    <input type="checkbox" value="" class="chkBx">
					    Account & Contracts
					  </label><br>
					  <label>
					    <input type="checkbox" value="" class="chkBx">
					    Simple Joint - Married Living Trust
					  </label><br>
					  <label>
					    <input type="checkbox" value="" class="chkBx">
					    Trust with A-B Provisions
					  </label><br>
					  <label>
					    <input type="checkbox" value="" class="chkBx">
					    Children's Trust
					  </label><br>
					  <label>
					    <input type="checkbox" value="" class="chkBx">
					    Pour Over Will
					  </label><br>
					  <label>
					    <input type="checkbox" value="" class="chkBx">
					    Quitclaim Deed - Community Real Estate
					  </label><br>
					  <label>
					    <input type="checkbox" value="" class="chkBx">
					    Quitclaim Deed - Separate Real Estate
					  </label><br>
					  <label>
					    <input type="checkbox" value="" class="chkBx">
					    Successor Trustee
					  </label><br>
					  <label>
					    <input type="checkbox" value="" class="chkBx">
					    ABC Trust
					  </label><br>
					  <label>
					    <input type="checkbox" value="" class="chkBx">
					    Amendment to Declaration of Trust
					  </label><br>
					  <label>
					    <input type="checkbox" value="" class="chkBx">
					    Basic Will
					  </label><br>
					  <label>
					    <input type="checkbox" value="" class="chkBx">
					    California Statutory Will
					  </label><br>
					  <label>
					    <input type="checkbox" value="" class="chkBx">
					    Living Will
					  </label><br>
					  <label>
					    <input type="checkbox" value="" class="chkBx">
					    Durable Power of Attorney
					  </label><br>
					  <label>
					    <input type="checkbox" value="" class="chkBx">
					    Advance Health Care Directive - Husband
					  </label><br>
					  <label>
					    <input type="checkbox" value="" class="chkBx">
					    Advance Health Care Directive - Wife
					  </label><br>
						<label>
					    <input type="checkbox" value="" class="chkBx">
					    HIPAA Privacy Authorization
					  </label><br>
					  <label>
					    <input type="checkbox" value="" class="chkBx">
					    California Directive to Physician
					  </label><br>
					  <label>
					    <input type="checkbox" value="" class="chkBx">
					    Asset Power of Attorney - Wife
					  </label><br>
					  <label>
					    <input type="checkbox" value="" class="chkBx">
					    Asset Power of Attorney - Husband
					  </label><br>
					  <label>
					    <input type="checkbox" value="" class="chkBx">
					    Transfer of Assets
					  </label><br>
					  <label>
					    <input type="checkbox" value="" class="chkBx">
					    Business - General Partnership Agreement
					  </label><br>
					  <label>
					    <input type="checkbox" value="" class="chkBx">
					    Business Buy-Sell Agreement
					  </label>
					  <label>
					    <input type="checkbox" value="" class="chkBx">
					    Revocation of Declaration of Trust
					  </label><br>
					  <label>
					    <input type="checkbox" value="" class="chkBx">
					    After Death Checklist
					  </label><br>
				  </div>
				  <label class="margin-top-xs">
				    <input style="margin-left: 40%;" type="checkbox" value="" name="" id="selectAll">
				  </label><br>
				</div>
			
		</div>
	</div>
</div>

<!-- Second Page -->
<div class="secondPageContainer" id="secondPage" style="display: none;">
	<div class="col-lg-12 col-md-12">
		<h4>Supply the following information for use in creating your documents: </h4>
		<div class="planForm margin-left-md" style="">
			<div class="form-group margin-top-xs col-lg-6 col-md-6">
        <label>ENTER PLAN NAME:</label>
        <div class="input-group" style="margin-left: 4%;">
          <div class="input-group-addon"><span class="fa fa-folder-open-o"></span></div>
          <input type="text" name="planName" class="form-control" id="" required placeholder="Plan Name">
        </div>
      </div>
      <div class="col-lg-12 col-md-12">
      	 <label>ABOUT YOURSELF: </label><br>
       	<table border="0" class="margin-left-sm">
       		<tr>
       			<td style="width: 60%;">
       				<label>Enter First Name:</label>
				        <div class="input-group">
				          <input type="text" name="firstName" required class="form-control" id="" placeholder="Enter First Name">
				        </div>
        		</td>
        		<td style="width: 70%;">
        			<label>Enter Last Name:</label>
				        <div class="input-group">
				          <input type="text" name="lastName" required class="form-control" id="" placeholder="Enter First Name">
				        </div>
        		</td>
       		</tr>
       	</table>
      </div>
		</div>
	</div>
	<div class="col-lg-8 col-md-8 margin-left-md margin-top-xs" >
		<h5 class="margin-left-xs text-capitalize"><strong>ABOUT YOUR BUSINESS</strong></h5>
		<div class="tableSecond margin-left-md">		
			<table class="table table-bordered secondPageTable" style="width: 93%">
			<tr>
				<td style="width: 50%;">Name of Trust</td>
				<td ><input type="text" class="" name="nameOfTrust"></td>
			</tr>
			<tr>
				<td>Date of Trust</td>
				<td>
					<div class="input-append date" id="datetimepicker1" data-date="" data-date-format="yyyy-dd-mm">
					  <span class="add-on"><input type="text" class="" name="dateOfTrust">
					  </span>
					</div>
					<!-- <input class="span2" size="16" type="text" value=""> --></td>
			</tr>
			<tr>
				<td>Trustor Name</td>
				<td><input type="text" class="" name="trustorName"></td>
			</tr>
			<tr>
				<td>2nd Trustor Name (for joint)</td>
				<td><input type="text" class="" name="secondTrustorName"></td>
			</tr>
			<tr>
				<td>Address</td>
				<td><input type="text" class="" name="address"></td>
			</tr>
			<tr>
				<td>City</td>
				<td><input type="text" class="" name="city"></td>
			</tr>
			<tr>
				<td>Country</td>
				<td><input type="text" class="" name="country"></td>
			</tr>
			<tr>
				<td>State</td>
				<td><input type="text" class="" name="state"></td>
			</tr>
			<tr>
				<td>Zip Code</td>
				<td><input type="text" class="" name="zipCode"></td>
			</tr>
			<tr>
				<td>Telephone</td>
				<td><input type="text" class="" name="telephone"></td>
			</tr>
			<tr>
				<td>Child 1</td>
				<td><input type="text" class="" name="child1"></td>
			</tr>
			<tr>
				<td>Child 2</td>
				<td><input type="text" class="" name="child2"></td>
			</tr>
			<tr>
				<td>Child 3</td>
				<td><input type="text" class="" name="child3"></td>
			</tr>
			<tr>
				<td>First Successor Trustee(s)</td>
				<td><input type="text" class="" name="firstSuccessorTrustee"></td>
			</tr>
			<tr>
				<td>Second Successor Trustee(s)</td>
				<td><input type="text" class="" name="secondSuccessorTrustee"></td>
			</tr>
			<tr>
				<td>Trustor 1's Primary Beneficiary 1</td>
				<td><input type="text" class="" name="trustor1PrimaryBeneficiary1"></td>
			</tr>
			<tr>
				<td>Trustor 1's Alternate Beneficiary 1</td>
				<td><input type="text" class="" name="trustor1AlternateBeneficiary1"></td>
			</tr>
			<tr>
				<td>Trustor 1's Primary Beneficiary 2</td>
				<td><input type="text" class="" name="trustor1PrimaryBeneficiary2"></td>
			</tr>
			<tr>
				<td>Trustor 1's Alternate Beneficiary 2</td>
				<td><input type="text" class="" name="trustor1AlternateBeneficiary2"></td>
			</tr>
			<tr>
				<td>Trustor 1's Primary Beneficiary 3</td>
				<td><input type="text" class="" name="trustor1PrimaryBeneficiary3"></td>
			</tr>
			<tr>
				<td>Trustor 1's Alternate Beneficiary 3</td>
				<td><input type="text" class="" name="trustor1AlternateBeneficiary3"></td>
			</tr>
			<tr>
				<td>Trustor 1's Primary Beneficiary 4</td>
				<td><input type="text" class="" name="trustor1PrimaryBeneficiary4"></td>
			</tr>
			<tr>
				<td>Trustor 1's Alternate Beneficiary 4</td>
				<td><input type="text" class="" name="trustor1AlternateBeneficiary4"></td>
			</tr>
			<tr>
				<td>Trustor 1's Residual Beneficiary</td>
				<td><input type="text" class="" name="trustor1ResidualBeneficiary"></td>
			</tr>
			<tr>
				<td>Trustor 1's Alternate Residuary Beneficiary</td>
				<td><input type="text" class="" name="trustor1AlternateBeneficiary"></td>
			</tr>
			<tr>
				<td>Trustor 2's Primary Beneficiary 1</td>
				<td><input type="text" class="" name="trustor2PrimaryBeneficiary1"></td>
			</tr>
			<tr>
				<td>Trustor 2's Alternate Beneficiary 1</td>
				<td><input type="text" class="" name="trustor2AlternateBeneficiary1"></td>
			</tr>
			<tr>
				<td>Trustor 2's Primary Beneficiary 2</td>
				<td><input type="text" class="" name="trustor2PrimaryBeneficiary2"></td>
			</tr>
			<tr>
				<td>Trustor 2's Alternate Beneficiary 2</td>
				<td><input type="text" class="" name="trustor2AlternateBeneficiary2"></td>
			</tr>
			<tr>
				<td>Trustor 2's Primary Beneficiary 3</td>
				<td><input type="text" class="" name="trustor2PrimaryBeneficiary"></td>
			</tr>
			<tr>
				<td>Trustor 2's Alternate Beneficiary 3</td>
				<td><input type="text" class="" name="trustor2AlternateBeneficiary3"></td>
			</tr>
			<tr>
				<td>Trustor 2's Primary Beneficiary 4</td>
				<td><input type="text" class="" name="trustor2PrimaryBeneficiary4"></td>
			</tr>
			<tr>
				<td>Trustor 2's Alternate Beneficiary 4</td>
				<td><input type="text" class="" name="trustor2AlternateBeneficiary4"></td>
			</tr>
			<tr>
				<td>Trustor 2's Residual Beneficiary</td>
				<td><input type="text" class="" name="trustor2ResidualBeneficiary"></td>
			</tr>
			<tr>
				<td>Trustor 2's Alternate Residuary Beneficiary</td>
				<td><input type="text" class="" name="trustor2AlternateResiduaryBeneficiary"></td>
			</tr>
			<tr>
				<td>Enter "None" for No Benefeciaries</td>
				<td><input type="text" class="" name="enterNoneForNoBenefeciaries"></td>
			</tr>
			<tr>
				<td>Child 1 Benefeciary for Child's Trust</td>
				<td><input type="text" class="" name="child1BeneficiaryForChildTrust"></td>
			</tr>
			<tr>
				<td>Age of Child 1 Beneficiary when Child's Trust will end</td>
				<td><input type="text" class="" name="ageOfChild1BeneficiaryWhenChildTrustWillEnd"></td>
			</tr>
			<tr>
				<td>Child 2 Benefeciary for Child's Trust</td>
				<td><input type="text" class="" name="child2BeneficiaryForChildTrust"></td>
			</tr>
			<tr>
				<td>Age of Child 2 Beneficiary when Child's Trust will end</td>
				<td><input type="text" class="" name="ageOfChild2BeneficiaryWhenChildTrustWillEnd"></td>
			</tr>
			<tr>
				<td>Child 3 Benefeciary for Child's Trust</td>
				<td><input type="text" class="" name="child3BeneficiaryForChildTrust"></td>
			</tr>
			<tr>
				<td>Age of Child 3 Beneficiary when Child's Trust will end</td>
				<td><input type="text" class="" name="ageOfChild3BeneficiaryWhenChildTrustWillEnd"></td>
			</tr>
			<tr>
				<td>Child 4 Benefeciary for Child's Trust</td>
				<td><input type="text" class="" name="child4BeneficiaryForChildTrust"></td>
			</tr>
			<tr>
				<td>Age of Child 4 Beneficiary when Child's Trust will end</td>
				<td><input type="text" class="" name="ageOfChild4BeneficiaryWhenChildTrustWillEnd"></td>
			</tr>
			<tr>
				<td>Child 1 Beneficiary for UTMA</td>
				<td><input type="text" class="" name="child1BeneficiaryForUtma"></td>
			</tr>
			<tr>
				<td>UTMA Custodian for Child 1 Beneficiary</td>
				<td><input type="text" class="" name="utmaCustodianForChild1Beneficiary"></td>
			</tr>
			<tr>
				<td>Age of Child 1 Beneficiary when UTMA custodianship will end</td>
				<td><input type="text" class="" name="ageOfChild1BeneficiaryWhenUtmaCustodianshipWillEnd"></td>
			</tr>
			<tr>
				<td>Child 2 Beneficiary for UTMA</td>
				<td><input type="text" class="" name="child2BeneficiaryForUtma"></td>
			</tr>
			<tr>
				<td>UTMA Custodian for Child 2 Beneficiary</td>
				<td><input type="text" class="" name="utmaCustodianForChild2Beneficiary"></td>
			</tr>
			<tr>
				<td>Age of Child 2 Beneficiary when UTMA custodianship will end</td>
				<td><input type="text" class="" name="ageOfChild2BeneficiaryWhenUtmaCustodianshipWillEnd"></td>
			</tr>
			<tr>
				<td>Child 3 Beneficiary for UTMA</td>
				<td><input type="text" class="" name="child3BeneficiaryForUtma"></td>
			</tr>
			<tr>
				<td>UTMA Custodian for Child 3 Beneficiary</td>
				<td><input type="text" class="" name="utmaCustodianForChild3Beneficiary"></td>
			</tr>
			<tr>
				<td>Age of Child 3 Beneficiary when UTMA custodianship will end</td>
				<td><input type="text" class="" name="ageOfChild3BeneficiaryWhenUtmaCustodianshipWillEnd"></td>
			</tr>
			<tr>
				<td>Child 4 Beneficiary for UTMA</td>
				<td><input type="text" class="" name="child4BeneficiaryForUtma"></td>
			</tr>
			<tr>
				<td>UTMA Custodian for Child 4 Beneficiary</td>
				<td><input type="text" class="" name="utmaCustodianForChild4Beneficiary"></td>
			</tr>
			<tr>
				<td>Age of Child 4 Beneficiary when UTMA custodianship will end</td>
				<td><input type="text" class="" name="ageOfChild4BeneficiaryWhenUtmaCustodianshipWillEnd"></td>
			</tr>
			<tr>
				<td>Person 1 to determine incapacity of Trustor(s) (contact info.)</td>
				<td><input type="text" class="" name="person1ContactInfo"></td>
			</tr>
			<tr>
				<td>Person 2 to determine incapacity of Trustor(s) (contact info.)</td>
				<td><input type="text" class="" name="person2ContactInfo"></td>
			</tr>
			<tr>
				<td>Person 3 to determine incapacity of Trustor(s) (contact info.)</td>
				<td><input type="text" class="" name="person3ContactInfo"></td>
			</tr>
		</table>
		</div>

	</div>
</div><!-- Container of Second page -->
<br><br>
<div class="btnFooter col-lg-9 col-md-9 margin-top-sm" style="">
	
	<a href="#" id="nextBtnFirstPage" class="btn btn-primary btn-lg pull-right" style="margin-left: 4%;"><i class="fa fa-arrow-circle-right"></i> Next</a>
	<!-- <a href="/" id="" style="" class="btn btn-primary btn-lg pull-right"><i class="fa fa-arrow-circle-right"></i> Next</a> -->
	<button type="submit" id="nextBtnSecondPage" style="display: none; margin-left: 4%;" class="btn btn-primary btn-lg pull-right">
		<i class="fa fa-arrow-circle-right"></i> Next
	</button>
	<a href="#" id="backBtnSecondPage" style="display: none;" class="btn btn-danger btn-lg pull-right"><i class="fa fa-arrow-circle-left"></i> Back</a>
	<a href="/getting-started" id="backBtnFirstPage" class="btn btn-danger btn-lg pull-right"><i class="fa fa-arrow-circle-left"></i> Back</a>

</div>

{!! Form::close() !!}
@stop