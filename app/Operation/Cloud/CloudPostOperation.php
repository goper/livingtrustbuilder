<?php 
namespace App\Operation\Cloud;

use Illuminate\Http\Request;
use Illuminate\Foundation\Validation\ValidatesRequests;
use App\Model\Cloud;

use Illuminate\Support\Facades\Storage;
use Illuminate\Support\Facades\File;

class CloudPostOperation extends Request {
	use ValidatesRequests;

	public $user_id;
	public $file_name;
	public $note;
	
	/**
	 * Determine if the user is authorized to make this request.
	 *
	 * @return bool
	 */
	public function authorize()
	{
		return false;
	}

	/**
	 * Get the validation rules that apply to the request.
	 *
	 * @return array
	 */
	public function rules()
	{
		return [
			'user_id' => 'required',
      'file_name' => 'required',
		];
	}

	public function save(Request $request)
	{
		$this->validate($request, $this->rules());

		$filename = $request->file('file_name');
		$extension = $filename->getClientOriginalExtension();
		Storage::disk('local')->put($filename->getFilename().'.'.$extension,  File::get($filename));

		$model = new Cloud();
		$model->user_id = $this->user_id;
		$model->file_name = $filename->getClientOriginalName();
		$model->mime = $filename->getClientMimeType();
		$model->original_filename = $filename->getFilename().'.'.$extension;
		$model->note = $request->input('note');

		$model->save();

	}

}
