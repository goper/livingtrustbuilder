<?php 
namespace App\Operation\Projects;

use Illuminate\Http\Request;
use Illuminate\Foundation\Validation\ValidatesRequests;

use App\Model\Projects;

class ProjectsOperation extends Request {
	use ValidatesRequests;

	public $userId;
	public $name;
	
	/**
	 * Determine if the user is authorized to make this request.
	 *
	 * @return bool
	 */
	public function authorize()
	{
		return false;
	}

	/**
	 * Get the validation rules that apply to the request.
	 *
	 * @return array
	 */
	public function rules()
	{
		return [
			'user_id' => 'required|exists:users',
      'name' => 'required',
		];
	}

	public function save(Request $request)
	{
		$this->validate($request, $this->rules());

		if ($validator->fails()) {
			//Session::flash('flash_message', $validator->messages());
			return $validator->mesages();
		}

		$model = new Projects();
		$model->user_id = $this->userId;
		$model->name = $this->name;

		$model->save();
	}

	/*public function createAssignment()
 {
  if (Request::isMethod('post')) {

   $request_data = Request::all();

   try {
    DB::beginTransaction();

    $assignment = Assignment::create([
     'user_id' => Auth::user()->id,
     'country_id' => $request_data['country_id'],
     'job_title' => $request_data['job_title'],
     'client_name' => $request_data['client_name'],
     'client_email' => $request_data['client_email'],
     'others' => $request_data['others'],
     'base_tariff' => $request_data['base_tariff'],
     'currency_code' => $request_data['currency_code'],
     'is_published' => isset($request_data['is_published']) ? Assignment::STATE_PUBLISHED : Assignment::STATE_DRAFT
    ]);

    if (isset($request_data['profile_id'])) {
     ActiveProfileAssignment::create([
      'profile_id' => $request_data['profile_id'],
      'assignment_id' => $assignment->id
     ]);

     DB::commit();
    }

   } catch (\Exception $e) {
    DB::rollback();
    Log::error($e->getMessage());

    return redirect()->intended('/moderator/create-assignment')->with('error_message', 'Something went wrong. Please try again!');
   }
  }

  return view('moderator.create-assignment')->with([
   'currencies' => Currency::getList(),
   'countries' => Country::getList()
  ]);
 }*/

}
