<?php 
namespace App\Operation;

use App\Model\NetEstate;
use DB;
use Log;

class NetEstateOperation {

	public $plan_id;
    public $type;
    public $property_of;
    public $child_1;
    public $child_2;
    public $child_3;
    public $child_total;
    public $col_1;
    public $col_2;
    public $col_3;
    public $col_4;

	public function save()
    {
        DB::beginTransaction();

        try {

        	$model = new NetEstate();
            $model->plan_id = $this->plan_id;
            $model->type = $this->type;
            $model->property_of = $this->property_of;
            $model->child_1 = $this->child_1;
            $model->child_2 = $this->child_2;
            $model->child_3 = $this->child_3;
            $model->child_total = $this->child_total;
            $model->col_1 = $this->col_1;
            $model->col_2 = $this->col_2;
            $model->col_3 = $this->col_3;
            $model->col_4 = $this->col_4;


            if ($model->save()) {
                DB::commit();
                return true;
            }
        } catch(\Exception $e) {
            DB::rollback();
            Log::error($e->getMessage());
            return false;
        }
    }
}