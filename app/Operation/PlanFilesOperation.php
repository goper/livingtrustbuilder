<?php 
namespace App\Operation;

use App\Model\PlanFiles;
use DB;
use Log;

class PlanFilesOperation {

    public $id;
	public $plan_id;
    public $file_name;
    public $content;
    public $type;

	public function save()
    {
        DB::beginTransaction();

        try {

        	$model = (!empty($this->id)) ? PlanFiles::getPlanFilesById($this->id) 
                : new PlanFiles();
            $model->plan_id = $this->plan_id;
            $model->file_name = $this->file_name;
            $model->content = $this->content;
            $model->type = $this->type;
            
            if ($model->save()) {
                DB::commit();
                return true;
            }
        } catch(\Exception $e) {
            DB::rollback();
            Log::error($e->getMessage());
            return false;
        }
    }
}