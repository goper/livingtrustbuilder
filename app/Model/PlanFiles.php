<?php

namespace App\Model;

use Illuminate\Database\Eloquent\Model;

class PlanFiles extends Model
{
    const WORD = 'word';
    const EXCEL = 'excel';

    /**
     * The table associated with the model.
     *
     * @var string
     */
    protected $table = 'plan_files';

    protected $fillable = [
    	'plan_id',
    	'file_name',
        'content',
        'type'

    ];

    /**
     * Return self by file_name and plan_id
     *
     * @param $file_name , $plan_id
     * @return array
     * */
    public static function getFilesByPlanIdAndFileName($file_name, $plan_id)
    {
        return self::where(['file_name'=> $file_name,'plan_id' => $plan_id])->first();
    }

    /**
     * Return self by ID
     *
     * @param $id
     * @return array
     * */
    public static function getPlanFilesById($id)
    {
        return self::find($id);
    }

}