<?php

namespace App\Model;

use Illuminate\Database\Eloquent\Model;

class NetEstate extends Model
{
    const BA = 'Bank Accounts';
    const CA = 'Cash Accounts';
    const I = 'Investments';
    const BI = 'Business Interests';
    const RP = 'Real Property';
    const RA = 'Retired Accounts';
    const C = 'Collectibles';
    const PR = 'Personal Property';
    const TA = 'Trust and Annuities';
    const LIDB = 'Life Insurance Death Benefits';
    const OA = 'Other Assets';

    /**
     * The table associated with the model.
     *
     * @var string
     */
    protected $table = 'net_estate';

    protected $fillable = [
    	'plan_id',
        'type',
        'property_of',
        'child_1',
        'child_2',
        'child_3',
        'child_total',
        'col_1',
        'col_2',
        'col_3',
        'col_4'
    ];

    /*
     * Get project by planId
     * 
     */
    public static function getExcelProject($planId, $type)
    {
        return self::where(['plan_id' => $planId, 'type' => $type])->first();
    }
}