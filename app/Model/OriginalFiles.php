<?php

namespace App\Model;

use Illuminate\Database\Eloquent\Model;

class OriginalFiles extends Model
{
    /**
     * The table associated with the model.
     *
     * @var string
     */
    protected $table = 'original_files';

    protected $fillable = [
    	'file_name',
        'content',
        'type',
    ];

}