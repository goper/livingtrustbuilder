<?php

namespace App\Model;

use Illuminate\Database\Eloquent\Model;

class Plans extends Model
{
    /**
     * The table associated with the model.
     *
     * @var string
     */
    protected $table = 'plans';

    protected $fillable = [
    	'project_id',
    	'name',
        'base_plan',
        'first_name',
        'last_name',
        'name_of_trust',
        'date_of_trust',
        'trustor_name',
        'second_trustor_name',
        'address',
        'city',
        'country',
        'state',
        'zip_code',
        'telephone',
        'child_1',
        'child_2',
        'child_3',
        'first_successor_trustee',
        'second_successor_trustee',

        'trustor_1s_primary_beneficiary_1',
        'trustor_1s_alternate_beneficiary_1',
        'trustor_1s_primary_beneficiary_2',
        'trustor_1s_alternate_beneficiary_2',
        'trustor_1s_primary_beneficiary_3',
        'trustor_1s_alternate_beneficiary_3',
        'trustor_1s_primary_beneficiary_4',
        'trustor_1s_alternate_beneficiary_4',
        'trustor_1s_residual_beneficiary',
        'trustor_1s_alternate_residuary_beneficiary',

        'trustor_2s_primary_beneficiary_1',
        'trustor_2s_alternate_beneficiary_1',
        'trustor_2s_primary_beneficiary_2',
        'trustor_2s_alternate_beneficiary_2',
        'trustor_2s_primary_beneficiary_3',
        'trustor_2s_alternate_beneficiary_3',
        'trustor_2s_primary_beneficiary_4',
        'trustor_2s_alternate_beneficiary_4',
        'trustor_2s_residual_beneficiary',
        'trustor_2s_alternate_residuary_beneficiary',

        'enter_none_for_no_beneficiary',
        'child_1_beneficiary_for_child_trust',
        'age_of_child_1_beneficiary_when_child_trus_will_end',
        'child_2_beneficiary_for_child_trust',
        'age_of_child_2_beneficiary_when_child_trus_will_end',
        'child_3_beneficiary_for_child_trust',
        'age_of_child_3_beneficiary_when_child_trus_will_end',
        'child_4_beneficiary_for_child_trust',
        'age_of_child_4_beneficiary_when_child_trus_will_end',
        'child_1_beneficiary_for_utma',
        'utma_custodian_for_child_1_beneficiary',
        'age_of_child_1_beneficiary_when_utma_custodianship_will_end',
        'child_2_beneficiary_for_utma',
        'utma_custodian_for_child_2_beneficiary',
        'age_of_child_2_beneficiary_when_utma_custodianship_will_end',
        'child_3_beneficiary_for_utma',
        'utma_custodian_for_child_3_beneficiary',
        'age_of_child_3_beneficiary_when_utma_custodianship_will_end',
        'child_4_beneficiary_for_utma',
        'utma_custodian_for_child_4_beneficiary',
        'age_of_child_4_beneficiary_when_utma_custodianship_will_end',
        'person_1_to_determine_incapacity_of_trustors',
        'person_2_to_determine_incapacity_of_trustors',
        'person_3_to_determine_incapacity_of_trustors',

    ];

    /**
     * Return self by project name and project id
     *
     * @param $name , $id
     * @return array
     * */
    public static function getPlanByProjectIdAndProjectName($id, $name)
    {
        return self::where(['project_id' => $id, 'name' => $name])->first();
    }

}