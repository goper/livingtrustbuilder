<?php 
namespace App\Http\Controllers\admin;

use App\Http\Controllers\Controller;

class StaffController extends Controller {

    /**
     * Admin Page
     *
     * @return Response
     */
    public function index()
    {

        return view('admin.staff');
    }

}