<?php 
namespace App\Http\Controllers\admin;

use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Session;
use App\Operation\Cloud\CloudPostOperation;
use App\Model\Cloud;
use Auth;
use Request;
use Input;
use Validator;
use Redirect;

use Illuminate\Support\Facades\Storage;
use Illuminate\Support\Facades\File;
use Illuminate\Http\Response;
use App\Model\PlanFiles;
use App\Model\Plans;
use App\Model\NetEstate;
use App\OriginalFiles\NetEstateFiles;
use App\OriginalFiles\AccountsAndContracts;
use App\OriginalFiles\OriginalFiles;

class BackupCloudController extends Controller {

    /**
     * Backup File Page
     *
     * @return Response
     */
    public function index()
    {

    	$cloudList = Cloud::all();

  		$data = [
  			'cloudList' => $cloudList,
  			];
        return view('admin.backup', $data);
    }

   /**
	 * Save Cloud
	 *
	 */
	public function save()
	{
		if (Request::method('post')) {
			
			/*$filename = Request::file('file_name');
			$extension = $filename->getClientOriginalExtension();
			Storage::disk('local')->put($filename->getFilename().'.'.$extension,  File::get($filename));

			$model = new Cloud();
			$model->user_id = Auth::user()->id;
			$model->file_name = $filename->getClientOriginalName();
			$model->mime = $filename->getClientMimeType();
			$model->original_filename = $filename->getFilename().'.'.$extension;
			$model->note = Request::input('note');

			$model->save();

			Session::flash('message', 'File has been saved.'); 
			Session::flash('alert-class', 'alert-success');

			return redirect('/admin/back-up');*/

			// getting all of the post data
		  $file = array('file_name' => Input::file('file_name'));
		  // setting up rules
		  $rules = array('file_name' => 'required',); //mimes:jpeg,bmp,png and for max size max:10000
		  // doing the validation, passing post data, rules and the messages
		  $validator = Validator::make($file, $rules);
		  if ($validator->fails()) {
		    // send back to the page with the input data and errors
		    return Redirect::to('/admin/back-up')->withInput()->withErrors($validator);
		  }
		  else {
		    if (Input::hasFile('file_name')) {
		    	if (Input::file('file_name')->isValid()) {

		    		$filename = Request::file('file_name');

			      $destinationPath = public_path() . '/cloud/';

			      $extension = $filename->getClientOriginalExtension();
			      $filename = $filename->getFilename() . '.' . $extension;

			      Input::file('file_name')->move($destinationPath, $filename);

			      $file = Request::file('file_name');
			      $model = new Cloud();
						$model->user_id = Auth::user()->id;
						$model->file_name = $file->getClientOriginalName();
						$model->mime = $file->getClientMimeType();
						$model->original_filename = $file->getFilename().'.'.$extension;
						$model->note = Request::input('note');

						$model->save();

						Session::flash('message', 'File has been saved.'); 
						Session::flash('alert-class', 'alert-success');

			      return Redirect::to('/admin/back-up');
			    }
			    else {
			      // sending back with error message.
			      Session::flash('error', 'uploaded file is not valid');
			      return Redirect::to('/admin/back-up');
			    }
		    }
		    
		  }

		}
	}

	/**
   * Backup File Page
   *
   * @return Response
   */
  public function download()
  {
  	if (Auth::check()) {

  		if (Request::method('post')) {
  			$request = Request::all();
  			$pageName = $request['pageName'];

  			if (isset($request['subPageName'])) {
  				$file = PlanFiles::getFilesByPlanIdAndFileName($request['pageName'] . '-' . $request['subPageName'], $request['planId']);
  			} else {
  				$file = PlanFiles::getFilesByPlanIdAndFileName($request['pageName'], $request['planId']);
  			}
  			
  			if (!($file)) {
  				// Get Original Files
  				$data = Plans::getPlanByProjectIdAndProjectName($request['projectId'], str_replace("-"," ",$request['projectName']));
  				
  				if (isset($request['subPageName'])) {
  					// Excel
  					$file = new AccountsAndContracts($data);

  					if (method_exists($file, $request['subPageName']) === true) {
  						$pageContent = $file->$request['subPageName']();
  					} else {
  						Session::flash('error_message', 'Page not found.');
            	return view('errors.503');
  					}
  					
  				} else {
  					// WORD
  					// Calculator
  					if ($pageName == 'net_estate_calculator') {
  						$data = NetEstate::where(['plan_id' => $request['planId'] ])->get();
  						$file = new NetEstateFiles($data);
  						$pageContent = $file->$pageName();
  					} else {
  						$file = new OriginalFiles($data);

	  					if (method_exists($file, $pageName) === true) {
	  						$pageContent = $file->$pageName();
	  					} else {
	  						Session::flash('error_message', 'Page not found.');
	            	return view('errors.503');
	  					}
  					}
  					
  					
  				}
  			} else {
  				$pageContent = $file->content;
  			}

  			$content = '<?php
					header("Content-type: application/vnd.ms-word");
					header("Content-Disposition: attachment; filename=document_name.doc");
				?>
				<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
				<html xmlns="http://www.w3.org/1999/xhtml">
				<meta http-equiv=\"Content-Type\" content=\"text/html; charset=Windows-1252\">
				<title>Saves as a Word Doc</title>
				<body>' . $pageContent . '</body>
				</html>';

  			$filename = public_path() . "\cloud\\" . Auth::user()->first_name . ".doc";

				if (file_exists($filename)) {
				    unlink($filename);
				}

				$fp = fopen($filename, "w+");
				fwrite($fp, $content);

				$pathToFile = $filename;
				$downloadFileName = $pageName . '.doc';
				if (isset($request['subPageName'])) {
					$downloadFileName = $pageName . '-' . $request['subPageName'] . '.doc';
				}

	    	return response()->download($pathToFile, $downloadFileName);
	    	/*Goper*/
  		}
  		
  	}
  }

}