<?php 
namespace App\Http\Controllers\admin;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;

class AdminController extends Controller {

    /**
     * Admin Page
     *
     * @return Response
     */
    public function index(Request $request)
    {
    		
        return view('admin.index');
    }

}