<?php 
namespace App\Http\Controllers\admin;

use App\Http\Controllers\Controller;
use App\Model\Cloud;
use Auth;

class CloudController extends Controller {

    /**
     * File Manager Page
     *
     * @return Response
     */
    public function index()
    {
    		$cloudList = Cloud::all();

    		$data = [
    			'cloudList' => $cloudList,
    			];
        return view('admin.cloud', $data);
    }

}