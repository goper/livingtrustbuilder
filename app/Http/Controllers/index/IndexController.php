<?php 
namespace App\Http\Controllers\index;

use App\Http\Controllers\Controller;
//use Illuminate\Http\Request;
use Illuminate\Support\Facades\Session;
use Auth;
use Request;
use DB;
use Log;
use Validator;

/*Models*/
use App\Model\Projects;
use App\Model\Users;
use App\Model\Plans;

/*Operations*/
use App\Operation\Plans\PlanOperation;
use App\Operation\Projects\ProjectsOperation;

class IndexController extends Controller {

    /**
     * Start Page
     *
     * @return Response
     */
    public function start()
    {
        $userId = Auth::id();
    	$planList = Projects::where('user_id', $userId)->get();
        
        $data = [
            'planList' => $planList,
        ];

        return view('index.start', $data);
    }

    /**
     * Start a new Project
     *
     * @return Response
     */
    public function newProject()
    {
        return view('index.new');
    }

    /**
     * Create New project
     *
     * @return Response
     */
    public function createNewProject()
    {
            
        if (Request::isMethod('post')) {
             $request = Request::all();

            $planNameChecker = Projects::where([
                'user_id' => Auth::id(),
                'name' => $request['planName']
            ])->count();

            if ($planNameChecker > 0) {
                Session::flash('error_message', 'Project name already exist.');
                return redirect()->back();
            }
            try {

                DB::beginTransaction();
                //Create New Project First
                $projectId = Projects::create([
                    'user_id' => Auth::id(),
                    'name' => $request['planName']
                ]);

                /*$opt->user_id = Auth::id();
                $opt->name = $request['planName'];
                $opt->save();*/

                //Create new Plan
                $model = Plans::create([
                    'project_id' => $projectId->id,
                    'name' => $request['planName'],
                    'base_plan' => $request['basePlan'],
                    'first_name' => $request['firstName'],
                    'last_name' => $request['lastName'],
                    'name_of_trust' => $request['nameOfTrust'],
                    'date_of_trust' => $request['dateOfTrust'],
                    'trustor_name' => $request['trustorName'],
                    'second_trustor_name' => $request['secondTrustorName'],
                    'address' => $request['address'],
                    'city' => $request['city'],
                    'country' => $request['country'],
                    'state' => $request['state'],
                    'zip_code' => $request['zipCode'],
                    'telephone' => $request['telephone'],
                    'child_1' => $request['child1'],
                    'child_2' => $request['child2'],
                    'child_3' => $request['child3'],
                    'first_successor_trustee' => $request['firstSuccessorTrustee'],
                    'second_successor_trustee' => $request['secondSuccessorTrustee'],

                    'trustor_1s_primary_beneficiary_1' => $request['trustor1PrimaryBeneficiary1'],
                    'trustor_1s_alternate_beneficiary_1' => $request['trustor1AlternateBeneficiary1'],
                    'trustor_1s_primary_beneficiary_2' => $request['trustor1PrimaryBeneficiary2'],
                    'trustor_1s_alternate_beneficiary_2' => $request['trustor1AlternateBeneficiary2'],
                    'trustor_1s_primary_beneficiary_3' => $request['trustor1PrimaryBeneficiary3'],
                    'trustor_1s_alternate_beneficiary_3' => $request['trustor1AlternateBeneficiary3'],
                    'trustor_1s_primary_beneficiary_4' => $request['trustor1PrimaryBeneficiary4'],
                    'trustor_1s_alternate_beneficiary_4' => $request['trustor1AlternateBeneficiary4'],
                    'trustor_1s_residual_beneficiary' => $request['trustor1ResidualBeneficiary'],
                    'trustor_1s_alternate_residuary_beneficiary' => $request['trustor1AlternateBeneficiary'],

                    'trustor_2s_primary_beneficiary_1' => $request['trustor2PrimaryBeneficiary1'],
                    'trustor_2s_alternate_beneficiary_1' => $request['trustor2AlternateBeneficiary1'],
                    'trustor_2s_primary_beneficiary_2' => $request['trustor2PrimaryBeneficiary2'],
                    'trustor_2s_alternate_beneficiary_2' => $request['trustor2AlternateBeneficiary2'],
                    'trustor_2s_primary_beneficiary_3' => $request['trustor2PrimaryBeneficiary'],
                    'trustor_2s_alternate_beneficiary_3' => $request['trustor2AlternateBeneficiary3'],
                    'trustor_2s_primary_beneficiary_4' => $request['trustor2PrimaryBeneficiary4'],
                    'trustor_2s_alternate_beneficiary_4' => $request['trustor2AlternateBeneficiary4'],
                    'trustor_2s_residual_beneficiary' => $request['trustor2ResidualBeneficiary'],
                    'trustor_2s_alternate_residuary_beneficiary' => $request['trustor2AlternateResiduaryBeneficiary'],
                    
                    'enter_none_for_no_beneficiary' => $request['enterNoneForNoBenefeciaries'],
                    
                    'child_1_beneficiary_for_child_trust' => $request['child1BeneficiaryForChildTrust'],
                    'age_of_child_1_beneficiary_when_child_trus_will_end' => $request['ageOfChild1BeneficiaryWhenChildTrustWillEnd'],
                    
                    'child_2_beneficiary_for_child_trust' => $request['child2BeneficiaryForChildTrust'],
                    'age_of_child_2_beneficiary_when_child_trus_will_end' => $request['ageOfChild2BeneficiaryWhenChildTrustWillEnd'],
                    
                    'child_3_beneficiary_for_child_trust' => $request['child3BeneficiaryForChildTrust'],
                    'age_of_child_3_beneficiary_when_child_trus_will_end' => $request['ageOfChild3BeneficiaryWhenChildTrustWillEnd'],
                    
                    'child_4_beneficiary_for_child_trust' => $request['child4BeneficiaryForChildTrust'],
                    'age_of_child_4_beneficiary_when_child_trus_will_end' => $request['ageOfChild4BeneficiaryWhenChildTrustWillEnd'],
                    
                    'child_1_beneficiary_for_utma' => $request['child1BeneficiaryForUtma'],
                    'utma_custodian_for_child_1_beneficiary' => $request['utmaCustodianForChild1Beneficiary'],
                    'age_of_child_1_beneficiary_when_utma_custodianship_will_end' => $request['ageOfChild1BeneficiaryWhenUtmaCustodianshipWillEnd'],
                    
                    'child_2_beneficiary_for_utma' => $request['child2BeneficiaryForUtma'],
                    'utma_custodian_for_child_2_beneficiary' => $request['utmaCustodianForChild2Beneficiary'],
                    'age_of_child_2_beneficiary_when_utma_custodianship_will_end' => $request['ageOfChild2BeneficiaryWhenUtmaCustodianshipWillEnd'],
                    
                    'child_3_beneficiary_for_utma' => $request['child3BeneficiaryForUtma'],
                    'utma_custodian_for_child_3_beneficiary' => $request['utmaCustodianForChild3Beneficiary'],
                    'age_of_child_3_beneficiary_when_utma_custodianship_will_end' => $request['ageOfChild3BeneficiaryWhenUtmaCustodianshipWillEnd'],
                    
                    'child_4_beneficiary_for_utma' => $request['child4BeneficiaryForUtma'],
                    'utma_custodian_for_child_4_beneficiary' => $request['utmaCustodianForChild4Beneficiary'],
                    'age_of_child_4_beneficiary_when_utma_custodianship_will_end' => $request['ageOfChild4BeneficiaryWhenUtmaCustodianshipWillEnd'],
                    
                    'person_1_to_determine_incapacity_of_trustors' => $request['person1ContactInfo'],
                    'person_2_to_determine_incapacity_of_trustors' => $request['person2ContactInfo'],
                    'person_3_to_determine_incapacity_of_trustors' => $request['person3ContactInfo']
                ]);
                
                DB::commit();

            } catch (\Exception $e) {
                DB::rollback();
                Log::error($e->getMessage());
                Session::flash('error_message', 'Something went wrong.');
                return view('errors.503');
            }
            Session::flash('flash_message', 'Project created successfully.');
            return redirect('/dashboard/' . $request['planName']);
        }//post
    }//class

}