<?php 
namespace App\Http\Controllers\dashboard;

use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Session;
use Request;
use Auth;
use DB;
use Log;

/*Models*/
use App\Model\Projects;
use App\Model\Plans;
use App\Model\PlanFiles;
use App\OriginalFiles\OriginalFiles;
use App\OriginalFiles\NetEstateFiles;
use App\OriginalFiles\AccountsAndContracts;
use App\Model\NetEstate;
use App\Operation\NetEstateOperation;
use App\Operation\PlanFilesOperation;

class DashBoardController extends Controller {

    /**
     * Dashboard Page
     *
     * @return Response
     */
    public function index()
    {
        $projectName = Request::route('projectName');
        $projectName = str_replace("-", " ", $projectName);

        $projectChecker = Projects::where('user_id', Auth::id())->where('name', $projectName)->count();

        if ($projectChecker == 0) {
            Session::flash('flash_message', 'Project not found.');
            return view('errors.503');
        }

        $projectData = Projects::where('user_id', Auth::id())->where('name', $projectName)->first();
        $planData = Plans::where(['id' => $projectData->id ])->first();

        $data = [
            'projectData' => $projectData,
            'planData' => $planData
            
        ];
        return view('dashboard.index', $data);
    }

    /**
     * Save file
     *
     * @return Response
     */
    public function saveFile()
    {
    	if (Request::method('post')) {
        $text = Request::input('editor1');

        //echo $text;
        file_put_contents('original_files/asd' . ".txt", $text);

      }
    }

    /*
     * Show files view
     */
    public function viewPage() {

        $pageName = Request::route('pageName');
        $projectName = Request::route('projectName');
        $projectName = str_replace("-", " ", $projectName);

        $projectChecker = Projects::where('user_id', Auth::id())->where('name', $projectName)->count();

        if ($projectChecker == 0) {
            Session::flash('error_message', 'Project not found.');
            return view('errors.503');
        }


        $projectData = Projects::where('user_id', Auth::id())->where('name', $projectName)->first();
        $data = Plans::getPlanByProjectIdAndProjectName($projectData->id, $projectData->name);
        $planData = Plans::where(['id' => $projectData->id ])->first();

        // Excel page
        if ($pageName == 'accounts_and_contracts' || $pageName == 'net_estate_calculator') {
            $data = [
                'pageName' => $pageName,
                'projectData' => $projectData,
                'pageName' => $pageName,
                'planId' => $data->id,
                'planData' => $planData
            ];

            if ($pageName == 'accounts_and_contracts') {
                return redirect('/dashboard/' . str_replace(" ", "-", $projectData->name) . '/accounts_and_contracts/sub-intro');
            }
            return view('dashboard.excel', $data);
        }

        $editedPageChecker = PlanFiles::where([
            'file_name'=> $pageName,
            'plan_id' => $data->id
        ])->count();

        $page = new OriginalFiles($data);

        if (method_exists($page, $pageName) === true) {

            if ($editedPageChecker > 0) {
                //Edited
                $editedFile = PlanFiles::where([
                    'file_name'=> $pageName,
                    'plan_id' => $data->id
                ])->first();
                $content = $editedFile->content;
            } else {
                //Original
                $content = $page->$pageName();
            }
            
            $datas = [
                'page' => $content,
                'projectData' => $projectData,
                'pageName' => $pageName,
                'planId' => $data->id,
                'planData' => $planData
            ];

            return view('dashboard.page', $datas);

        } else {

            Session::flash('error_message', 'Page not found.');
            return view('errors.503');
        }

        
    }

    /**
     * Edit file
     *
     * @return Response
     */
    public function editFile()
    {
        if (Request::method('post')) {
            $request = Request::all();

            try {

                DB::beginTransaction();

                $pageChecker = PlanFiles::where([
                    'file_name'=> $request['pageName'],
                    'plan_id' => $request['planId'],
                ])->count();

                if ($pageChecker > 0) {

                    //EDIT
                    $model = PlanFiles::where([
                        'file_name'=> $request['pageName'],
                        'plan_id' => $request['planId']
                    ])->first();

                    $model->content = $request['editor1'];
                    $model->save();

                } else {
                    // Create
                    $opt = PlanFiles::create([
                        'plan_id' => $request['planId'],
                        'file_name' => $request['pageName'],
                        'content' => $request['editor1'],
                        'type' => 'word'
                    ]);
                }

                DB::commit();

            } catch (\Exception $e) {
                DB::rollback();
                Log::error($e->getMessage());
                Session::flash('error_message', 'Something went wrong.');
                return view('errors.503');
            }

            $projectName = $request['projectName'];
            $pageName = $request['pageName'];

            Session::flash('flash_message',  ucfirst(str_replace("_", " ", $pageName)) . ' is successfully edited.');
            return redirect('/dashboard/' .$projectName . '/' . $pageName);

        }
    }

    public function editNetEstate()
    {
        if (Request::method('post')) {
            $request = Request::all();
            $planId = $request['planId'];
            $projectName = $request['projectName'];
            //Filechecker create or update
            
            //Insert All Bank Accounts
            $type = NetEstate::BA;
           
            $childTotal1 = $request['baChild11'] + $request['baChild21'] + $request['baChild31'];
            $childTotal2 = $request['baChild12'] + $request['baChild22'] + $request['baChild32'];
            $childTotal3 = $request['baChild13'] + $request['baChild23'] + $request['baChild33'];

            $opt = new NetEstateOperation();
            $opt->plan_id = $planId;
            $opt->type = $type;
            $opt->property_of = $request['baPropertyOf1'] . ';' . $request['baPropertyOf2'] . ';' . $request['baPropertyOf3'];
            $opt->child_1 = $request['baChild11'] . ';' . $request['baChild12'] . ';' . $request['baChild13'];
            $opt->child_2 = $request['baChild21'] . ';' . $request['baChild22'] . ';' . $request['baChild23'];
            $opt->child_3 = $request['baChild31'] . ';' . $request['baChild32'] . ';' . $request['baChild33'];
            $opt->child_total =  $childTotal1. ';' . $childTotal2  . ';' .  $childTotal3;
            $opt->col_1 = $request['baFinancialInstitution1'] . ';' . $request['baFinancialInstitution2'] . ';' . $request['baFinancialInstitution3'];
            $opt->col_2 = $request['baTypeOfAccount1'] . ';' . $request['baTypeOfAccount2'] . ';' . $request['baTypeOfAccount3'];
            $opt->col_3 = $request['baValue1'] . ';' . $request['baValue2'] . ';' . $request['baValue3'];
            $opt->col_4 = '';

            if (!$opt->save()) {
                Session::flash('error_message', 'Internal server error');
                return view('errors.503');
            }

            //Insert All Cash Accounts
            $type = NetEstate::CA;
           
            $childTotal1 = $request['caChild11'] + $request['caChild21'] + $request['caChild31'];
            $childTotal2 = $request['caChild12'] + $request['caChild22'] + $request['caChild32'];
            $childTotal3 = $request['caChild13'] + $request['caChild23'] + $request['caChild33'];

            $opt = new NetEstateOperation();
            $opt->plan_id = $planId;
            $opt->type = $type;
            $opt->property_of = $request['caPropertyOf1'] . ';' . $request['caPropertyOf2'] . ';' . $request['caPropertyOf3'];
            $opt->child_1 = $request['caChild11'] . ';' . $request['caChild12'] . ';' . $request['caChild13'];
            $opt->child_2 = $request['caChild21'] . ';' . $request['caChild22'] . ';' . $request['caChild23'];
            $opt->child_3 = $request['caChild31'] . ';' . $request['caChild32'] . ';' . $request['caChild33'];
            $opt->child_total =  $childTotal1. ';' . $childTotal2  . ';' .  $childTotal3;
            $opt->col_1 = $request['caFinancialInstitution1'] . ';' . $request['caFinancialInstitution2'] . ';' . $request['caFinancialInstitution3'];
            $opt->col_2 = $request['caTypeOfAccount1'] . ';' . $request['caTypeOfAccount2'] . ';' . $request['caTypeOfAccount3'];
            $opt->col_3 = $request['caValue1'] . ';' . $request['caValue2'] . ';' . $request['caValue3'];
            $opt->col_4 = '';
            if (!$opt->save()) {
                Session::flash('error_message', 'Internal server error');
                return view('errors.503');
            }

            //Insert All Investments
            $type = NetEstate::I;
           
            $childTotal1 = $request['iChild11'] + $request['iChild21'] + $request['iChild31'];
            $childTotal2 = $request['iChild12'] + $request['iChild22'] + $request['iChild32'];
            $childTotal3 = $request['iChild13'] + $request['iChild23'] + $request['iChild33'];

            $opt = new NetEstateOperation();
            $opt->plan_id = $planId;
            $opt->type = $type;
            $opt->property_of = $request['iPropertyOf1'] . ';' . $request['iPropertyOf2'] . ';' . $request['iPropertyOf3'];
            $opt->child_1 = $request['iChild11'] . ';' . $request['iChild12'] . ';' . $request['iChild13'];
            $opt->child_2 = $request['iChild21'] . ';' . $request['iChild22'] . ';' . $request['iChild23'];
            $opt->child_3 = $request['iChild31'] . ';' . $request['iChild32'] . ';' . $request['iChild33'];
            $opt->child_total =  $childTotal1. ';' . $childTotal2  . ';' .  $childTotal3;
            $opt->col_1 = $request['iDescription1'] . ';' . $request['iDescription2'] . ';' . $request['iDescription3'];
            $opt->col_2 = $request['iNoOfSharedUnits1'] . ';' . $request['iNoOfSharedUnits2'] . ';' . $request['iNoOfSharedUnits3'];
            $opt->col_3 = $request['iValue1'] . ';' . $request['iValue2'] . ';' . $request['iValue3'];
            $opt->col_4 = '';

            if (!$opt->save()) {
                Session::flash('error_message', 'Internal server error');
                return view('errors.503');
            }

            //Business Interest
            $type = NetEstate::BI;
           
            $childTotal1 = $request['biChild11'] + $request['biChild21'] + $request['biChild31'];
            $childTotal2 = $request['biChild12'] + $request['biChild22'] + $request['biChild32'];
            $childTotal3 = $request['biChild13'] + $request['biChild23'] + $request['biChild33'];

            $opt = new NetEstateOperation();
            $opt->plan_id = $planId;
            $opt->type = $type;
            $opt->property_of = $request['biPropertyOf1'] . ';' . $request['biPropertyOf2'] . ';' . $request['biPropertyOf3'];
            $opt->child_1 = $request['biChild11'] . ';' . $request['biChild12'] . ';' . $request['biChild13'];
            $opt->child_2 = $request['biChild21'] . ';' . $request['biChild22'] . ';' . $request['biChild23'];
            $opt->child_3 = $request['biChild31'] . ';' . $request['biChild32'] . ';' . $request['biChild33'];
            $opt->child_total =  $childTotal1. ';' . $childTotal2  . ';' .  $childTotal3;
            $opt->col_1 = $request['biNameOfBusiness1'] . ';' . $request['biNameOfBusiness2'] . ';' . $request['biNameOfBusiness3'];
            $opt->col_2 = $request['biTypeOfInterest1'] . ';' . $request['biTypeOfInterest2'] . ';' . $request['biTypeOfInterest3'];
            $opt->col_3 = $request['biValue1'] . ';' . $request['biValue2'] . ';' . $request['biValue3'];
            $opt->col_4 = '';

            if (!$opt->save()) {
                Session::flash('error_message', 'Internal server error');
                return view('errors.503');
            }

            //Real Property
            $type = NetEstate::RP;
           
            $childTotal1 = $request['rpChild11'] + $request['rpChild21'] + $request['rpChild31'];
            $childTotal2 = $request['rpChild12'] + $request['rpChild22'] + $request['rpChild32'];
            $childTotal3 = $request['rpChild13'] + $request['rpChild23'] + $request['rpChild33'];

            $net1 = $request['rpMarketValue1'] - $request['rpMortgage1'];
            $net2 = $request['rpMarketValue2'] - $request['rpMortgage2'];
            $net3 = $request['rpMarketValue3'] - $request['rpMortgage3'];

            $opt = new NetEstateOperation();
            $opt->plan_id = $planId;
            $opt->type = $type;
            $opt->property_of = $request['rpPropertyOf1'] . ';' . $request['rpPropertyOf2'] . ';' . $request['rpPropertyOf3'];
            $opt->child_1 = $request['rpChild11'] . ';' . $request['rpChild12'] . ';' . $request['rpChild13'];
            $opt->child_2 = $request['rpChild21'] . ';' . $request['rpChild22'] . ';' . $request['rpChild23'];
            $opt->child_3 = $request['rpChild31'] . ';' . $request['rpChild32'] . ';' . $request['rpChild33'];
            $opt->child_total =  $childTotal1. ';' . $childTotal2  . ';' .  $childTotal3;
            $opt->col_1 = $request['rpLocation1'] . ';' . $request['rpLocation2'] . ';' . $request['rpLocation3'];
            $opt->col_2 = $request['rpMarketValue1'] . ';' . $request['rpMarketValue1'] . ';' . $request['rpMarketValue3'];
            $opt->col_3 = $request['rpMortgage1'] . ';' . $request['rpMortgage2'] . ';' . $request['rpMortgage3'];
            $opt->col_4 = $net1 . ';' . $net2 . ';' . $net3;

            if (!$opt->save()) {
                Session::flash('error_message', 'Internal server error');
                return view('errors.503');
            }

            //Retired Accounts
            $type = NetEstate::RA;
           
            $childTotal1 = $request['raChild11'] + $request['raChild21'] + $request['raChild31'];
            $childTotal2 = $request['raChild12'] + $request['raChild22'] + $request['raChild32'];
            $childTotal3 = $request['raChild13'] + $request['raChild23'] + $request['raChild33'];

            $opt = new NetEstateOperation();
            $opt->plan_id = $planId;
            $opt->type = $type;
            $opt->property_of = $request['raPropertyOf1'] . ';' . $request['raPropertyOf2'] . ';' . $request['raPropertyOf3'];
            $opt->child_1 = $request['raChild11'] . ';' . $request['raChild12'] . ';' . $request['raChild13'];
            $opt->child_2 = $request['raChild21'] . ';' . $request['raChild22'] . ';' . $request['raChild23'];
            $opt->child_3 = $request['raChild31'] . ';' . $request['raChild32'] . ';' . $request['raChild33'];
            $opt->child_total =  $childTotal1. ';' . $childTotal2  . ';' .  $childTotal3;
            $opt->col_1 = $request['raFinancialInstitution1'] . ';' . $request['raFinancialInstitution2'] . ';' . $request['raFinancialInstitution3'];
            $opt->col_2 = $request['raTypeOfAccount1'] . ';' . $request['raTypeOfAccount2'] . ';' . $request['raTypeOfAccount3'];
            $opt->col_3 = $request['raValue1'] . ';' . $request['raValue2'] . ';' . $request['raValue3'];
            $opt->col_4 = '';

            if (!$opt->save()) {
                Session::flash('error_message', 'Internal server error');
                return view('errors.503');
            }

            //Collectibles
            $type = NetEstate::C;
           
            $childTotal1 = $request['cChild11'] + $request['cChild21'] + $request['cChild31'];
            $childTotal2 = $request['cChild12'] + $request['cChild22'] + $request['cChild32'];
            $childTotal3 = $request['cChild13'] + $request['cChild23'] + $request['cChild33'];

            $opt = new NetEstateOperation();
            $opt->plan_id = $planId;
            $opt->type = $type;
            $opt->property_of = $request['cPropertyOf1'] . ';' . $request['cPropertyOf2'] . ';' . $request['cPropertyOf3'];
            $opt->child_1 = $request['cChild11'] . ';' . $request['cChild12'] . ';' . $request['cChild13'];
            $opt->child_2 = $request['cChild21'] . ';' . $request['cChild22'] . ';' . $request['cChild23'];
            $opt->child_3 = $request['cChild31'] . ';' . $request['cChild32'] . ';' . $request['cChild33'];
            $opt->child_total =  $childTotal1. ';' . $childTotal2  . ';' .  $childTotal3;
            $opt->col_1 = $request['cItem1'] . ';' . $request['cItem2'] . ';' . $request['cItem3'];
            $opt->col_2 = $request['cValue1'] . ';' . $request['cValue2'] . ';' . $request['cValue3'];
            $opt->col_3 = '';
            $opt->col_4 = '';

            if (!$opt->save()) {
                Session::flash('error_message', 'Internal server error');
                return view('errors.503');
            }

            //Personal Property
            $type = NetEstate::PR;
           
            $childTotal1 = $request['ppChild11'] + $request['ppChild21'] + $request['ppChild31'];
            $childTotal2 = $request['ppChild12'] + $request['ppChild22'] + $request['ppChild32'];
            $childTotal3 = $request['ppChild13'] + $request['ppChild23'] + $request['ppChild33'];

            $opt = new NetEstateOperation();
            $opt->plan_id = $planId;
            $opt->type = $type;
            $opt->property_of = $request['ppPropertyOf1'] . ';' . $request['ppPropertyOf2'] . ';' . $request['ppPropertyOf3'];
            $opt->child_1 = $request['ppChild11'] . ';' . $request['ppChild12'] . ';' . $request['ppChild13'];
            $opt->child_2 = $request['ppChild21'] . ';' . $request['ppChild22'] . ';' . $request['ppChild23'];
            $opt->child_3 = $request['ppChild31'] . ';' . $request['ppChild32'] . ';' . $request['ppChild33'];
            $opt->child_total =  $childTotal1. ';' . $childTotal2  . ';' .  $childTotal3;
            $opt->col_1 = $request['ppItem1'] . ';' . $request['ppItem2'] . ';' . $request['ppItem3'];
            $opt->col_2 = $request['ppValue1'] . ';' . $request['ppValue2'] . ';' . $request['ppValue3'];
            $opt->col_3 = '';
            $opt->col_4 = '';

            if (!$opt->save()) {
                Session::flash('error_message', 'Internal server error');
                return view('errors.503');
            }

            // Trust and Annuities
            $type = NetEstate::TA;
           
            $childTotal1 = $request['taChild11'] + $request['taChild21'] + $request['taChild31'];
            $childTotal2 = $request['taChild12'] + $request['taChild22'] + $request['taChild32'];
            $childTotal3 = $request['taChild13'] + $request['taChild23'] + $request['taChild33'];

            $opt = new NetEstateOperation();
            $opt->plan_id = $planId;
            $opt->type = $type;
            $opt->property_of = $request['taPropertyOf1'] . ';' . $request['taPropertyOf2'] . ';' . $request['taPropertyOf3'];
            $opt->child_1 = $request['taChild11'] . ';' . $request['taChild12'] . ';' . $request['taChild13'];
            $opt->child_2 = $request['taChild21'] . ';' . $request['taChild22'] . ';' . $request['taChild23'];
            $opt->child_3 = $request['taChild31'] . ';' . $request['taChild32'] . ';' . $request['taChild33'];
            $opt->child_total =  $childTotal1. ';' . $childTotal2  . ';' .  $childTotal3;
            $opt->col_1 = $request['taName1'] . ';' . $request['taName2'] . ';' . $request['taName3'];
            $opt->col_2 = $request['taType1'] . ';' . $request['taType2'] . ';' . $request['taType3'];
            $opt->col_3 = $request['taValue1'] . ';' . $request['taValue2'] . ';' . $request['taValue3'];
            $opt->col_4 = '';

            if (!$opt->save()) {
                Session::flash('error_message', 'Internal server error');
                return view('errors.503');
            }

            // Life Insurance Death Benefits
            $type = NetEstate::LIDB;
           
            $childTotal1 = $request['lfChild11'] + $request['lfChild21'] + $request['lfChild31'];
            $childTotal2 = $request['lfChild12'] + $request['lfChild22'] + $request['lfChild32'];
            $childTotal3 = $request['lfChild13'] + $request['lfChild23'] + $request['lfChild33'];
            
            $opt = new NetEstateOperation();
            $opt->plan_id = $planId;
            $opt->type = $type;
            $opt->property_of = $request['lfPropertyOf1'] . ';' . $request['lfPropertyOf2'] . ';' . $request['lfPropertyOf3'];
            $opt->child_1 = $request['lfChild11'] . ';' . $request['lfChild12'] . ';' . $request['lfChild13'];
            $opt->child_2 = $request['lfChild21'] . ';' . $request['lfChild22'] . ';' . $request['lfChild23'];
            $opt->child_3 = $request['lfChild31'] . ';' . $request['lfChild32'] . ';' . $request['lfChild33'];
            $opt->child_total =  $childTotal1. ';' . $childTotal2  . ';' .  $childTotal3;
            $opt->col_1 = $request['lfInsuranceGroup1'] . ';' . $request['lfInsuranceGroup2'] . ';' . $request['lfInsuranceGroup3'];
            $opt->col_2 = $request['lfPolicyNo1'] . ';' . $request['lfPolicyNo2'] . ';' . $request['lfPolicyNo3'];
            $opt->col_3 = $request['lfValue1'] . ';' . $request['lfValue2'] . ';' . $request['lfValue3'];
            $opt->col_4 = '';

            if (!$opt->save()) {
                Session::flash('error_message', 'Internal server error');
                return view('errors.503');
            }

            // Other Assets
            $type = NetEstate::OA;
           
            $childTotal1 = $request['oaChild11'] + $request['oaChild21'] + $request['oaChild31'];
            $childTotal2 = $request['oaChild12'] + $request['oaChild22'] + $request['oaChild32'];
            $childTotal3 = $request['oaChild13'] + $request['oaChild23'] + $request['oaChild33'];
            
            $opt = new NetEstateOperation();
            $opt->plan_id = $planId;
            $opt->type = $type;
            $opt->property_of = $request['oaPropertyOf1'] . ';' . $request['oaPropertyOf2'] . ';' . $request['oaPropertyOf3'];
            $opt->child_1 = $request['oaChild11'] . ';' . $request['oaChild12'] . ';' . $request['oaChild13'];
            $opt->child_2 = $request['oaChild21'] . ';' . $request['oaChild22'] . ';' . $request['oaChild23'];
            $opt->child_3 = $request['oaChild31'] . ';' . $request['oaChild32'] . ';' . $request['oaChild33'];
            $opt->child_total =  $childTotal1. ';' . $childTotal2  . ';' .  $childTotal3;
            $opt->col_1 = $request['oaDescription1'] . ';' . $request['oaDescription2'] . ';' . $request['oaDescription3'];
            $opt->col_2 = $request['oaValue1'] . ';' . $request['oaValue2'] . ';' . $request['oaValue3'];
            $opt->col_3 = '';
            $opt->col_4 = '';

            if (!$opt->save()) {
                Session::flash('error_message', 'Internal server error');
                return view('errors.503');
            }

            return redirect('/dashboard/' . str_replace(" ","-",$projectName) . '/net_estate_calculator/preview');
        }
    }

    /**
     * Insert all row in three
     */
    public function netEstatePreview()
    {
        $pageName = Request::route('pageName');
        $projectName = Request::route('projectName');
        $projectName = str_replace("-", " ", $projectName);

        $projectData = Projects::where('user_id', Auth::id())->where('name', $projectName)->first();
        $plan = Plans::getPlanByProjectIdAndProjectName($projectData->id, $projectData->name);
        $data = NetEstate::where(['plan_id' => $plan->id ])->get();
        $page = new NetEstateFiles($data);
        $planData = Plans::where(['id' => $projectData->id ])->first();

        if ($pageName == 'net_estate_calculator') {
            $editedPageChecker = PlanFiles::where([
                'plan_id' => $plan->id
            ])->count();

            $content = $page->$pageName();

        } else {

            Session::flash('error_message', 'Page not found.');
            return view('errors.503');
        }

        $data = [
            'pageName' => $pageName,
            'projectData' => $projectData,
            'pageName' => $pageName,
            'planId' => $plan->id,
            'content' => $content,
            'planData' => $planData
        ];

        return view('dashboard.excelPreview', $data);
    }

    /**
     * Edit Accounts File
     * @return redirect
     */
    public function editAccountsFile()
    {
        if (Request::method('post')) {

            $request = Request::all();
            $subPageName = $request['subPageName'];
            $pageName = $request['pageName'];
            $id = PlanFiles::getFilesByPlanIdAndFileName($pageName . '-' . $subPageName, $request['planId']);

            $opt = new PlanFilesOperation();
            $opt->id = (!empty($id)) ? $id->id : '';
            $opt->plan_id = $request['planId'];
            $opt->file_name = $pageName . '-' . $subPageName;
            $opt->content = $request['content'];
            $opt->type = PlanFiles::EXCEL;

            if (!$opt->save()) {
                Session::flash('error_message', 'Internal server error.');
                return view('errors.503');
            }
            Session::flash('flash_message', 'File has been updated.');

            return redirect("/dashboard/" . $request['projectName'] . "/" . $pageName . "/sub-" . $subPageName);
        }
    }

    /**
     * Account and Contracts Page
     *
     * @return Response
     */
    public function accountPage()
    {
        $pageName = Request::route('pageName');
        $projectName = Request::route('projectName');
        $projectName = str_replace("-", " ", $projectName);
        $subPage = Request::route('subPageName');

        $projectChecker = Projects::where('user_id', Auth::id())->where('name', $projectName)->count();

        if ($projectChecker == 0) {
            Session::flash('error_message', 'Project not found.');
            return view('errors.503');
        }

        $projectData = Projects::where('user_id', Auth::id())->where('name', $projectName)->first();
        $data = Plans::getPlanByProjectIdAndProjectName($projectData->id, $projectData->name);
        $planData = Plans::where(['id' => $projectData->id ])->first();
        $sub = '';

        $content = new AccountsAndContracts();
        if (method_exists($content, $subPage) === true) {

            $file = PlanFiles::getFilesByPlanIdAndFileName($pageName . '-' . $subPage, $data->id);
            if (!empty($file)) {
                $sub = $file->content;
            } else {
                $sub = $content->$subPage();
            }
        } else {
            Session::flash('error_message', 'Sub page not found.');
            return view('errors.503');
        }

        $datas = [
            'pageName' => $pageName,
            'content' => $sub,
            'projectData' => $projectData,
            'pageName' => $pageName,
            'planId' => $data->id,
            'subPage' => $subPage,
            'planData' => $planData
        ];
        return view('dashboard.accounts', $datas);
    }
}

