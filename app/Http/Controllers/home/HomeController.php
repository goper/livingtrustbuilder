<?php 
namespace App\Http\Controllers\home;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use Auth;
use Session;

/*Model*/
use App\Model\Projects;

class HomeController extends Controller {

    /**
     * Home page
     *
     * @return Response
     */
    public function index()
    {
        if (Auth::check()) {
            $userId = Auth::id();
            $planList = Projects::where('user_id', $userId)->get();
            
            $data = [
                'planList' => $planList,
            ];
            return view('index.start', $data);
        } else {
            return view('welcome');
        }
    }

    /**
     * Logout user
     *
     * @return Response
     */
    public function logout()
    {
        Auth::logout();
        Session::flush();
        return redirect('/');
    }

    /**
     * Login user
     */
    public function login(Request $request)
    {
        $email = $request->input('email');
        $password = $request->input('password');

        if (Auth::attempt(['email' => $email, 'password' => $password]))
        {
            return redirect('/getting-started');
        } else {
            $data = [
                'message' => 'This credentials do not match our records',
            ];
            return view('welcome', $data);
        }
    }

}