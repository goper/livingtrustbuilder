<?php

/*
|--------------------------------------------------------------------------
| Application Routes
|--------------------------------------------------------------------------
|
| Here is where you can register all of the routes for an application.
| It's a breeze. Simply tell Laravel the URIs it should respond to
| and give it the controller to call when that URI is requested.
|
*/


Route::post('/login', [
	'uses' => 'home\HomeController@login'
]);

/* Index Controller */
Route::get('/getting-started', [
	'middleware' => 'auth',
	'uses' => 'index\IndexController@start'
]);

Route::get('/new-project', [
	'middleware' => 'auth',
	'uses' => 'index\IndexController@newProject'
]);

Route::post('/create-project', [
	'middleware' => 'auth',
	'uses'			 => 'index\IndexController@createNewProject'
]);

/* Dashboard Controller */
Route::get('/dashboard/{projectName}', [
	'middleware' => 'auth',
	'uses' => 'dashboard\DashBoardController@index'
]);

Route::get('/dashboard/{projectName}/{pageName}', [
	'middleware' => 'auth',
	'uses' => 'dashboard\DashBoardController@viewPage'
]);

Route::post('/edit-file', [
	'middleware' => 'auth',
	'uses' => 'dashboard\DashBoardController@editFile'
]);

Route::post('/edit-accounts', [
	'middleware' => 'auth',
	'uses' => 'dashboard\DashBoardController@editAccountsFile'
]);

Route::post('/save-net-estate', [
	'middleware' => 'auth',
	'uses' => 'dashboard\DashBoardController@editNetEstate'
]);

Route::get('/dashboard/{projectName}/{pageName}/preview', [
	'middleware' => 'auth',
	'uses' => 'dashboard\DashBoardController@netEstatePreview'
]);

Route::get('/dashboard/{projectName}/{pageName}/sub-{subPageName}', [
	'middleware' => 'auth',
	'uses' => 'dashboard\DashBoardController@accountPage'
]);

/*Route::get('/file/{fileName}', [
	'middleware' => 'auth',
	'uses' => 'dashboard\DashBoardController@view'
]);*/

Route::post('/file-save', [
	'middleware' => 'auth',
	'uses' => 'dashboard\DashBoardController@saveFile'
]);







Route::get('/logout', [
	'uses' => 'home\HomeController@logout'
]);

Route::get('/', [
	'uses' => 'home\HomeController@index'
]);

// Routes for AdminController
Route::get('/admin', [
		'middleware' => 'auth',
    'uses' => 'admin\AdminController@index'
]);


// Routes for StaffController
Route::get('/admin/staff', [
		'middleware' => 'auth',
    'uses' => 'admin\StaffController@index'
]);

// Routes for BackupController
Route::get('/admin/back-up', [
		'middleware' => 'auth',
    'uses' => 'admin\BackupCloudController@index'
]);

Route::post('/admin/back-up/save', [
		'middleware' => 'auth',
    'uses' => 'admin\BackupCloudController@save'
]);

Route::post('/download', [
		'middleware' => 'auth',
    'uses' => 'admin\BackupCloudController@download'
]);

// Routes for CloudController
Route::get('/admin/files', [
		'middleware' => 'auth',
    'uses' => 'admin\CloudController@index'
]);