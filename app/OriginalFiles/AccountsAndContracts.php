<?php

namespace App\OriginalFiles;

class AccountsAndContracts {

	/*
	 * Intro Files
	 */
	public function intro()
	{
		$content = '';

		$content = '<h1 style="text-align: center;"><strong>Accounts &amp; Contacts</strong></h1>
		<p><span style="font-size:11px;"><span style="color:#008080;">You can use this handy Excel workbook to summarize all of your &quot;In case of emergency&quot; contacts as well as all account information<br />
		You might think of it as an easy way to keep track of all your important numbers usernames, passwords, and PINs or yourself as well as your heirs.<br />
		Feel free to add &amp; edit as needed - this page is completely customizable for your purposes.<br />
		Perhaps give a copy to your attorney, keep a copy in your wall safe, or otherwise make it available to your executor upon your exit from this world.</span></span></p>

		<table style="width:100%;">
			<tbody>
				<tr>
					<td width="150"><strong>Agreements</strong></td>
					<td>There are certain agreements that I have made that you must be made aware of&hellip;</td>
				</tr>
				<tr>
					<td><strong>Will or Trust</strong></td>
					<td>Copies of your will or trust, including names of the executor and person with power of attorney</td>
				</tr>
				<tr>
					<td><strong>Financial Accounts</strong></td>
					<td>All accounts, including account numbers, online addresses, usernames, passwords and PINs are listed on the Accounts page.</td>
				</tr>
				<tr>
					<td><strong>Insurance Policies</strong></td>
					<td>List all health, life, auto, homeowners&#39; policies, etc. Include who is covered, policy numbers and contact information.</td>
				</tr>
				<tr>
					<td><strong>Funeral Plans</strong></td>
					<td>All instructions should be noted so family can fulfill your wishes. If married, include both spouses&#39; wishes.</td>
				</tr>
				<tr>
					<td><strong>Vital Documents</strong></td>
					<td>Include birth certificates, divorce papers, military and Social Security records, car and boat titles, mortgages and property deeds. .</td>
				</tr>
				<tr>
					<td><strong>Legacy Letters</strong></td>
					<td>Since the intent is to guide your family after you&#39;re gone, include personal notes -or letters to loved ones.</td>
				</tr>
				<tr>
					<td><strong>Monthly Budget</strong></td>
					<td>Add a copy of your budget, including bills to pay, so your family is prepared to handle household expenses.</td>
				</tr>
				<tr>
					<td><strong>Affiliate Relationships</strong></td>
					<td>I have certain links to websites / businesses who pay me a commission on sales I refer to them.</td>
				</tr>
				<tr>
					<td><strong>Tax Returns</strong></td>
					<td>State and federal tax returns.</td>
				</tr>
				<tr>
					<td><strong>Safe Deposit Box</strong></td>
					<td>Indicate where it&#39;s located and who has access. As backup, keep a copy in your box of the legacy drawer&#39;s contents.</td>
				</tr>
				<tr>
					<td><strong>Passwords</strong></td>
					<td>All passwords, user names and PINs are listed on the Accounts page</td>
				</tr>
				<tr>
					<td><strong>Computer Passwords</strong></td>
					<td>Your password.</td>
				</tr>
			</tbody>
		</table>
		';

		return $content;
	}

	public function contacts()
	{
		$content = '<h1 style="text-align: center;"><strong>Contacts </strong></h1>

		<h1><span style="color:#008080;"><span style="font-size:11px;">People I&#39;ve been working and must be contacted.</span></span></h1>

		<table class="text-center" style="width: 100%;">
			<tbody>
				<tr>
					<th width="150">Relationship</th>
					<th width="160">Name</th>
					<th width="100">Telephone</th>
					<th width="90">Email</th>
					<th width="80">City</th>
					<th width="80">State</th>
				</tr>
				<tr>
					<td><span style="font-size:12px;">Account / CPA</span></td>
					<td><span style="font-size:12px;">First Name &amp; Last Name</span></td>
					<td><span style="font-size:12px;">000-000-000-00</span></td>
					<td><span style="font-size:12px;">name@email.com</span></td>
					<td>&nbsp;</td>
					<td>&nbsp;</td>
				</tr>
				<tr>
					<td><span style="font-size:12px;">Best Friend</span></td>
					<td><span style="font-size:12px;">First Name &amp; Last Name</span></td>
					<td><span style="font-size:12px;">000-000-000-00</span></td>
					<td><span style="font-size:12px;">name@email.com</span></td>
					<td></td>
					<td></td>
				</tr>
				<tr>
					<td><span style="font-size:12px;">Business Attorney</span></td>
					<td><span style="font-size:12px;">First Name &amp; Last Name</span></td>
					<td><span style="font-size:12px;">000-000-000-00</span></td>
					<td><span style="font-size:12px;">name@email.com</span></td>
					<td></td>
					<td></td>
				</tr>
				<tr>
					<td><span style="font-size:12px;">Business Partner</span></td>
					<td><span style="font-size:12px;">First Name &amp; Last Name</span></td>
					<td><span style="font-size:12px;">000-000-000-00</span></td>
					<td><span style="font-size:12px;">name@email.com</span></td>
					<td>&nbsp;</td>
					<td>&nbsp;</td>
				</tr>
				<tr>
					<td><span style="font-size:12px;">Chiropractor</span></td>
					<td><span style="font-size:12px;">First Name &amp; Last Name</span></td>
					<td><span style="font-size:12px;">000-000-000-00</span></td>
					<td><span style="font-size:12px;">name@email.com</span></td>
					<td>&nbsp;</td>
					<td>&nbsp;</td>
				</tr>
				<tr>
					<td><span style="font-size:12px;">Account / CPA</span></td>
					<td><span style="font-size:12px;">First Name &amp; Last Name</span></td>
					<td><span style="font-size:12px;">000-000-000-00</span></td>
					<td>&nbsp;</td>
					<td>&nbsp;</td>
					<td>&nbsp;</td>
				</tr>
				<tr>
					<td><span style="font-size:12px;">Gardner</span></td>
					<td><span style="font-size:12px;">First Name &amp; Last Name</span></td>
					<td><span style="font-size:12px;">000-000-000-00</span></td>
					<td>&nbsp;</td>
					<td>&nbsp;</td>
					<td>&nbsp;</td>
				</tr>
				<tr>
					<td><span style="font-size:12px;">Account / CPA</span></td>
					<td><span style="font-size:12px;">First Name &amp; Last Name</span></td>
					<td><span style="font-size:12px;">000-000-000-00</span></td>
					<td>&nbsp;</td>
					<td>&nbsp;</td>
					<td>&nbsp;</td>
				</tr>
				<tr>
					<td><span style="font-size:12px;">Home-Owner Insurance</span></td>
					<td><span style="font-size:12px;">First Name &amp; Last Name</span></td>
					<td><span style="font-size:12px;">000-000-000-00</span></td>
					<td>&nbsp;</td>
					<td>&nbsp;</td>
					<td>&nbsp;</td>
				</tr>
				<tr>
					<td><span style="font-size:12px;">Housekeeper</span></td>
					<td><span style="font-size:12px;">First Name &amp; Last Name</span></td>
					<td><span style="font-size:12px;">000-000-000-00</span></td>
					<td>&nbsp;</td>
					<td>&nbsp;</td>
					<td>&nbsp;</td>
				</tr>
				<tr>
					<td><span style="font-size:12px;">Investment Advisor</span></td>
					<td><span style="font-size:12px;">First Name &amp; Last Name</span></td>
					<td><span style="font-size:12px;">000-000-000-00</span></td>
					<td>&nbsp;</td>
					<td>&nbsp;</td>
					<td>&nbsp;</td>
				</tr>
				<tr>
					<td><span style="font-size:12px;">Life Insurance</span></td>
					<td><span style="font-size:12px;">First Name &amp; Last Name</span></td>
					<td><span style="font-size:12px;">000-000-000-00</span></td>
					<td>&nbsp;</td>
					<td>&nbsp;</td>
					<td>&nbsp;</td>
				</tr>
				<tr>
					<td><span style="font-size:12px;">Mechanic</span></td>
					<td><span style="font-size:12px;">First Name &amp; Last Name</span></td>
					<td><span style="font-size:12px;">000-000-000-00</span></td>
					<td>&nbsp;</td>
					<td>&nbsp;</td>
					<td>&nbsp;</td>
				</tr>
				<tr>
					<td><span style="font-size:12px;">Personal Attorney</span></td>
					<td><span style="font-size:12px;">First Name &amp; Last Name</span></td>
					<td><span style="font-size:12px;">000-000-000-00</span></td>
					<td>&nbsp;</td>
					<td>&nbsp;</td>
					<td>&nbsp;</td>
				</tr>
				<tr>
					<td><span style="font-size:12px;">Storage</span></td>
					<td><span style="font-size:12px;">First Name &amp; Last Name</span></td>
					<td><span style="font-size:12px;">000-000-000-00</span></td>
					<td>&nbsp;</td>
					<td>&nbsp;</td>
					<td>&nbsp;</td>
				</tr>
			</tbody>
		</table>
		';

		return $content;
	}

	public function accounts()
	{
		$content = '<p style="text-align: center;"><strong><span style="font-size:18px;">Accounts </span></strong></p>

		<p><span style="color:#008080;"><span style="font-size:11px;">Access to all of my financial resources.</span></span></p>

		<table style="width:100%;">
			<tbody>
				<tr>
					<th width="150">&nbsp;</th>
					<th width=""><span style="font-size:11px;">Ownership</span></th>
					<th width=""><span style="font-size:11px;">Account No.</span></th>
					<th width=""><span style="font-size:11px;">Web Address</span></th>
					<th width=""><span style="font-size:11px;">UserName</span></th>
					<th width=""><span style="font-size:11px;">Password</span></th>
					<th width=""><span style="font-size:11px;">PIN</span></th>
					<th width=""><span style="font-size:11px;">Expiry</span></th>
					<th width=""><span style="font-size:11px;">Description</span></th>
					<th width=""><span style="font-size:11px;">Notes</span></th>
				</tr>
				<tr>
					<td><span style="font-size:11px;">Google Analytics</span></td>
					<td>&nbsp;</td>
					<td>&nbsp;</td>
					<td>&nbsp;</td>
					<td>&nbsp;</td>
					<td>&nbsp;</td>
					<td>&nbsp;</td>
					<td>&nbsp;</td>
					<td>&nbsp;</td>
					<td>&nbsp;</td>
				</tr>
				<tr>
					<td><span style="font-size:11px;">Network Solutions</span></td>
					<td>&nbsp;</td>
					<td>&nbsp;</td>
					<td>&nbsp;</td>
					<td>&nbsp;</td>
					<td>&nbsp;</td>
					<td>&nbsp;</td>
					<td>&nbsp;</td>
					<td>&nbsp;</td>
					<td>&nbsp;</td>
				</tr>
				<tr>
					<td><span style="font-size:11px;">Authorize.net</span></td>
					<td>&nbsp;</td>
					<td>&nbsp;</td>
					<td>&nbsp;</td>
					<td>&nbsp;</td>
					<td>&nbsp;</td>
					<td>&nbsp;</td>
					<td>&nbsp;</td>
					<td>&nbsp;</td>
					<td>&nbsp;</td>
				</tr>
				<tr>
					<td>&nbsp;</td>
					<td>&nbsp;</td>
					<td>&nbsp;</td>
					<td>&nbsp;</td>
					<td>&nbsp;</td>
					<td>&nbsp;</td>
					<td>&nbsp;</td>
					<td>&nbsp;</td>
					<td>&nbsp;</td>
					<td>&nbsp;</td>
				</tr>
				<tr>
					<td>&nbsp;</td>
					<td>&nbsp;</td>
					<td>&nbsp;</td>
					<td>&nbsp;</td>
					<td>&nbsp;</td>
					<td>&nbsp;</td>
					<td>&nbsp;</td>
					<td>&nbsp;</td>
					<td>&nbsp;</td>
					<td>&nbsp;</td>
				</tr>
				<tr>
					<td>&nbsp;</td>
					<td>&nbsp;</td>
					<td>&nbsp;</td>
					<td>&nbsp;</td>
					<td>&nbsp;</td>
					<td>&nbsp;</td>
					<td>&nbsp;</td>
					<td>&nbsp;</td>
					<td>&nbsp;</td>
					<td>&nbsp;</td>
				</tr>
			</tbody>
		</table>

		<p><span style="font-size:14px;"><strong>Bank Accounts</strong></span></p>

		<table style="width:100%;">
			<tbody>
				<tr>
					<th width="150">&nbsp;</th>
					<th width=""><span style="font-size:11px;">Ownership</span></th>
					<th width=""><span style="font-size:11px;">Account No.</span></th>
					<th width=""><span style="font-size:11px;">Web Address</span></th>
					<th width=""><span style="font-size:11px;">UserName</span></th>
					<th width=""><span style="font-size:11px;">Password</span></th>
					<th width=""><span style="font-size:11px;">PIN</span></th>
					<th width=""><span style="font-size:11px;">Expiry</span></th>
					<th width=""><span style="font-size:11px;">Description</span></th>
					<th width=""><span style="font-size:11px;">Notes</span></th>
				</tr>
				<tr>
					<td><span style="font-size:11px;">Bank Name</span></td>
					<td>&nbsp;</td>
					<td>&nbsp;</td>
					<td>&nbsp;</td>
					<td>&nbsp;</td>
					<td>&nbsp;</td>
					<td>&nbsp;</td>
					<td>&nbsp;</td>
					<td>&nbsp;</td>
					<td>&nbsp;</td>
				</tr>
				<tr>
					<td><span style="font-size:11px;">Bank Name</span></td>
					<td>&nbsp;</td>
					<td>&nbsp;</td>
					<td>&nbsp;</td>
					<td>&nbsp;</td>
					<td>&nbsp;</td>
					<td>&nbsp;</td>
					<td>&nbsp;</td>
					<td>&nbsp;</td>
					<td>&nbsp;</td>
				</tr>
				<tr>
					<td><span style="font-size:11px;">Bank Name</span></td>
					<td>&nbsp;</td>
					<td>&nbsp;</td>
					<td>&nbsp;</td>
					<td>&nbsp;</td>
					<td>&nbsp;</td>
					<td>&nbsp;</td>
					<td>&nbsp;</td>
					<td>&nbsp;</td>
					<td>&nbsp;</td>
				</tr>
				<tr>
					<td>&nbsp;</td>
					<td>&nbsp;</td>
					<td>&nbsp;</td>
					<td>&nbsp;</td>
					<td>&nbsp;</td>
					<td>&nbsp;</td>
					<td>&nbsp;</td>
					<td>&nbsp;</td>
					<td>&nbsp;</td>
					<td>&nbsp;</td>
				</tr>
				<tr>
					<td><span style="font-size:11px;">PayPal - Jian</span></td>
					<td>&nbsp;</td>
					<td>&nbsp;</td>
					<td>&nbsp;</td>
					<td>&nbsp;</td>
					<td>&nbsp;</td>
					<td>&nbsp;</td>
					<td>&nbsp;</td>
					<td>&nbsp;</td>
					<td>&nbsp;</td>
				</tr>
				<tr>
					<td><span style="font-size:11px;">Safe Deposit Box</span></td>
					<td>&nbsp;</td>
					<td>&nbsp;</td>
					<td>&nbsp;</td>
					<td>&nbsp;</td>
					<td>&nbsp;</td>
					<td>&nbsp;</td>
					<td>&nbsp;</td>
					<td>&nbsp;</td>
					<td>&nbsp;</td>
				</tr>
			</tbody>
		</table>

		<p><span style="font-size:14px;"><strong>Credit Cards</strong></span></p>

		<table style="width:100%;">
			<tbody>
				<tr>
					<th width="150">&nbsp;</th>
					<th width=""><span style="font-size:11px;">Ownership</span></th>
					<th width=""><span style="font-size:11px;">Account No.</span></th>
					<th width=""><span style="font-size:11px;">Web Address</span></th>
					<th width=""><span style="font-size:11px;">UserName</span></th>
					<th width=""><span style="font-size:11px;">Password</span></th>
					<th width=""><span style="font-size:11px;">PIN</span></th>
					<th width=""><span style="font-size:11px;">Expiry</span></th>
					<th width=""><span style="font-size:11px;">Description</span></th>
					<th width=""><span style="font-size:11px;">Notes</span></th>
				</tr>
				<tr>
					<td><span style="font-size:11px;">American Express</span></td>
					<td>&nbsp;</td>
					<td>&nbsp;</td>
					<td>&nbsp;</td>
					<td>&nbsp;</td>
					<td>&nbsp;</td>
					<td>&nbsp;</td>
					<td>&nbsp;</td>
					<td>&nbsp;</td>
					<td>&nbsp;</td>
				</tr>
				<tr>
					<td><span style="font-size:11px;">Bank of America</span></td>
					<td>&nbsp;</td>
					<td>&nbsp;</td>
					<td>&nbsp;</td>
					<td>&nbsp;</td>
					<td>&nbsp;</td>
					<td>&nbsp;</td>
					<td>&nbsp;</td>
					<td>&nbsp;</td>
					<td>&nbsp;</td>
				</tr>
				<tr>
					<td><span style="font-size:11px;">Chase</span></td>
					<td>&nbsp;</td>
					<td>&nbsp;</td>
					<td>&nbsp;</td>
					<td>&nbsp;</td>
					<td>&nbsp;</td>
					<td>&nbsp;</td>
					<td>&nbsp;</td>
					<td>&nbsp;</td>
					<td>&nbsp;</td>
				</tr>
				<tr>
					<td>&nbsp;</td>
					<td>&nbsp;</td>
					<td>&nbsp;</td>
					<td>&nbsp;</td>
					<td>&nbsp;</td>
					<td>&nbsp;</td>
					<td>&nbsp;</td>
					<td>&nbsp;</td>
					<td>&nbsp;</td>
					<td>&nbsp;</td>
				</tr>
				<tr>
					<td>&nbsp;</td>
					<td>&nbsp;</td>
					<td>&nbsp;</td>
					<td>&nbsp;</td>
					<td>&nbsp;</td>
					<td>&nbsp;</td>
					<td>&nbsp;</td>
					<td>&nbsp;</td>
					<td>&nbsp;</td>
					<td>&nbsp;</td>
				</tr>
				<tr>
					<td>&nbsp;</td>
					<td>&nbsp;</td>
					<td>&nbsp;</td>
					<td>&nbsp;</td>
					<td>&nbsp;</td>
					<td>&nbsp;</td>
					<td>&nbsp;</td>
					<td>&nbsp;</td>
					<td>&nbsp;</td>
					<td>&nbsp;</td>
				</tr>
			</tbody>
		</table>

		<p><span style="font-size:14px;"><strong>Retail</strong></span></p>

		<table style="width:100%;">
			<tbody>
				<tr>
					<th width="150">&nbsp;</th>
					<th width=""><span style="font-size:11px;">Ownership</span></th>
					<th width=""><span style="font-size:11px;">Account No.</span></th>
					<th width=""><span style="font-size:11px;">Web Address</span></th>
					<th width=""><span style="font-size:11px;">UserName</span></th>
					<th width=""><span style="font-size:11px;">Password</span></th>
					<th width=""><span style="font-size:11px;">PIN</span></th>
					<th width=""><span style="font-size:11px;">Expiry</span></th>
					<th width=""><span style="font-size:11px;">Description</span></th>
					<th width=""><span style="font-size:11px;">Notes</span></th>
				</tr>
				<tr>
					<td><span style="font-size:11px;">Best Buy</span></td>
					<td>&nbsp;</td>
					<td>&nbsp;</td>
					<td>&nbsp;</td>
					<td>&nbsp;</td>
					<td>&nbsp;</td>
					<td>&nbsp;</td>
					<td>&nbsp;</td>
					<td>&nbsp;</td>
					<td>&nbsp;</td>
				</tr>
				<tr>
					<td><span style="font-size:11px;">Macys</span></td>
					<td>&nbsp;</td>
					<td>&nbsp;</td>
					<td>&nbsp;</td>
					<td>&nbsp;</td>
					<td>&nbsp;</td>
					<td>&nbsp;</td>
					<td>&nbsp;</td>
					<td>&nbsp;</td>
					<td>&nbsp;</td>
				</tr>
				<tr>
					<td><span style="font-size:11px;">Chevron</span></td>
					<td>&nbsp;</td>
					<td>&nbsp;</td>
					<td>&nbsp;</td>
					<td>&nbsp;</td>
					<td>&nbsp;</td>
					<td>&nbsp;</td>
					<td>&nbsp;</td>
					<td>&nbsp;</td>
					<td>&nbsp;</td>
				</tr>
				<tr>
					<td>&nbsp;</td>
					<td>&nbsp;</td>
					<td>&nbsp;</td>
					<td>&nbsp;</td>
					<td>&nbsp;</td>
					<td>&nbsp;</td>
					<td>&nbsp;</td>
					<td>&nbsp;</td>
					<td>&nbsp;</td>
					<td>&nbsp;</td>
				</tr>
				<tr>
					<td>&nbsp;</td>
					<td>&nbsp;</td>
					<td>&nbsp;</td>
					<td>&nbsp;</td>
					<td>&nbsp;</td>
					<td>&nbsp;</td>
					<td>&nbsp;</td>
					<td>&nbsp;</td>
					<td>&nbsp;</td>
					<td>&nbsp;</td>
				</tr>
			</tbody>
		</table>

		<p><span style="font-size:14px;"><strong>Insurance Policies</strong></span></p>

		<table style="width:100%;">
			<tbody>
				<tr>
					<th width="150">&nbsp;</th>
					<th width=""><span style="font-size:11px;">Ownership</span></th>
					<th width=""><span style="font-size:11px;">Account No.</span></th>
					<th width=""><span style="font-size:11px;">Web Address</span></th>
					<th width=""><span style="font-size:11px;">UserName</span></th>
					<th width=""><span style="font-size:11px;">Password</span></th>
					<th width=""><span style="font-size:11px;">PIN</span></th>
					<th width=""><span style="font-size:11px;">Expiry</span></th>
					<th width=""><span style="font-size:11px;">Description</span></th>
					<th width=""><span style="font-size:11px;">Notes</span></th>
				</tr>
				<tr>
					<td><span style="font-size:11px;">Life - Lafayette Life</span></td>
					<td>&nbsp;</td>
					<td>&nbsp;</td>
					<td>&nbsp;</td>
					<td>&nbsp;</td>
					<td>&nbsp;</td>
					<td>&nbsp;</td>
					<td>&nbsp;</td>
					<td>&nbsp;</td>
					<td>&nbsp;</td>
				</tr>
				<tr>
					<td><span style="font-size:11px;">Health - Aetna</span></td>
					<td>&nbsp;</td>
					<td>&nbsp;</td>
					<td>&nbsp;</td>
					<td>&nbsp;</td>
					<td>&nbsp;</td>
					<td>&nbsp;</td>
					<td>&nbsp;</td>
					<td>&nbsp;</td>
					<td>&nbsp;</td>
				</tr>
				<tr>
					<td><span style="font-size:11px;">Supplemental - AFLAC</span></td>
					<td>&nbsp;</td>
					<td>&nbsp;</td>
					<td>&nbsp;</td>
					<td>&nbsp;</td>
					<td>&nbsp;</td>
					<td>&nbsp;</td>
					<td>&nbsp;</td>
					<td>&nbsp;</td>
					<td>&nbsp;</td>
				</tr>
				<tr>
					<td>&nbsp;</td>
					<td>&nbsp;</td>
					<td>&nbsp;</td>
					<td>&nbsp;</td>
					<td>&nbsp;</td>
					<td>&nbsp;</td>
					<td>&nbsp;</td>
					<td>&nbsp;</td>
					<td>&nbsp;</td>
					<td>&nbsp;</td>
				</tr>
				<tr>
					<td>&nbsp;</td>
					<td>&nbsp;</td>
					<td>&nbsp;</td>
					<td>&nbsp;</td>
					<td>&nbsp;</td>
					<td>&nbsp;</td>
					<td>&nbsp;</td>
					<td>&nbsp;</td>
					<td>&nbsp;</td>
					<td>&nbsp;</td>
				</tr>
				<tr>
					<td>&nbsp;</td>
					<td>&nbsp;</td>
					<td>&nbsp;</td>
					<td>&nbsp;</td>
					<td>&nbsp;</td>
					<td>&nbsp;</td>
					<td>&nbsp;</td>
					<td>&nbsp;</td>
					<td>&nbsp;</td>
					<td>&nbsp;</td>
				</tr>
			</tbody>
		</table>

		<p><span style="font-size:14px;"><strong>Combination Locks</strong></span></p>

		<table style="width:100%;">
			<tbody>
				<tr>
					<th width="150">&nbsp;</th>
					<th width=""><span style="font-size:11px;">Ownership</span></th>
					<th width=""><span style="font-size:11px;">Account No.</span></th>
					<th width=""><span style="font-size:11px;">Web Address</span></th>
					<th width=""><span style="font-size:11px;">UserName</span></th>
					<th width=""><span style="font-size:11px;">Password</span></th>
					<th width=""><span style="font-size:11px;">PIN</span></th>
					<th width=""><span style="font-size:11px;">Expiry</span></th>
					<th width=""><span style="font-size:11px;">Description</span></th>
					<th width=""><span style="font-size:11px;">Notes</span></th>
				</tr>
				<tr>
					<td><span style="font-size:11px;">Wall Safe</span></td>
					<td>&nbsp;</td>
					<td>&nbsp;</td>
					<td>&nbsp;</td>
					<td>&nbsp;</td>
					<td>&nbsp;</td>
					<td>&nbsp;</td>
					<td>&nbsp;</td>
					<td>&nbsp;</td>
					<td>&nbsp;</td>
				</tr>
				<tr>
					<td><span style="font-size:11px;">Gun Safe</span></td>
					<td>&nbsp;</td>
					<td>&nbsp;</td>
					<td>&nbsp;</td>
					<td>&nbsp;</td>
					<td>&nbsp;</td>
					<td>&nbsp;</td>
					<td>&nbsp;</td>
					<td>&nbsp;</td>
					<td>&nbsp;</td>
				</tr>
				<tr>
					<td><span style="font-size:11px;">Gate Padlock</span></td>
					<td>&nbsp;</td>
					<td>&nbsp;</td>
					<td>&nbsp;</td>
					<td>&nbsp;</td>
					<td>&nbsp;</td>
					<td>&nbsp;</td>
					<td>&nbsp;</td>
					<td>&nbsp;</td>
					<td>&nbsp;</td>
				</tr>
				<tr>
					<td>&nbsp;</td>
					<td>&nbsp;</td>
					<td>&nbsp;</td>
					<td>&nbsp;</td>
					<td>&nbsp;</td>
					<td>&nbsp;</td>
					<td>&nbsp;</td>
					<td>&nbsp;</td>
					<td>&nbsp;</td>
					<td>&nbsp;</td>
				</tr>
				<tr>
					<td>&nbsp;</td>
					<td>&nbsp;</td>
					<td>&nbsp;</td>
					<td>&nbsp;</td>
					<td>&nbsp;</td>
					<td>&nbsp;</td>
					<td>&nbsp;</td>
					<td>&nbsp;</td>
					<td>&nbsp;</td>
					<td>&nbsp;</td>
				</tr>
				<tr>
					<td>&nbsp;</td>
					<td>&nbsp;</td>
					<td>&nbsp;</td>
					<td>&nbsp;</td>
					<td>&nbsp;</td>
					<td>&nbsp;</td>
					<td>&nbsp;</td>
					<td>&nbsp;</td>
					<td>&nbsp;</td>
					<td>&nbsp;</td>
				</tr>
			</tbody>
		</table>

		<p><span style="font-size:14px;"><strong>Software Access</strong></span></p>

		<table style="width:100%;">
			<tbody>
				<tr>
					<th width="150">&nbsp;</th>
					<th width=""><span style="font-size:11px;">Ownership</span></th>
					<th width=""><span style="font-size:11px;">Account No.</span></th>
					<th width=""><span style="font-size:11px;">Web Address</span></th>
					<th width=""><span style="font-size:11px;">UserName</span></th>
					<th width=""><span style="font-size:11px;">Password</span></th>
					<th width=""><span style="font-size:11px;">PIN</span></th>
					<th width=""><span style="font-size:11px;">Expiry</span></th>
					<th width=""><span style="font-size:11px;">Description</span></th>
					<th width=""><span style="font-size:11px;">Notes</span></th>
				</tr>
				<tr>
					<td><span style="font-size:11px;">Quickbooks</span></td>
					<td>&nbsp;</td>
					<td>&nbsp;</td>
					<td>&nbsp;</td>
					<td>&nbsp;</td>
					<td>&nbsp;</td>
					<td>&nbsp;</td>
					<td>&nbsp;</td>
					<td>&nbsp;</td>
					<td>&nbsp;</td>
				</tr>
				<tr>
					<td>&nbsp;</td>
					<td>&nbsp;</td>
					<td>&nbsp;</td>
					<td>&nbsp;</td>
					<td>&nbsp;</td>
					<td>&nbsp;</td>
					<td>&nbsp;</td>
					<td>&nbsp;</td>
					<td>&nbsp;</td>
					<td>&nbsp;</td>
				</tr>
				<tr>
					<td>&nbsp;</td>
					<td>&nbsp;</td>
					<td>&nbsp;</td>
					<td>&nbsp;</td>
					<td>&nbsp;</td>
					<td>&nbsp;</td>
					<td>&nbsp;</td>
					<td>&nbsp;</td>
					<td>&nbsp;</td>
					<td>&nbsp;</td>
				</tr>
			</tbody>
		</table>
		';

		return $content;
	}

	public function agreements()
	{
		$content = '<h1 style="text-align: center;"><strong>Agreements </strong></h1>

		<p><span style="color:#008080;"><span style="font-size:11px;">Handshake deals I&#39;ve made with friends...</span></span></p>

		<table>
			<tbody>
				<tr>
					<th width="100">Contact</th>
					<th width="150">Our Deal</th>
					<th width="90">Phone</th>
					<th width="100">Email</th>
					<th width="160">Document Location</th>
					<th width="200">Instructions</th>
				</tr>
				<tr>
					<td>&nbsp;</td>
					<td>&nbsp;</td>
					<td>&nbsp;</td>
					<td>&nbsp;</td>
					<td>&nbsp;</td>
					<td>&nbsp;</td>
				</tr>
				<tr>
					<td>&nbsp;</td>
					<td>&nbsp;</td>
					<td>&nbsp;</td>
					<td>&nbsp;</td>
					<td>&nbsp;</td>
					<td>&nbsp;</td>
				</tr>
				<tr>
					<td>&nbsp;</td>
					<td>&nbsp;</td>
					<td>&nbsp;</td>
					<td>&nbsp;</td>
					<td>&nbsp;</td>
					<td>&nbsp;</td>
				</tr>
				<tr>
					<td>&nbsp;</td>
					<td>&nbsp;</td>
					<td>&nbsp;</td>
					<td>&nbsp;</td>
					<td>&nbsp;</td>
					<td>&nbsp;</td>
				</tr>
				<tr>
					<td>&nbsp;</td>
					<td>&nbsp;</td>
					<td>&nbsp;</td>
					<td>&nbsp;</td>
					<td>&nbsp;</td>
					<td>&nbsp;</td>
				</tr>
				<tr>
					<td>&nbsp;</td>
					<td>&nbsp;</td>
					<td>&nbsp;</td>
					<td>&nbsp;</td>
					<td>&nbsp;</td>
					<td>&nbsp;</td>
				</tr>
				<tr>
					<td>&nbsp;</td>
					<td>&nbsp;</td>
					<td>&nbsp;</td>
					<td>&nbsp;</td>
					<td>&nbsp;</td>
					<td>&nbsp;</td>
				</tr>
			</tbody>
		</table>
		';

		return $content;
	}

	public function affiliates()
	{
		$content = '<p>Affiliates I have certain links to websites/business who pay me a commision on sales I refer to them.</p>

		<table style="width:100%">
			<tbody>
				<tr>
					<th width="100">Affiliate</th>
					<th width="160">Link</th>
					<th width="90">User Name</th>
					<th width="90">Password</th>
					<th width="160">Comments</th>
					<th width="110">Contacts</th>
				</tr>
				<tr>
					<td>&nbsp;</td>
					<td>&nbsp;</td>
					<td>&nbsp;</td>
					<td>&nbsp;</td>
					<td>&nbsp;</td>
					<td>&nbsp;</td>
				</tr>
				<tr>
					<td>&nbsp;</td>
					<td>&nbsp;</td>
					<td>&nbsp;</td>
					<td>&nbsp;</td>
					<td>&nbsp;</td>
					<td>&nbsp;</td>
				</tr>
				<tr>
					<td>&nbsp;</td>
					<td>&nbsp;</td>
					<td>&nbsp;</td>
					<td>&nbsp;</td>
					<td>&nbsp;</td>
					<td>&nbsp;</td>
				</tr>
				<tr>
					<td>&nbsp;</td>
					<td>&nbsp;</td>
					<td>&nbsp;</td>
					<td>&nbsp;</td>
					<td>&nbsp;</td>
					<td>&nbsp;</td>
				</tr>
				<tr>
					<td>&nbsp;</td>
					<td>&nbsp;</td>
					<td>&nbsp;</td>
					<td>&nbsp;</td>
					<td>&nbsp;</td>
					<td>&nbsp;</td>
				</tr>
				<tr>
					<td>&nbsp;</td>
					<td>&nbsp;</td>
					<td>&nbsp;</td>
					<td>&nbsp;</td>
					<td>&nbsp;</td>
					<td>&nbsp;</td>
				</tr>
				<tr>
					<td>&nbsp;</td>
					<td>&nbsp;</td>
					<td>&nbsp;</td>
					<td>&nbsp;</td>
					<td>&nbsp;</td>
					<td>&nbsp;</td>
				</tr>
			</tbody>
		</table>
		';

		return $content;
	}

	public function funeral_instructions()
	{
		$content = '<h1 style="text-align: center;"><strong>Funeral Instructions</strong></h1>

		<p>1. Here is an example.</p>

		<p>2. I want an [xxx] beach service, scattered over [xxx] beach.</p>

		<p>3. I want to be skyrocketed in fireworks. Orbit = gods spirit home.</p>

		<p>4. Special instructions.</p>

		<p>5. No dull church. Under montage resort or old treasure island.</p>

		<p>6. Gather on beach.</p>

		<p>7. Add this up in your preferences.</p>
		';

		return $content;
	}
}