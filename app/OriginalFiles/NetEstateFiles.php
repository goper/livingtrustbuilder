<?php

namespace App\OriginalFiles;

class NetEstateFiles {

  public $baFi1;
  public $baFi2;
  public $baFi3;
  public $baType1;
  public $baType2;
  public $baType3;
  public $baValue1;
  public $baValue2;
  public $baValue3;
  public $baProperty1;
  public $baProperty2;
  public $baProperty3;
  public $baChild11;
  public $baChild12;
  public $baChild13;
  public $baChild21;
  public $baChild22;
  public $baChild23;
  public $baChild31;
  public $baChild32;
  public $baChild33;
  public $baChildTotal1;
  public $baChildTotal2;
  public $baChildTotal3;
  public $baCol11;
  public $baCol21;
  public $baCol31;
  public $baCol12;
  public $baCol22;
  public $baCol32;
  public $baCol13;
  public $baCol23;
  public $baCol33;

  /*Cash Accounts*/
  public $caProperty1;
  public $caProperty2;
  public $caProperty3;
  public $caChild11;
  public $caChild12;
  public $caChild13;
  public $caChild21;
  public $caChild22;
  public $caChild23;
  public $caChild31;
  public $caChild32;
  public $caChild33;
  public $caChildTotal1;
  public $caChildTotal2;
  public $caChildTotal3;
  public $caCol11;
  public $caCol21;
  public $caCol31;
  public $caCol12;
  public $caCol22;
  public $caCol32;
  public $caCol13;
  public $caCol23;
  public $caCol33;

  /*Investments*/
  public $iProperty1;
  public $iProperty2;
  public $iProperty3;
  public $iChild11;
  public $iChild12;
  public $iChild13;
  public $iChild21;
  public $iChild22;
  public $iChild23;
  public $iChild31;
  public $iChild32;
  public $iChild33;
  public $iChildTotal1;
  public $iChildTotal2;
  public $iChildTotal3;
  public $iCol11;
  public $iCol21;
  public $iCol31;
  public $iCol12;
  public $iCol22;
  public $iCol32;
  public $iCol13;
  public $iCol23;
  public $iCol33;

  /*Business Interests*/
  public $biProperty1;
  public $biProperty2;
  public $biProperty3;
  public $biChild11;
  public $biChild12;
  public $biChild13;
  public $biChild21;
  public $biChild22;
  public $biChild23;
  public $biChild31;
  public $biChild32;
  public $biChild33;
  public $biChildTotal1;
  public $biChildTotal2;
  public $biChildTotal3;
  public $biCol11;
  public $biCol21;
  public $biCol31;
  public $biCol12;
  public $biCol22;
  public $biCol32;
  public $biCol13;
  public $biCol23;
  public $biCol33;

  /*Real Property*/
  public $rpProperty1;
  public $rpProperty2;
  public $rpProperty3;
  public $rpChild11;
  public $rpChild12;
  public $rpChild13;
  public $rpChild21;
  public $rpChild22;
  public $rpChild23;
  public $rpChild31;
  public $rpChild32;
  public $rpChild33;
  public $rpChildTotal1;
  public $rpChildTotal2;
  public $rpChildTotal3;
  public $rpCol11;
  public $rpCol21;
  public $rpCol31;
  public $rpCol41;
  public $rpCol12;
  public $rpCol22;
  public $rpCol32;
  public $rpCol42;
  public $rpCol13;
  public $rpCol23;
  public $rpCol33;
  public $rpCol43;

  /*Retired Accounts*/
  public $raProperty1;
  public $raProperty2;
  public $raProperty3;
  public $raChild11;
  public $raChild12;
  public $raChild13;
  public $raChild21;
  public $raChild22;
  public $raChild23;
  public $raChild31;
  public $raChild32;
  public $raChild33;
  public $raChildTotal1;
  public $raChildTotal2;
  public $raChildTotal3;
  public $raCol11;
  public $raCol21;
  public $raCol31;
  public $raCol12;
  public $raCol22;
  public $raCol32;
  public $raCol13;
  public $raCol23;
  public $raCol33;

  /*Collectibles*/
  public $cProperty1;
  public $cProperty2;
  public $cProperty3;
  public $cChild11;
  public $cChild12;
  public $cChild13;
  public $cChild21;
  public $cChild22;
  public $cChild23;
  public $cChild31;
  public $cChild32;
  public $cChild33;
  public $cChildTotal1;
  public $cChildTotal2;
  public $cChildTotal3;
  public $cCol11;
  public $cCol21;
  public $cCol12;
  public $cCol22;
  public $cCol13;
  public $cCol23;

  /*Personal Property*/
  public $ppProperty1;
  public $ppProperty2;
  public $ppProperty3;
  public $ppChild11;
  public $ppChild12;
  public $ppChild13;
  public $ppChild21;
  public $ppChild22;
  public $ppChild23;
  public $ppChild31;
  public $ppChild32;
  public $ppChild33;
  public $ppChildTotal1;
  public $ppChildTotal2;
  public $ppChildTotal3;
  public $ppCol11;
  public $ppCol21;
  public $ppCol12;
  public $ppCol22;
  public $ppCol13;
  public $ppCol23;

  /*Trust and Annuities*/
  public $taProperty1;
  public $taProperty2;
  public $taProperty3;
  public $taChild11;
  public $taChild12;
  public $taChild13;
  public $taChild21;
  public $taChild22;
  public $taChild23;
  public $taChild31;
  public $taChild32;
  public $taChild33;
  public $taChildTotal1;
  public $taChildTotal2;
  public $taChildTotal3;
  public $taCol11;
  public $taCol21;
  public $taCol31;
  public $taCol12;
  public $taCol22;
  public $taCol32;
  public $taCol13;
  public $taCol23;
  public $taCol33;

  /*Life Insurance Death Benefits*/
  public $lfProperty1;
  public $lfProperty2;
  public $lfProperty3;
  public $lfChild11;
  public $lfChild12;
  public $lfChild13;
  public $lfChild21;
  public $lfChild22;
  public $lfChild23;
  public $lfChild31;
  public $lfChild32;
  public $lfChild33;
  public $lfChildTotal1;
  public $lfChildTotal2;
  public $lfChildTotal3;
  public $lfCol11;
  public $lfCol21;
  public $lfCol31;
  public $lfCol12;
  public $lfCol22;
  public $lfCol32;
  public $lfCol13;
  public $lfCol23;
  public $lfCol33;

  /*Other Assets*/
  public $oaProperty1;
  public $oaProperty2;
  public $oaProperty3;
  public $oaChild11;
  public $oaChild12;
  public $oaChild13;
  public $oaChild21;
  public $oaChild22;
  public $oaChild23;
  public $oaChild31;
  public $oaChild32;
  public $oaChild33;
  public $oaChildTotal1;
  public $oaChildTotal2;
  public $oaChildTotal3;
  public $oaCol11;
  public $oaCol21;
  public $oaCol12;
  public $oaCol22;
  public $oaCol13;
  public $oaCol23;

  /*SUMMARY*/
  public $baTotalH = 0;
  public $baTotalW = 0;
  public $baTotalJ = 0;

  public $caTotalH = 0;
  public $caTotalW = 0;
  public $caTotalJ = 0;

  public $iTotalH = 0;
  public $iTotalW = 0;
  public $iTotalJ = 0;

  public $biTotalH = 0;
  public $biTotalW = 0;
  public $biTotalJ = 0;

  public $rpTotalH = 0;
  public $rpTotalW = 0;
  public $rpTotalJ = 0;

  public $raTotalH = 0;
  public $raTotalW = 0;
  public $raTotalJ = 0;

  public $cTotalH = 0;
  public $cTotalW = 0;
  public $cTotalJ = 0;

  public $ppTotalH = 0;
  public $ppTotalW = 0;
  public $ppTotalJ = 0;

  public $taTotalH = 0;
  public $taTotalW = 0;
  public $taTotalJ = 0;

  public $liTotalH = 0;
  public $liTotalW = 0;
  public $liTotalJ = 0;

  public $dbTotalH = 0;
  public $dbTotalW = 0;
  public $dbTotalJ = 0;

  public $oaTotalH = 0;
  public $oaTotalW = 0;
  public $oaTotalJ = 0;

  /*Grand Total*/
  public $grandTotalH = 0;
  public $grandTotalW = 0;
  public $grandTotalJ = 0;

  /*Grand Libilities*/
  public $grandLiabilitiesH = 0;
  public $grandLiabilitiesW = 0;
  public $grandLiabilitiesJ = 0;

  /*Net Estate*/
  public $netH = 0;
  public $netW = 0;
  public $netJ = 0;

  /*Grand*/
  public $grand = 0;

  private function calculate($prop, $col, $result) {

    for ($i = 1; $i < 4; $i++) { 
      if ($this->{$prop . $i} == 'H') {
        $this->{$result . 'H'} += $this->{$col . $i};
      } else if ($this->{$prop . $i} == 'W') {
        $this->{$result . 'W'} += $this->{$col . $i};
      } else if ($this->{$prop . $i} == 'J') {
        $this->{$result . 'J'} += $this->{$col . $i};
      }
    }
    
  }

  public function __construct($data)
  {
    foreach ($data as $key => $a) {
     if ($a->type == 'Bank Accounts') {
      $baChildTotal = $a->child_total;
      $baPropertyOf = $a->property_of;
      $baChild1 = $a->child_1;
      $baChild2 = $a->child_2;
      $baChild3 = $a->child_3;
      $baCol1 = $a->col_1;
      $baCol2 = $a->col_2;
      $baCol3 = $a->col_3;
      
     }
     if ($a->type == 'Cash Accounts') {
      $caChildTotal = $a->child_total;
      $caPropertyOf = $a->property_of;
      $caChild1 = $a->child_1;
      $caChild2 = $a->child_2;
      $caChild3 = $a->child_3;
      $caCol1 = $a->col_1;
      $caCol2 = $a->col_2;
      $caCol3 = $a->col_3;
     }

     if ($a->type == 'Investments') {
      $iChildTotal = $a->child_total;
      $iPropertyOf = $a->property_of;
      $iChild1 = $a->child_1;
      $iChild2 = $a->child_2;
      $iChild3 = $a->child_3;
      $iCol1 = $a->col_1;
      $iCol2 = $a->col_2;
      $iCol3 = $a->col_3;
     }

     if ($a->type == 'Business Interests') {
      $biChildTotal = $a->child_total;
      $biPropertyOf = $a->property_of;
      $biChild1 = $a->child_1;
      $biChild2 = $a->child_2;
      $biChild3 = $a->child_3;
      $biCol1 = $a->col_1;
      $biCol2 = $a->col_2;
      $biCol3 = $a->col_3;
     }

     if ($a->type == 'Real Property') {
      $rpChildTotal = $a->child_total;
      $rpPropertyOf = $a->property_of;
      $rpChild1 = $a->child_1;
      $rpChild2 = $a->child_2;
      $rpChild3 = $a->child_3;
      $rpCol1 = $a->col_1;
      $rpCol2 = $a->col_2;
      $rpCol3 = $a->col_3;
      $rpCol4 = $a->col_4;
     }

     if ($a->type == 'Retired Accounts') {
      $raChildTotal = $a->child_total;
      $raPropertyOf = $a->property_of;
      $raChild1 = $a->child_1;
      $raChild2 = $a->child_2;
      $raChild3 = $a->child_3;
      $raCol1 = $a->col_1;
      $raCol2 = $a->col_2;
      $raCol3 = $a->col_3;
     }

     if ($a->type == 'Collectibles') {
      $cChildTotal = $a->child_total;
      $cPropertyOf = $a->property_of;
      $cChild1 = $a->child_1;
      $cChild2 = $a->child_2;
      $cChild3 = $a->child_3;
      $cCol1 = $a->col_1;
      $cCol2 = $a->col_2;
      $cCol3 = $a->col_3;
     }

     if ($a->type == 'Personal Property') {
      $ppChildTotal = $a->child_total;
      $ppPropertyOf = $a->property_of;
      $ppChild1 = $a->child_1;
      $ppChild2 = $a->child_2;
      $ppChild3 = $a->child_3;
      $ppCol1 = $a->col_1;
      $ppCol2 = $a->col_2;
      $ppCol3 = $a->col_3;
     }

     if ($a->type == 'Trust and Annuities') {
      $taChildTotal = $a->child_total;
      $taPropertyOf = $a->property_of;
      $taChild1 = $a->child_1;
      $taChild2 = $a->child_2;
      $taChild3 = $a->child_3;
      $taCol1 = $a->col_1;
      $taCol2 = $a->col_2;
      $taCol3 = $a->col_3;
     }

     if ($a->type == 'Life Insurance Death Benefits') {
      $lfChildTotal = $a->child_total;
      $lfPropertyOf = $a->property_of;
      $lfChild1 = $a->child_1;
      $lfChild2 = $a->child_2;
      $lfChild3 = $a->child_3;
      $lfCol1 = $a->col_1;
      $lfCol2 = $a->col_2;
      $lfCol3 = $a->col_3;
     }

     if ($a->type == 'Other Assets') {
      $oaChildTotal = $a->child_total;
      $oaPropertyOf = $a->property_of;
      $oaChild1 = $a->child_1;
      $oaChild2 = $a->child_2;
      $oaChild3 = $a->child_3;
      $oaCol1 = $a->col_1;
      $oaCol2 = $a->col_2;
      $oaCol3 = $a->col_3;
     }
    }

    /*Bank Accounts*/
    $baPropertyOf = $this->exploder((!empty($baPropertyOf) ? $baPropertyOf : ''));
    $this->baProperty1 = (isset($baPropertyOf[0])) ? $baPropertyOf[0] : '';
    $this->baProperty2 = (isset($baPropertyOf[1])) ? $baPropertyOf[1] : '';
    $this->baProperty3 = (isset($baPropertyOf[2])) ? $baPropertyOf[2] : '';
    $baChild1 = $this->exploder((!empty($baChild1) ? $baChild1 : ''));
    $this->baChild11 = (isset($baChild1[0])) ? $baChild1[0] : '';
    $this->baChild12 = (isset($baChild1[1])) ? $baChild1[1] : '';
    $this->baChild13 = (isset($baChild1[2])) ? $baChild1[2] : '' ;
    $baChild2 = $this->exploder((!empty($baChild2) ? $baChild2 : ''));
    $this->baChild21 = (isset($baChild2[0])) ? $baChild2[0] : '';
    $this->baChild22 = (isset($baChild2[1])) ? $baChild2[1] : '';
    $this->baChild23 = (isset($baChild2[2])) ? $baChild2[2] : '';
    $baChild3 = $this->exploder((!empty($baChild3) ? $baChild3 : ''));
    $this->baChild31 = (isset($baChild3[0])) ? $baChild3[0] : '';
    $this->baChild32 = (isset($baChild3[1])) ? $baChild3[1] : '';
    $this->baChild33 = (isset($baChild3[2])) ? $baChild3[2] : '';
    $baChildTotal = $this->exploder((!empty($baChildTotal) ? $baChildTotal : ''));
    $this->baChildTotal1 = (isset($baChildTotal[0])) ? $baChildTotal[0] : '';
    $this->baChildTotal2 = (isset($baChildTotal[1])) ? $baChildTotal[1] : '';
    $this->baChildTotal3 = (isset($baChildTotal[2])) ? $baChildTotal[2] : '';
    $baCol1 = $this->exploder((!empty($baCol1) ? $baCol1 : ''));
    $this->baCol11 = (isset($baCol1[0])) ? $baCol1[0] : '';
    $this->baCol12 = (isset($baCol1[1])) ? $baCol1[1] : '';
    $this->baCol13 = (isset($baCol1[2])) ? $baCol1[2] : '';
    $baCol2 = $this->exploder((!empty($baCol2) ? $baCol2 : ''));
    $this->baCol21 = (isset($baCol2[0])) ? $baCol2[0] :'';
    $this->baCol22 = (isset($baCol2[1])) ? $baCol2[1] :'';
    $this->baCol23 = (isset($baCol2[2])) ? $baCol2[2] :'';
    $baCol3 = $this->exploder((!empty($baCol3) ? $baCol3 : ''));
    $this->baCol31 = (isset($baCol3[0])) ? $baCol3[0] :'';
    $this->baCol32 = (isset($baCol3[1])) ? $baCol3[1] :'';
    $this->baCol33 = (isset($baCol3[2])) ? $baCol3[2] :'';
    /*SUMMARY*/
    $this->calculate('baProperty', 'baCol3', 'baTotal');

    /*Cash Accoutns*/
    $caPropertyOf = $this->exploder((!empty($caPropertyOf) ? $caPropertyOf : ''));
    $this->caProperty1 = (isset($caPropertyOf[0])) ? $caPropertyOf[0] : '';
    $this->caProperty2 = (isset($caPropertyOf[1])) ? $caPropertyOf[1] : '';
    $this->caProperty3 = (isset($caPropertyOf[2])) ? $caPropertyOf[2] : '';
    $caChild1 = $this->exploder((!empty($caChild1) ? $caChild1 : ''));
    $this->caChild11 = (isset($caChild1[0])) ? $caChild1[0] : '';
    $this->caChild12 = (isset($caChild1[1])) ? $caChild1[1] : '';
    $this->caChild13 = (isset($caChild1[2])) ? $caChild1[2] : '' ;
    $caChild2 = $this->exploder((!empty($caChild2) ? $caChild2 : ''));
    $this->caChild21 = (isset($caChild2[0])) ? $caChild2[0] : '';
    $this->caChild22 = (isset($caChild2[1])) ? $caChild2[1] : '';
    $this->caChild23 = (isset($caChild2[2])) ? $caChild2[2] : '';
    $caChild3 = $this->exploder((!empty($caChild3) ? $caChild3 : ''));
    $this->caChild31 = (isset($caChild3[0])) ? $caChild3[0] : '';
    $this->caChild32 = (isset($caChild3[1])) ? $caChild3[1] : '';
    $this->caChild33 = (isset($caChild3[2])) ? $caChild3[2] : '';
    $caChildTotal = $this->exploder((!empty($caChildTotal) ? $caChildTotal : ''));
    $this->caChildTotal1 = (isset($caChildTotal[0])) ? $caChildTotal[0] : '';
    $this->caChildTotal2 = (isset($caChildTotal[1])) ? $caChildTotal[1] : '';
    $this->caChildTotal3 = (isset($caChildTotal[2])) ? $caChildTotal[2] : '';
    $caCol1 = $this->exploder((!empty($caCol1) ? $caCol1 : ''));
    $this->caCol11 = (isset($caCol1[0])) ? $caCol1[0] : '';
    $this->caCol12 = (isset($caCol1[1])) ? $caCol1[1] : '';
    $this->caCol13 = (isset($caCol1[2])) ? $caCol1[2] : '';
    $caCol2 = $this->exploder((!empty($caCol2) ? $caCol2 : ''));
    $this->caCol21 = (isset($caCol2[0])) ? $caCol2[0] :'';
    $this->caCol22 = (isset($caCol2[1])) ? $caCol2[1] :'';
    $this->caCol23 = (isset($caCol2[2])) ? $caCol2[2] :'';
    $caCol3 = $this->exploder((!empty($caCol3) ? $caCol3 : ''));
    $this->caCol31 = (isset($caCol3[0])) ? $caCol3[0] :'';
    $this->caCol32 = (isset($caCol3[1])) ? $caCol3[1] :'';
    $this->caCol33 = (isset($caCol3[2])) ? $caCol3[2] :'';
    /*SUMMARY*/
    $this->calculate('caProperty', 'caCol3', 'caTotal');

    /*Investments*/
    $iPropertyOf = $this->exploder((!empty($iPropertyOf) ? $iPropertyOf : ''));
    $this->iProperty1 = (isset($iPropertyOf[0])) ? $iPropertyOf[0] : '';
    $this->iProperty2 = (isset($iPropertyOf[1])) ? $iPropertyOf[1] : '';
    $this->iProperty3 = (isset($iPropertyOf[2])) ? $iPropertyOf[2] : '';
    $iChild1 = $this->exploder((!empty($iChild1) ? $iChild1 : ''));
    $this->iChild11 = (isset($iChild1[0])) ? $iChild1[0] : '';
    $this->iChild12 = (isset($iChild1[1])) ? $iChild1[1] : '';
    $this->iChild13 = (isset($iChild1[2])) ? $iChild1[2] : '' ;
    $iChild2 = $this->exploder((!empty($iChild2) ? $iChild2 : ''));
    $this->iChild21 = (isset($iChild2[0])) ? $iChild2[0] : '';
    $this->iChild22 = (isset($iChild2[1])) ? $iChild2[1] : '';
    $this->iChild23 = (isset($iChild2[2])) ? $iChild2[2] : '';
    $iChild3 = $this->exploder((!empty($iChild3) ? $iChild3 : ''));
    $this->iChild31 = (isset($iChild3[0])) ? $iChild3[0] : '';
    $this->iChild32 = (isset($iChild3[1])) ? $iChild3[1] : '';
    $this->iChild33 = (isset($iChild3[2])) ? $iChild3[2] : '';
    $iChildTotal = $this->exploder((!empty($iChildTotal) ? $iChildTotal : ''));
    $this->iChildTotal1 = (isset($iChildTotal[0])) ? $iChildTotal[0] : '';
    $this->iChildTotal2 = (isset($iChildTotal[1])) ? $iChildTotal[1] : '';
    $this->iChildTotal3 = (isset($iChildTotal[2])) ? $iChildTotal[2] : '';
    $iCol1 = $this->exploder((!empty($iCol1) ? $iCol1 : ''));
    $this->iCol11 = (isset($iCol1[0])) ? $iCol1[0] : '';
    $this->iCol12 = (isset($iCol1[1])) ? $iCol1[1] : '';
    $this->iCol13 = (isset($iCol1[2])) ? $iCol1[2] : '';
    $iCol2 = $this->exploder((!empty($iCol2) ? $iCol2 : ''));
    $this->iCol21 = (isset($iCol2[0])) ? $iCol2[0] :'';
    $this->iCol22 = (isset($iCol2[1])) ? $iCol2[1] :'';
    $this->iCol23 = (isset($iCol2[2])) ? $iCol2[2] :'';
    $iCol3 = $this->exploder((!empty($iCol3) ? $iCol3 : ''));
    $this->iCol31 = (isset($iCol3[0])) ? $iCol3[0] :'';
    $this->iCol32 = (isset($iCol3[1])) ? $iCol3[1] :'';
    $this->iCol33 = (isset($iCol3[2])) ? $iCol3[2] :'';
    /*SUMMARY*/
    $this->calculate('iProperty', 'iCol3', 'caTotal');

    /*Business Interest*/
    $biPropertyOf = $this->exploder((!empty($biPropertyOf) ? $biPropertyOf : ''));
    $this->biProperty1 = (isset($biPropertyOf[0])) ? $biPropertyOf[0] : '';
    $this->biProperty2 = (isset($biPropertyOf[1])) ? $biPropertyOf[1] : '';
    $this->biProperty3 = (isset($biPropertyOf[2])) ? $biPropertyOf[2] : '';
    $biChild1 = $this->exploder((!empty($biChild1) ? $biChild1 : ''));
    $this->biChild11 = (isset($biChild1[0])) ? $biChild1[0] : '';
    $this->biChild12 = (isset($biChild1[1])) ? $biChild1[1] : '';
    $this->biChild13 = (isset($biChild1[2])) ? $biChild1[2] : '' ;
    $biChild2 = $this->exploder((!empty($biChild2) ? $biChild2 : ''));
    $this->biChild21 = (isset($biChild2[0])) ? $biChild2[0] : '';
    $this->biChild22 = (isset($biChild2[1])) ? $biChild2[1] : '';
    $this->biChild23 = (isset($biChild2[2])) ? $biChild2[2] : '';
    $biChild3 = $this->exploder((!empty($biChild3) ? $biChild3 : ''));
    $this->biChild31 = (isset($biChild3[0])) ? $biChild3[0] : '';
    $this->biChild32 = (isset($biChild3[1])) ? $biChild3[1] : '';
    $this->biChild33 = (isset($biChild3[2])) ? $biChild3[2] : '';
    $biChildTotal = $this->exploder((!empty($biChildTotal) ? $biChildTotal : ''));
    $this->biChildTotal1 = (isset($biChildTotal[0])) ? $biChildTotal[0] : '';
    $this->biChildTotal2 = (isset($biChildTotal[1])) ? $biChildTotal[1] : '';
    $this->biChildTotal3 = (isset($biChildTotal[2])) ? $biChildTotal[2] : '';
    $biCol1 = $this->exploder((!empty($biCol1) ? $biCol1 : ''));
    $this->biCol11 = (isset($biCol1[0])) ? $biCol1[0] : '';
    $this->biCol12 = (isset($biCol1[1])) ? $biCol1[1] : '';
    $this->biCol13 = (isset($biCol1[2])) ? $biCol1[2] : '';
    $biCol2 = $this->exploder((!empty($biCol2) ? $biCol2 : ''));
    $this->biCol21 = (isset($biCol2[0])) ? $biCol2[0] :'';
    $this->biCol22 = (isset($biCol2[1])) ? $biCol2[1] :'';
    $this->biCol23 = (isset($biCol2[2])) ? $biCol2[2] :'';
    $biCol3 = $this->exploder((!empty($biCol3) ? $biCol3 : ''));
    $this->biCol31 = (isset($biCol3[0])) ? $biCol3[0] :'';
    $this->biCol32 = (isset($biCol3[1])) ? $biCol3[1] :'';
    $this->biCol33 = (isset($biCol3[2])) ? $biCol3[2] :'';
    /*SUMMARY*/
    $this->calculate('biProperty', 'biCol3', 'biTotal');

    /*Real Property*/
    $rpPropertyOf = $this->exploder((!empty($rpPropertyOf) ? $rpPropertyOf : ''));
    $this->rpProperty1 = (isset($rpPropertyOf[0])) ? $rpPropertyOf[0] : '';
    $this->rpProperty2 = (isset($rpPropertyOf[1])) ? $rpPropertyOf[1] : '';
    $this->rpProperty3 = (isset($rpPropertyOf[2])) ? $rpPropertyOf[2] : '';
    $rpChild1 = $this->exploder((!empty($rpChild1) ? $rpChild1 : ''));
    $this->rpChild11 = (isset($rpChild1[0])) ? $rpChild1[0] : '';
    $this->rpChild12 = (isset($rpChild1[1])) ? $rpChild1[1] : '';
    $this->rpChild13 = (isset($rpChild1[2])) ? $rpChild1[2] : '' ;
    $rpChild2 = $this->exploder((!empty($rpChild2) ? $rpChild2 : ''));
    $this->rpChild21 = (isset($rpChild2[0])) ? $rpChild2[0] : '';
    $this->rpChild22 = (isset($rpChild2[1])) ? $rpChild2[1] : '';
    $this->rpChild23 = (isset($rpChild2[2])) ? $rpChild2[2] : '';
    $rpChild3 = $this->exploder((!empty($rpChild3) ? $rpChild3 : ''));
    $this->rpChild31 = (isset($rpChild3[0])) ? $rpChild3[0] : '';
    $this->rpChild32 = (isset($rpChild3[1])) ? $rpChild3[1] : '';
    $this->rpChild33 = (isset($rpChild3[2])) ? $rpChild3[2] : '';
    $rpChildTotal = $this->exploder((!empty($rpChildTotal) ? $rpChildTotal : ''));
    $this->rpChildTotal1 = (isset($rpChildTotal[0])) ? $rpChildTotal[0] : '';
    $this->rpChildTotal2 = (isset($rpChildTotal[1])) ? $rpChildTotal[1] : '';
    $this->rpChildTotal3 = (isset($rpChildTotal[2])) ? $rpChildTotal[2] : '';
    $rpCol1 = $this->exploder((!empty($rpCol1) ? $rpCol1 : ''));
    $this->rpCol11 = (isset($rpCol1[0])) ? $rpCol1[0] : '';
    $this->rpCol12 = (isset($rpCol1[1])) ? $rpCol1[1] : '';
    $this->rpCol13 = (isset($rpCol1[2])) ? $rpCol1[2] : '';
    $rpCol2 = $this->exploder((!empty($rpCol2) ? $rpCol2 : ''));
    $this->rpCol21 = (isset($rpCol2[0])) ? $rpCol2[0] :'';
    $this->rpCol22 = (isset($rpCol2[1])) ? $rpCol2[1] :'';
    $this->rpCol23 = (isset($rpCol2[2])) ? $rpCol2[2] :'';
    $rpCol3 = $this->exploder((!empty($rpCol3) ? $rpCol3 : ''));
    $this->rpCol31 = (isset($rpCol3[0])) ? $rpCol3[0] :'';
    $this->rpCol32 = (isset($rpCol3[1])) ? $rpCol3[1] :'';
    $this->rpCol33 = (isset($rpCol3[2])) ? $rpCol3[2] :'';
    $rpCol4 = $this->exploder((!empty($rpCol4) ? $rpCol4 : ''));
    $this->rpCol41 = (isset($rpCol4[0])) ? $rpCol4[0] :'';
    $this->rpCol42 = (isset($rpCol4[1])) ? $rpCol4[1] :'';
    $this->rpCol43 = (isset($rpCol4[2])) ? $rpCol4[2] :'';
    /*SUMMARY*/
    $this->calculate('rpProperty', 'rpCol4', 'rpTotal');

    /*Retired Accounts*/
    $raPropertyOf = $this->exploder((!empty($raPropertyOf) ? $raPropertyOf : ''));
    $this->raProperty1 = (isset($raPropertyOf[0])) ? $raPropertyOf[0] : '';
    $this->raProperty2 = (isset($raPropertyOf[1])) ? $raPropertyOf[1] : '';
    $this->raProperty3 = (isset($raPropertyOf[2])) ? $raPropertyOf[2] : '';
    $raChild1 = $this->exploder((!empty($raChild1) ? $raChild1 : ''));
    $this->raChild11 = (isset($raChild1[0])) ? $raChild1[0] : '';
    $this->raChild12 = (isset($raChild1[1])) ? $raChild1[1] : '';
    $this->raChild13 = (isset($raChild1[2])) ? $raChild1[2] : '' ;
    $raChild2 = $this->exploder((!empty($raChild2) ? $raChild2 : ''));
    $this->raChild21 = (isset($raChild2[0])) ? $raChild2[0] : '';
    $this->raChild22 = (isset($raChild2[1])) ? $raChild2[1] : '';
    $this->raChild23 = (isset($raChild2[2])) ? $raChild2[2] : '';
    $raChild3 = $this->exploder((!empty($raChild3) ? $raChild3 : ''));
    $this->raChild31 = (isset($raChild3[0])) ? $raChild3[0] : '';
    $this->raChild32 = (isset($raChild3[1])) ? $raChild3[1] : '';
    $this->raChild33 = (isset($raChild3[2])) ? $raChild3[2] : '';
    $raChildTotal = $this->exploder((!empty($raChildTotal) ? $raChildTotal : ''));
    $this->raChildTotal1 = (isset($raChildTotal[0])) ? $raChildTotal[0] : '';
    $this->raChildTotal2 = (isset($raChildTotal[1])) ? $raChildTotal[1] : '';
    $this->raChildTotal3 = (isset($raChildTotal[2])) ? $raChildTotal[2] : '';
    $raCol1 = $this->exploder((!empty($raCol1) ? $raCol1 : ''));
    $this->raCol11 = (isset($raCol1[0])) ? $raCol1[0] : '';
    $this->raCol12 = (isset($raCol1[1])) ? $raCol1[1] : '';
    $this->raCol13 = (isset($raCol1[2])) ? $raCol1[2] : '';
    $raCol2 = $this->exploder((!empty($raCol2) ? $raCol2 : ''));
    $this->raCol21 = (isset($raCol2[0])) ? $raCol2[0] :'';
    $this->raCol22 = (isset($raCol2[1])) ? $raCol2[1] :'';
    $this->raCol23 = (isset($raCol2[2])) ? $raCol2[2] :'';
    $raCol3 = $this->exploder((!empty($raCol3) ? $raCol3 : ''));
    $this->raCol31 = (isset($raCol3[0])) ? $raCol3[0] :'';
    $this->raCol32 = (isset($raCol3[1])) ? $raCol3[1] :'';
    $this->raCol33 = (isset($raCol3[2])) ? $raCol3[2] :'';
    /*SUMMARY*/
    $this->calculate('raProperty', 'raCol3', 'raTotal');

    /*Collectibles*/
    $cPropertyOf = $this->exploder((!empty($cPropertyOf) ? $cPropertyOf : ''));
    $this->cProperty1 = (isset($cPropertyOf[0])) ? $cPropertyOf[0] : '';
    $this->cProperty2 = (isset($cPropertyOf[1])) ? $cPropertyOf[1] : '';
    $this->cProperty3 = (isset($cPropertyOf[2])) ? $cPropertyOf[2] : '';
    $cChild1 = $this->exploder((!empty($cChild1) ? $cChild1 : ''));
    $this->cChild11 = (isset($cChild1[0])) ? $cChild1[0] : '';
    $this->cChild12 = (isset($cChild1[1])) ? $cChild1[1] : '';
    $this->cChild13 = (isset($cChild1[2])) ? $cChild1[2] : '' ;
    $cChild2 = $this->exploder((!empty($cChild2) ? $cChild2 : ''));
    $this->cChild21 = (isset($cChild2[0])) ? $cChild2[0] : '';
    $this->cChild22 = (isset($cChild2[1])) ? $cChild2[1] : '';
    $this->cChild23 = (isset($cChild2[2])) ? $cChild2[2] : '';
    $cChild3 = $this->exploder((!empty($cChild3) ? $cChild3 : ''));
    $this->cChild31 = (isset($cChild3[0])) ? $cChild3[0] : '';
    $this->cChild32 = (isset($cChild3[1])) ? $cChild3[1] : '';
    $this->cChild33 = (isset($cChild3[2])) ? $cChild3[2] : '';
    $cChildTotal = $this->exploder((!empty($cChildTotal) ? $cChildTotal : ''));
    $this->cChildTotal1 = (isset($cChildTotal[0])) ? $cChildTotal[0] : '';
    $this->cChildTotal2 = (isset($cChildTotal[1])) ? $cChildTotal[1] : '';
    $this->cChildTotal3 = (isset($cChildTotal[2])) ? $cChildTotal[2] : '';
    $cCol1 = $this->exploder((!empty($cCol1) ? $cCol1 : ''));
    $this->cCol11 = (isset($cCol1[0])) ? $cCol1[0] : '';
    $this->cCol12 = (isset($cCol1[1])) ? $cCol1[1] : '';
    $this->cCol13 = (isset($cCol1[2])) ? $cCol1[2] : '';
    $cCol2 = $this->exploder((!empty($cCol2) ? $cCol2 : ''));
    $this->cCol21 = (isset($cCol2[0])) ? $cCol2[0] :'';
    $this->cCol22 = (isset($cCol2[1])) ? $cCol2[1] :'';
    $this->cCol23 = (isset($cCol2[2])) ? $cCol2[2] : '';
    /*SUMMARY*/
    $this->calculate('cProperty', 'cCol2', 'cTotal');

    /*Personal Property*/
    $ppPropertyOf = $this->exploder((!empty($ppPropertyOf) ? $ppPropertyOf : ''));
    $this->ppProperty1 = (isset($ppPropertyOf[0])) ? $ppPropertyOf[0] : '';
    $this->ppProperty2 = (isset($ppPropertyOf[1])) ? $ppPropertyOf[1] : '';
    $this->ppProperty3 = (isset($ppPropertyOf[2])) ? $ppPropertyOf[2] : '';
    $ppChild1 = $this->exploder((!empty($ppChild1) ? $ppChild1 : ''));
    $this->ppChild11 = (isset($ppChild1[0])) ? $ppChild1[0] : '';
    $this->ppChild12 = (isset($ppChild1[1])) ? $ppChild1[1] : '';
    $this->ppChild13 = (isset($ppChild1[2])) ? $ppChild1[2] : '' ;
    $ppChild2 = $this->exploder((!empty($ppChild2) ? $ppChild2 : ''));
    $this->ppChild21 = (isset($ppChild2[0])) ? $ppChild2[0] : '';
    $this->ppChild22 = (isset($ppChild2[1])) ? $ppChild2[1] : '';
    $this->ppChild23 = (isset($ppChild2[2])) ? $ppChild2[2] : '';
    $ppChild3 = $this->exploder((!empty($ppChild3) ? $ppChild3 : ''));
    $this->ppChild31 = (isset($ppChild3[0])) ? $ppChild3[0] : '';
    $this->ppChild32 = (isset($ppChild3[1])) ? $ppChild3[1] : '';
    $this->ppChild33 = (isset($ppChild3[2])) ? $ppChild3[2] : '';
    $ppChildTotal = $this->exploder((!empty($ppChildTotal) ? $ppChildTotal : ''));
    $this->ppChildTotal1 = (isset($ppChildTotal[0])) ? $ppChildTotal[0] : '';
    $this->ppChildTotal2 = (isset($ppChildTotal[1])) ? $ppChildTotal[1] : '';
    $this->ppChildTotal3 = (isset($ppChildTotal[2])) ? $ppChildTotal[2] : '';
    $ppCol1 = $this->exploder((!empty($ppCol1) ? $ppCol1 : ''));
    $this->ppCol11 = (isset($ppCol1[0])) ? $ppCol1[0] : '';
    $this->ppCol12 = (isset($ppCol1[1])) ? $ppCol1[1] : '';
    $this->ppCol13 = (isset($ppCol1[2])) ? $ppCol1[2] : '';
    $ppCol2 = $this->exploder((!empty($ppCol2) ? $ppCol2 : ''));
    $this->ppCol21 = (isset($ppCol2[0])) ? $ppCol2[0] :'';
    $this->ppCol22 = (isset($ppCol2[1])) ? $ppCol2[1] :'';
    $this->ppCol23 = (isset($ppCol2[2])) ? $ppCol2[2] : '';
    /*SUMMARY*/
    $this->calculate('ppProperty', 'ppCol2', 'ppTotal');

    /*Trust and Annuities*/
    $taPropertyOf = $this->exploder((!empty($taPropertyOf) ? $taPropertyOf : ''));
    $this->taProperty1 = (isset($taPropertyOf[0])) ? $taPropertyOf[0] : '';
    $this->taProperty2 = (isset($taPropertyOf[1])) ? $taPropertyOf[1] : '';
    $this->taProperty3 = (isset($taPropertyOf[2])) ? $taPropertyOf[2] : '';
    $taChild1 = $this->exploder((!empty($taChild1) ? $taChild1 : ''));
    $this->taChild11 = (isset($taChild1[0])) ? $taChild1[0] : '';
    $this->taChild12 = (isset($taChild1[1])) ? $taChild1[1] : '';
    $this->taChild13 = (isset($taChild1[2])) ? $taChild1[2] : '' ;
    $taChild2 = $this->exploder((!empty($taChild2) ? $taChild2 : ''));
    $this->taChild21 = (isset($taChild2[0])) ? $taChild2[0] : '';
    $this->taChild22 = (isset($taChild2[1])) ? $taChild2[1] : '';
    $this->taChild23 = (isset($taChild2[2])) ? $taChild2[2] : '';
    $taChild3 = $this->exploder((!empty($taChild3) ? $taChild3 : ''));
    $this->taChild31 = (isset($taChild3[0])) ? $taChild3[0] : '';
    $this->taChild32 = (isset($taChild3[1])) ? $taChild3[1] : '';
    $this->taChild33 = (isset($taChild3[2])) ? $taChild3[2] : '';
    $taChildTotal = $this->exploder((!empty($taChildTotal) ? $taChildTotal : ''));
    $this->taChildTotal1 = (isset($taChildTotal[0])) ? $taChildTotal[0] : '';
    $this->taChildTotal2 = (isset($taChildTotal[1])) ? $taChildTotal[1] : '';
    $this->taChildTotal3 = (isset($taChildTotal[2])) ? $taChildTotal[2] : '';
    $taCol1 = $this->exploder((!empty($taCol1) ? $taCol1 : ''));
    $this->taCol11 = (isset($taCol1[0])) ? $taCol1[0] : '';
    $this->taCol12 = (isset($taCol1[1])) ? $taCol1[1] : '';
    $this->taCol13 = (isset($taCol1[2])) ? $taCol1[2] : '';
    $taCol2 = $this->exploder((!empty($taCol2) ? $taCol2 : ''));
    $this->taCol21 = (isset($taCol2[0])) ? $taCol2[0] :'';
    $this->taCol22 = (isset($taCol2[1])) ? $taCol2[1] :'';
    $this->taCol23 = (isset($taCol2[2])) ? $taCol2[2] :'';
    $taCol3 = $this->exploder((!empty($taCol3) ? $taCol3 : ''));
    $this->taCol31 = (isset($taCol3[0])) ? $taCol3[0] :'';
    $this->taCol32 = (isset($taCol3[1])) ? $taCol3[1] :'';
    $this->taCol33 = (isset($taCol3[2])) ? $taCol3[2] :'';
    /*SUMMARY*/
    $this->calculate('taProperty', 'taCol3', 'taTotal');

    /*Life Insurance Death Benefits*/
    $lfPropertyOf = $this->exploder((!empty($lfPropertyOf) ? $lfPropertyOf : ''));
    $this->lfProperty1 = (isset($lfPropertyOf[0])) ? $lfPropertyOf[0] : '';
    $this->lfProperty2 = (isset($lfPropertyOf[1])) ? $lfPropertyOf[1] : '';
    $this->lfProperty3 = (isset($lfPropertyOf[2])) ? $lfPropertyOf[2] : '';
    $lfChild1 = $this->exploder((!empty($lfChild1) ? $lfChild1 : ''));
    $this->lfChild11 = (isset($lfChild1[0])) ? $lfChild1[0] : '';
    $this->lfChild12 = (isset($lfChild1[1])) ? $lfChild1[1] : '';
    $this->lfChild13 = (isset($lfChild1[2])) ? $lfChild1[2] : '' ;
    $lfChild2 = $this->exploder((!empty($lfChild2) ? $lfChild2 : ''));
    $this->lfChild21 = (isset($lfChild2[0])) ? $lfChild2[0] : '';
    $this->lfChild22 = (isset($lfChild2[1])) ? $lfChild2[1] : '';
    $this->lfChild23 = (isset($lfChild2[2])) ? $lfChild2[2] : '';
    $lfChild3 = $this->exploder((!empty($lfChild3) ? $lfChild3 : ''));
    $this->lfChild31 = (isset($lfChild3[0])) ? $lfChild3[0] : '';
    $this->lfChild32 = (isset($lfChild3[1])) ? $lfChild3[1] : '';
    $this->lfChild33 = (isset($lfChild3[2])) ? $lfChild3[2] : '';
    $lfChildTotal = $this->exploder((!empty($lfChildTotal) ? $lfChildTotal : ''));
    $this->lfChildTotal1 = (isset($lfChildTotal[0])) ? $lfChildTotal[0] : '';
    $this->lfChildTotal2 = (isset($lfChildTotal[1])) ? $lfChildTotal[1] : '';
    $this->lfChildTotal3 = (isset($lfChildTotal[2])) ? $lfChildTotal[2] : '';
    $lfCol1 = $this->exploder((!empty($lfCol1) ? $lfCol1 : ''));
    $this->lfCol11 = (isset($lfCol1[0])) ? $lfCol1[0] : '';
    $this->lfCol12 = (isset($lfCol1[1])) ? $lfCol1[1] : '';
    $this->lfCol13 = (isset($lfCol1[2])) ? $lfCol1[2] : '';
    $lfCol2 = $this->exploder((!empty($lfCol2) ? $lfCol2 : ''));
    $this->lfCol21 = (isset($lfCol2[0])) ? $lfCol2[0] :'';
    $this->lfCol22 = (isset($lfCol2[1])) ? $lfCol2[1] :'';
    $this->lfCol23 = (isset($lfCol2[2])) ? $lfCol2[2] :'';
    $lfCol3 = $this->exploder((!empty($lfCol3) ? $lfCol3 : ''));
    $this->lfCol31 = (isset($lfCol3[0])) ? $lfCol3[0] :'';
    $this->lfCol32 = (isset($lfCol3[1])) ? $lfCol3[1] :'';
    $this->lfCol33 = (isset($lfCol3[2])) ? $lfCol3[2] :'';
    /*SUMMARY*/
    $this->calculate('lfProperty', 'lfCol3', 'lfTotal');

    /*Other Assets*/
    $oaPropertyOf = $this->exploder((!empty($oaPropertyOf) ? $oaPropertyOf : ''));
    $this->oaProperty1 = (isset($oaPropertyOf[0])) ? $oaPropertyOf[0] : '';
    $this->oaProperty2 = (isset($oaPropertyOf[1])) ? $oaPropertyOf[1] : '';
    $this->oaProperty3 = (isset($oaPropertyOf[2])) ? $oaPropertyOf[2] : '';
    $oaChild1 = $this->exploder((!empty($oaChild1) ? $oaChild1 : ''));
    $this->oaChild11 = (isset($oaChild1[0])) ? $oaChild1[0] : '';
    $this->oaChild12 = (isset($oaChild1[1])) ? $oaChild1[1] : '';
    $this->oaChild13 = (isset($oaChild1[2])) ? $oaChild1[2] : '' ;
    $oaChild2 = $this->exploder((!empty($oaChild2) ? $oaChild2 : ''));
    $this->oaChild21 = (isset($oaChild2[0])) ? $oaChild2[0] : '';
    $this->oaChild22 = (isset($oaChild2[1])) ? $oaChild2[1] : '';
    $this->oaChild23 = (isset($oaChild2[2])) ? $oaChild2[2] : '';
    $oaChild3 = $this->exploder((!empty($oaChild3) ? $oaChild3 : ''));
    $this->oaChild31 = (isset($oaChild3[0])) ? $oaChild3[0] : '';
    $this->oaChild32 = (isset($oaChild3[1])) ? $oaChild3[1] : '';
    $this->oaChild33 = (isset($oaChild3[2])) ? $oaChild3[2] : '';
    $oaChildTotal = $this->exploder((!empty($oaChildTotal) ? $oaChildTotal : ''));
    $this->oaChildTotal1 = (isset($oaChildTotal[0])) ? $oaChildTotal[0] : '';
    $this->oaChildTotal2 = (isset($oaChildTotal[1])) ? $oaChildTotal[1] : '';
    $this->oaChildTotal3 = (isset($oaChildTotal[2])) ? $oaChildTotal[2] : '';
    $oaCol1 = $this->exploder((!empty($oaCol1) ? $oaCol1 : ''));
    $this->oaCol11 = (isset($oaCol1[0])) ? $oaCol1[0] : '';
    $this->oaCol12 = (isset($oaCol1[1])) ? $oaCol1[1] : '';
    $this->oaCol13 = (isset($oaCol1[2])) ? $oaCol1[2] : '';
    $oaCol2 = $this->exploder((!empty($oaCol2) ? $oaCol2 : ''));
    $this->oaCol21 = (isset($oaCol2[0])) ? $oaCol2[0] :'';
    $this->oaCol22 = (isset($oaCol2[1])) ? $oaCol2[1] :'';
    $this->oaCol23 = (isset($oaCol2[2])) ? $oaCol2[2] : '';
    /*SUMMARY*/
    $this->calculate('oaProperty', 'oaCol2', 'oaTotal');

    $this->getGrandTotal();
    $this->getTotalLiabilities();
    $this->getNet();
    $this->getGrand();

  }

  private function getGrand()
  {
    $this->grand = $this->netH + $this->netW + $this->netJ;
  }

  private function getNet()
  {
    $this->netH = $this->grandTotalH - $this->grandLiabilitiesH;
    $this->netW = $this->grandTotalW - $this->grandLiabilitiesW;
    $this->netJ = $this->grandTotalJ - $this->grandLiabilitiesJ;
  }

  private function getTotalLiabilities()
  {
    $this->grandLiabilitiesH = $this->rpTotalH;
    $this->grandLiabilitiesW = $this->rpTotalW;
    $this->grandLiabilitiesJ = $this->rpTotalJ;
  }

  private function getGrandTotal()
  {
    $this->grandTotalH = $this->baTotalH + $this->caTotalH 
      + $this->iTotalH + $this->biTotalH + $this->rpTotalH 
      + $this->taTotalH + $this->liTotalH + $this->dbTotalH + $this->oaTotalH;

    $this->grandTotalW = $this->baTotalW + $this->caTotalW 
      + $this->iTotalW + $this->biTotalW + $this->rpTotalW 
      + $this->taTotalW + $this->liTotalW + $this->dbTotalW + $this->oaTotalW;

    $this->grandTotalJ = $this->baTotalJ + $this->caTotalJ 
      + $this->iTotalJ + $this->biTotalJ + $this->rpTotalJ 
      + $this->taTotalJ + $this->liTotalJ + $this->dbTotalW + $this->oaTotalJ;
  }

  private function exploder($str)
  {
    $exp = explode(";", $str);
    return $exp;
  }

  public function net_estate_calculator()
  {
    $content = '';

    $content = '<!-- Start Textarea -->
    <h4 style="text-align: center;"><span style="font-size:18px;"><strong>Estate Planning Worksheet - Living TrsutBuilder v 4.0</strong></span></h4>

    <h4><span style="color:#008080;"><small>Use this worksheet to calculate the net value of your estate before estate taxes. Replace the sample data on lines 9-106 with your own information. Your net estate is then calculated in the summary beginning on the line 108. If you are married the worksheet can subtotal the husband &amp; wife&#39;s separately held property, as well as property which is jointly owned. If you are single simply type the letter H in column F next to the name of the asset. When you have completed rows 9-106, skip down to the summary. You may need to insert additional information regarding liabilities in rows 131 &amp; 132. </small></span></h4>

    <p><strong><span style="font-size:16px;">Exhibit A - Trustor&#39;s Property List</span></strong></p>

    <div class="calculationContainer text-center">
    <table border="1" class="table table-striped" style="width:100%;">
      <tbody>
        <tr class="tableHeader" style="background-color: #4A65CF;">
          <th colspan="8" style="text-align: left;"><strong>Bank Accounts</strong></th>
        </tr>
        <tr>
          <td width="150">&nbsp;</td>
          <td width="">&nbsp;</td>
          <td width="">&nbsp;</td>
          <td style="text-align: center;" width="">&nbsp;</td>
          <td class="text-center" colspan="3" style="text-align: center;">Allocation of Estate</td>
          <td width="">&nbsp;</td>
        </tr>
        <tr class="text-center" style="font-weight: bold">
          <td style="text-align: center;" width="170">Financial Institution</td>
          <td style="font-size: 8pt; text-align: center;" width="140">Type of Account</td>
          <td style="text-align: center;" width="80">Value</td>
          <td style="text-align: center;" width="90">Property of</td>
          <td style="text-align: center;" width="60">Child 1</td>
          <td style="text-align: center;" width="60">Child 2</td>
          <td style="text-align: center;" width="60">Child 3</td>
          <td style="text-align: center;">100%</td>
        </tr>
        <tr>
          <td>' . $this->baCol11 . '</td>
          <td>' . $this->baCol21 . '</td>
          <td>' . $this->baCol31 . '</td>
          <td>' . $this->baProperty1 . '</td>
          <td>' . $this->baChild11 . '</td>
          <td>' . $this->baChild21 . '</td>
          <td>' . $this->baChild31 . '</td>
          <td>' . $this->baChildTotal1 . '</td>
        </tr>
        <tr>
          <td>' . $this->baCol12 . '</td>
          <td>' . $this->baCol22 . '</td>
          <td>' . $this->baCol32 . '</td>
          <td>' . $this->baProperty2 . '</td>
          <td>' . $this->baChild12 . '</td>
          <td>' . $this->baChild22 . '</td>
          <td>' . $this->baChild32 . '</td>
          <td>' . $this->baChildTotal2 . '</td>
        </tr>
        <tr>
          <td>' . $this->baCol13 . '</td>
          <td>' . $this->baCol23 . '</td>
          <td>' . $this->baCol33 . '</td>
          <td>' . $this->baProperty3 . '</td>
          <td>' . $this->baChild13 . '</td>
          <td>' . $this->baChild23 . '</td>
          <td>' . $this->baChild33 . '</td>
          <td>' . $this->baChildTotal3 . '</td>
        </tr>
        <!-- <tr id="addRow">
              <td><a href="#" id="addRowBankAccount">
                Add Field
                <i class="fa fa-plus-circle"></i>
              </a></td>
            </tr> -->
      </tbody>
    </table>
    <br>
    <!-- Cash Accounts -->

    <table border="1" class="table table-striped" id="bankAccountsTable" style="width: 100%;">
      <tbody>
        <tr style="background-color: #4A65CF;">
          <th colspan="8" style="text-align: left;"><strong>Cash Accounts</strong><span><small> (Money, Market, CD&#39;s, Other)</small></span></th>
        </tr>
        <tr class="text-center" style="font-weight: bold">
          <td style="text-align: center;" width="170">Financial Institution</td>
          <td style="font-size: 8pt; text-align: center;" width="140">Type of Account</td>
          <td style="text-align: center;" width="80">Value</td>
          <td style="text-align: center;" width="90">Property of</td>
          <td style="text-align: center;" width="60">Child 1</td>
          <td style="text-align: center;" width="60">Child 2</td>
          <td style="text-align: center;" width="60">Child 3</td>
          <td style="text-align: center;">100%</td>
        </tr>
        <tr>
          <td>' . $this->caCol11 . '</td>
          <td>' . $this->caCol21 . '</td>
          <td>' . $this->caCol31 . '</td>
          <td>' . $this->caProperty1 . '</td>
          <td>' . $this->caChild11 . '</td>
          <td>' . $this->caChild21 . '</td>
          <td>' . $this->caChild31 . '</td>
          <td>' . $this->caChildTotal1 . '</td>
        </tr>
        <tr>
          <td>' . $this->caCol12 . '</td>
          <td>' . $this->caCol22 . '</td>
          <td>' . $this->caCol32 . '</td>
          <td>' . $this->caProperty2 . '</td>
          <td>' . $this->caChild12 . '</td>
          <td>' . $this->caChild22 . '</td>
          <td>' . $this->caChild32 . '</td>
          <td>' . $this->caChildTotal2 . '</td>
        </tr>
        <tr>
          <td>' . $this->caCol13 . '</td>
          <td>' . $this->caCol23 . '</td>
          <td>' . $this->caCol33 . '</td>
          <td>' . $this->caProperty3 . '</td>
          <td>' . $this->caChild13 . '</td>
          <td>' . $this->caChild23 . '</td>
          <td>' . $this->caChild33 . '</td>
          <td>' . $this->caChildTotal3 . '</td>
        </tr>
      </tbody>
    </table><br>
    <!-- Investments -->

    <table border="1" class="table table-striped" id="bankAccountsTable" style="width: 100%;">
      <tbody>
        <tr style="background-color: #4A65CF;">
          <th colspan="8" style="text-align: left;"><strong>Investments</strong><span><small> (Stocks, Bonds, Mutual Funds, Other)</small></span></th>
        </tr>
        <tr>
          <td width="190">&nbsp;</td>
          <td width="">&nbsp;</td>
          <td width="">&nbsp;</td>
          <td width="">&nbsp;</td>
          <td class="text-center" colspan="3" style="text-align: center;">Allocation of Estate</td>
          <td style="text-align: center;" width="">&nbsp;</td>
        </tr>
        <tr class="text-center" style="font-weight: bold">
          <td style="text-align: center;">Description Investments</td>
          <td style="font-size: 8pt; text-align: center;" width="130"># of Shares/Units</td>
          <td style="text-align: center;">Value</td>
          <td style="text-align: center;" width="152">Property of</td>
          <td style="text-align: center;" width="70">Child 1</td>
          <td style="text-align: center;" width="70">Child 2</td>
          <td style="text-align: center;" width="70">Child 3</td>
          <td style="text-align: center;" width="">100%</td>
        </tr>
        <tr>
          <td>' . $this->iCol11 . '</td>
          <td>' . $this->iCol21 . '</td>
          <td>' . $this->iCol31 . '</td>
          <td>' . $this->iProperty1 . '</td>
          <td>' . $this->iChild11 . '</td>
          <td>' . $this->iChild21 . '</td>
          <td>' . $this->iChild31 . '</td>
          <td>' . $this->iChildTotal1 . '</td>
        </tr>
        <tr>
          <td>' . $this->iCol12 . '</td>
          <td>' . $this->iCol22 . '</td>
          <td>' . $this->iCol32 . '</td>
          <td>' . $this->iProperty2 . '</td>
          <td>' . $this->iChild12 . '</td>
          <td>' . $this->iChild22 . '</td>
          <td>' . $this->iChild32 . '</td>
          <td>' . $this->iChildTotal2 . '</td>
        </tr>
        <tr>
          <td>' . $this->iCol13 . '</td>
          <td>' . $this->iCol23 . '</td>
          <td>' . $this->iCol33 . '</td>
          <td>' . $this->iProperty3 . '</td>
          <td>' . $this->iChild13 . '</td>
          <td>' . $this->iChild23 . '</td>
          <td>' . $this->iChild33 . '</td>
          <td>' . $this->iChildTotal3 . '</td>
        </tr>
      </tbody>
    </table><br>
    <!-- Business Interests -->

    <table border="1" class="table table-striped" id="bankAccountsTable" style="width: 100%;">
      <tbody>
        <tr style="background-color: #4A65CF;">
          <th colspan="8" style="text-align: left;"><strong>Business Interests</strong><span><small> (Stocks, Bonds, Mutual Funds, Other)</small></span></th>
        </tr>
        <tr>
          <td style="text-align: center;" width="190">&nbsp;</td>
          <td style="text-align: center;" width="">&nbsp;</td>
          <td style="text-align: center;" width="">&nbsp;</td>
          <td style="text-align: center;" width="">&nbsp;</td>
          <td class="text-center" colspan="3" style="text-align: center;">Allocation of Estate</td>
          <td style="text-align: center;" width="">&nbsp;</td>
        </tr>
        <tr class="text-center" style="font-weight: bold">
          <td style="text-align: center;">Name of Business</td>
          <td style="font-size: 8pt; text-align: center;" width="130">Type of Interest</td>
          <td style="text-align: center;">Value</td>
          <td style="text-align: center;" width="152">Property of</td>
          <td style="text-align: center;" width="70">Child 1</td>
          <td style="text-align: center;" width="70">Child 2</td>
          <td style="text-align: center;" width="70">Child 3</td>
          <td style="text-align: center;" width="">100%</td>
        </tr>
        <tr>
          <td>' . $this->biCol11 . '</td>
          <td>' . $this->biCol21 . '</td>
          <td>' . $this->biCol31 . '</td>
          <td>' . $this->biProperty1 . '</td>
          <td>' . $this->biChild11 . '</td>
          <td>' . $this->biChild21 . '</td>
          <td>' . $this->biChild31 . '</td>
          <td>' . $this->biChildTotal1 . '</td>
        </tr>
        <tr>
          <td>' . $this->biCol12 . '</td>
          <td>' . $this->biCol22 . '</td>
          <td>' . $this->biCol32 . '</td>
          <td>' . $this->biProperty2 . '</td>
          <td>' . $this->biChild12 . '</td>
          <td>' . $this->biChild22 . '</td>
          <td>' . $this->biChild32 . '</td>
          <td>' . $this->biChildTotal2 . '</td>
        </tr>
        <tr>
          <td>' . $this->biCol13 . '</td>
          <td>' . $this->biCol23 . '</td>
          <td>' . $this->biCol33 . '</td>
          <td>' . $this->biProperty3 . '</td>
          <td>' . $this->biChild13 . '</td>
          <td>' . $this->biChild23 . '</td>
          <td>' . $this->biChild33 . '</td>
          <td>' . $this->biChildTotal3 . '</td>
        </tr>
      </tbody>
    </table><br>
    <!-- Real Property -->

    <table border="1" class="table table-striped" id="bankAccountsTable" style="width:100%;">
      <tbody>
        <tr style="background-color: #4A65CF;">
          <th colspan="9" style="text-align: left;"><strong>Real Property</strong><span><small> (Home, Vacation Home, etc. )</small></span></th>
        </tr>
        <tr>
          <td style="text-align: center;" width="190">&nbsp;</td>
          <td style="text-align: center;" width="">&nbsp;</td>
          <td style="text-align: center;" width="">&nbsp;</td>
          <td style="text-align: center;" width="">&nbsp;</td>
          <td class="text-center" colspan="3" style="text-align: center;">Allocation of Estate</td>
          <td style="text-align: center;" width="">&nbsp;</td>
        </tr>
        <tr class="text-center" style="font-weight: bold">
          <td style="text-align: center;">Location</td>
          <td style="font-size: 8pt; text-align: center;" width="100">Market Value</td>
          <td style="text-align: center;" width="100">Mortgage</td>
          <td style="text-align: center;" width="132">Net Value</td>
          <td style="text-align: center;">Property of</td>
          <td style="text-align: center;" width="70">Child 1</td>
          <td style="text-align: center;" width="70">Child 2</td>
          <td style="text-align: center;" width="70">Child 3</td>
          <td style="text-align: center;" width="70">100%</td>
        </tr>
        <tr>
          <td>' . $this->rpCol11 . '</td>
          <td>' . $this->rpCol21 . '</td>
          <td>' . $this->rpCol31 . '</td>
          <td>' . $this->rpCol41 . '</td>
          <td>' . $this->rpProperty1 . '</td>
          <td>' . $this->rpChild11 . '</td>
          <td>' . $this->rpChild21 . '</td>
          <td>' . $this->rpChild31 . '</td>
          <td>' . $this->rpChildTotal1 . '</td>
        </tr>
        <tr>
          <td>' . $this->rpCol12 . '</td>
          <td>' . $this->rpCol22 . '</td>
          <td>' . $this->rpCol32 . '</td>
          <td>' . $this->rpCol42 . '</td>
          <td>' . $this->rpProperty2 . '</td>
          <td>' . $this->rpChild12 . '</td>
          <td>' . $this->rpChild22 . '</td>
          <td>' . $this->rpChild32 . '</td>
          <td>' . $this->rpChildTotal2 . '</td>
        </tr>
        <tr>
          <td>' . $this->rpCol13 . '</td>
          <td>' . $this->rpCol23 . '</td>
          <td>' . $this->rpCol33 . '</td>
          <td>' . $this->rpCol43 . '</td>
          <td>' . $this->rpProperty3 . '</td>
          <td>' . $this->rpChild13 . '</td>
          <td>' . $this->rpChild23 . '</td>
          <td>' . $this->rpChild33 . '</td>
          <td>' . $this->rpChildTotal3 . '</td>
        </tr>
      </tbody>
    </table><br>
    <!-- Retired Accounts -->

    <table border="1" class="table table-striped" id="bankAccountsTable">
      <tbody>
        <tr style="background-color: #4A65CF;">
          <th colspan="8" style="text-align: left;"><strong>Retirement Accounts</strong><span> <small> (401K&#39;s, IRA&#39;s, Profit-Sharing Plans, Other)</small> </span></th>
        </tr>
        <tr>
          <td style="text-align: center;" width="190">&nbsp;</td>
          <td style="text-align: center;" width="">&nbsp;</td>
          <td style="text-align: center;" width="">&nbsp;</td>
          <td style="text-align: center;" width="">&nbsp;</td>
          <td class="text-center" colspan="3" style="text-align: center;">Allocation of Estate</td>
          <td style="text-align: center;" width="">&nbsp;</td>
        </tr>
        <tr class="text-center" style="font-weight: bold">
          <td style="text-align: center;">Financial Institution</td>
          <td style="font-size: 8pt; text-align: center;" width="130">Type of Account</td>
          <td style="text-align: center;">Value</td>
          <td style="text-align: center;" width="152">Property of</td>
          <td style="text-align: center;" width="70">Child 1</td>
          <td style="text-align: center;" width="70">Child 2</td>
          <td style="text-align: center;" width="70">Child 3</td>
          <td style="text-align: center;" width="">100%</td>
        </tr>
        <tr>
          <td>' . $this->raCol11 . '</td>
          <td>' . $this->raCol21 . '</td>
          <td>' . $this->raCol31 . '</td>
          <td>' . $this->raProperty1 . '</td>
          <td>' . $this->raChild11 . '</td>
          <td>' . $this->raChild21 . '</td>
          <td>' . $this->raChild31 . '</td>
          <td>' . $this->raChildTotal1 . '</td>
        </tr>
        <tr>
          <td>' . $this->raCol12 . '</td>
          <td>' . $this->raCol22 . '</td>
          <td>' . $this->raCol32 . '</td>
          <td>' . $this->raProperty2 . '</td>
          <td>' . $this->raChild12 . '</td>
          <td>' . $this->raChild22 . '</td>
          <td>' . $this->raChild32 . '</td>
          <td>' . $this->raChildTotal2 . '</td>
        </tr>
        <tr>
          <td>' . $this->raCol13 . '</td>
          <td>' . $this->raCol23 . '</td>
          <td>' . $this->raCol33 . '</td>
          <td>' . $this->raProperty3 . '</td>
          <td>' . $this->raChild13 . '</td>
          <td>' . $this->raChild23 . '</td>
          <td>' . $this->raChild33 . '</td>
          <td>' . $this->raChildTotal3 . '</td>
        </tr>
      </tbody>
    </table><br>
    <!-- Collectibles -->

    <table border="1" class="table table-striped" id="bankAccountsTable" style="width: 100%;">
      <tbody>
        <tr style="background-color: #4A65CF;">
          <th colspan="8" style="text-align: left;"><strong>Collectibles</strong><span> <small> (Fine Art, Coins, Other)</small> </span></th>
        </tr>
        <tr>
          <td style="text-align: center;" width="190">&nbsp;</td>
          <td style="text-align: center;" width="">&nbsp;</td>
          <td style="text-align: center;" width="">&nbsp;</td>
          <td style="text-align: center;" width="">&nbsp;</td>
          <td class="text-center" colspan="3" style="text-align: center;">Allocation of Estate</td>
          <td style="text-align: center;" width="">&nbsp;</td>
        </tr>
        <tr class="text-center" style="font-weight: bold">
          <td style="text-align: center;">Item</td>
          <td style="text-align: center;">Value</td>
          <td style="text-align: center;" width="200">Property of</td>
          <td style="text-align: center;" width="70">Child 1</td>
          <td style="text-align: center;" width="70">Child 2</td>
          <td style="text-align: center;" width="70">Child 3</td>
          <td style="text-align: center;" width="">100%</td>
        </tr>
        <tr>
          <td>' . $this->cCol11 . '</td>
          <td>' . $this->cCol21 . '</td>
          <td>' . $this->cProperty1 . '</td>
          <td>' . $this->cChild11 . '</td>
          <td>' . $this->cChild21 . '</td>
          <td>' . $this->cChild31 . '</td>
          <td>' . $this->cChildTotal1 . '</td>
        </tr>
        <tr>
          <td>' . $this->cCol12 . '</td>
          <td>' . $this->cCol22 . '</td>
          <td>' . $this->cProperty2 . '</td>
          <td>' . $this->cChild12 . '</td>
          <td>' . $this->cChild22 . '</td>
          <td>' . $this->cChild32 . '</td>
          <td>' . $this->cChildTotal2 . '</td>
        </tr>
        <tr>
          <td>' . $this->cCol13 . '</td>
          <td>' . $this->cCol23 . '</td>
          <td>' . $this->cProperty3 . '</td>
          <td>' . $this->cChild13 . '</td>
          <td>' . $this->cChild23 . '</td>
          <td>' . $this->cChild33 . '</td>
          <td>' . $this->cChildTotal3 . '</td>
        </tr>
      </tbody>
    </table><br>
    <!-- Personal Property -->

    <table border="1" class="table table-striped" id="bankAccountsTable" style="width:100%;">
      <tbody>
        <tr style="background-color: #4A65CF;">
          <th colspan="8" style="text-align: left;"><strong>Personal Property</strong><span> <small> (Furniture, Household Goods, Jewelry)</small> </span></th>
        </tr>
        <tr>
          <td style="text-align: center;" width="190">&nbsp;</td>
          <td style="text-align: center;" width="">&nbsp;</td>
          <td style="text-align: center;" width="">&nbsp;</td>
          <td style="text-align: center;" width="">&nbsp;</td>
          <td class="text-center" colspan="3" style="text-align: center;">Allocation of Estate</td>
          <td style="text-align: center;" width="">&nbsp;</td>
        </tr>
        <tr class="text-center" style="font-weight: bold">
          <td style="text-align: center;">Item</td>
          <td style="text-align: center;">Value</td>
          <td style="text-align: center;" width="200">Property of</td>
          <td style="text-align: center;" width="70">Child 1</td>
          <td style="text-align: center;" width="70">Child 2</td>
          <td style="text-align: center;" width="70">Child 3</td>
          <td style="text-align: center;" width="">100%</td>
        </tr>
        <tr>
          <td>' . $this->ppCol11 . '</td>
          <td>' . $this->ppCol21 . '</td>
          <td>' . $this->ppProperty1 . '</td>
          <td>' . $this->ppChild11 . '</td>
          <td>' . $this->ppChild21 . '</td>
          <td>' . $this->ppChild31 . '</td>
          <td>' . $this->ppChildTotal1 . '</td>
        </tr>
        <tr>
          <td>' . $this->ppCol12 . '</td>
          <td>' . $this->ppCol22 . '</td>
          <td>' . $this->ppProperty2 . '</td>
          <td>' . $this->ppChild12 . '</td>
          <td>' . $this->ppChild22 . '</td>
          <td>' . $this->ppChild32 . '</td>
          <td>' . $this->ppChildTotal2 . '</td>
        </tr>
        <tr>
          <td>' . $this->ppCol13 . '</td>
          <td>' . $this->ppCol23 . '</td>
          <td>' . $this->ppProperty3 . '</td>
          <td>' . $this->ppChild13 . '</td>
          <td>' . $this->ppChild23 . '</td>
          <td>' . $this->ppChild33 . '</td>
          <td>' . $this->ppChildTotal3 . '</td>
        </tr>
      </tbody>
    </table><br>
    <!-- Trust and Annuities -->

    <table border="1" class="table table-striped" id="bankAccountsTable" style="width: 100%;">
      <tbody>
        <tr style="background-color: #4A65CF;">
          <th colspan="8" style="text-align: left;"><strong>Trust and Annuities</strong><span> </span></th>
        </tr>
        <tr>
          <td width="190">&nbsp;</td>
          <td width="">&nbsp;</td>
          <td width="">&nbsp;</td>
          <td width="">&nbsp;</td>
          <td class="text-center" colspan="3" style="text-align: center;">Allocation of Estate</td>
          <td style="text-align: center;" width="">&nbsp;</td>
        </tr>
        <tr class="text-center" style="font-weight: bold">
          <td style="text-align: center;">Name</td>
          <td style="font-size: 8pt; text-align: center;" width="130">Type</td>
          <td style="text-align: center;">Value</td>
          <td style="text-align: center;" width="152">Property of</td>
          <td style="text-align: center;" width="70">Child 1</td>
          <td style="text-align: center;" width="70">Child 2</td>
          <td style="text-align: center;" width="70">Child 3</td>
          <td style="text-align: center;" width="">100%</td>
        </tr>
        <tr>
          <td>' . $this->taCol11 . '</td>
          <td>' . $this->taCol21 . '</td>
          <td>' . $this->taCol31 . '</td>
          <td>' . $this->taProperty1 . '</td>
          <td>' . $this->taChild11 . '</td>
          <td>' . $this->taChild21 . '</td>
          <td>' . $this->taChild31 . '</td>
          <td>' . $this->taChildTotal1 . '</td>
        </tr>
        <tr>
          <td>' . $this->taCol12 . '</td>
          <td>' . $this->taCol22 . '</td>
          <td>' . $this->taCol32 . '</td>
          <td>' . $this->taProperty2 . '</td>
          <td>' . $this->taChild12 . '</td>
          <td>' . $this->taChild22 . '</td>
          <td>' . $this->taChild32 . '</td>
          <td>' . $this->taChildTotal2 . '</td>
        </tr>
        <tr>
          <td>' . $this->taCol13 . '</td>
          <td>' . $this->taCol23 . '</td>
          <td>' . $this->taCol33 . '</td>
          <td>' . $this->taProperty3 . '</td>
          <td>' . $this->taChild13 . '</td>
          <td>' . $this->taChild23 . '</td>
          <td>' . $this->taChild33 . '</td>
          <td>' . $this->taChildTotal3 . '</td>
        </tr>
      </tbody>
    </table><br>
    <!-- Life Insurance Death Benefits -->

    <table border="1" class="table table-striped" id="bankAccountsTable" style="width: 100%;">
      <tbody>
        <tr style="background-color: #4A65CF;">
          <th colspan="8" style="text-align: left;"><strong>Life Insurance Death Benefits</strong><span> </span></th>
        </tr>
        <tr>
          <td style="text-align: center;" width="190">&nbsp;</td>
          <td style="text-align: center;" width="">&nbsp;</td>
          <td style="text-align: center;" width="">&nbsp;</td>
          <td style="text-align: center;" width="">&nbsp;</td>
          <td class="text-center" colspan="3" style="text-align: center;">Allocation of Estate</td>
          <td style="text-align: center;" width="">&nbsp;</td>
        </tr>
        <tr class="text-center" style="font-weight: bold">
          <td style="text-align: center;">Insurance Group</td>
          <td style="font-size: 8pt; text-align: center;" width="130">Policy Number</td>
          <td style="text-align: center;">Value</td>
          <td style="text-align: center;" width="152">Property of</td>
          <td style="text-align: center;" width="70">Child 1</td>
          <td style="text-align: center;" width="70">Child 2</td>
          <td style="text-align: center;" width="70">Child 3</td>
          <td style="text-align: center;" width="">100%</td>
        </tr>
        <tr>
          <td>' . $this->lfCol11 . '</td>
          <td>' . $this->lfCol21 . '</td>
          <td>' . $this->lfCol31 . '</td>
          <td>' . $this->lfProperty1 . '</td>
          <td>' . $this->lfChild11 . '</td>
          <td>' . $this->lfChild21 . '</td>
          <td>' . $this->lfChild31 . '</td>
          <td>' . $this->lfChildTotal1 . '</td>
        </tr>
        <tr>
          <td>' . $this->lfCol12 . '</td>
          <td>' . $this->lfCol22 . '</td>
          <td>' . $this->lfCol32 . '</td>
          <td>' . $this->lfProperty2 . '</td>
          <td>' . $this->lfChild12 . '</td>
          <td>' . $this->lfChild22 . '</td>
          <td>' . $this->lfChild32 . '</td>
          <td>' . $this->lfChildTotal2 . '</td>
        </tr>
        <tr>
          <td>' . $this->lfCol13 . '</td>
          <td>' . $this->lfCol23 . '</td>
          <td>' . $this->lfCol33 . '</td>
          <td>' . $this->lfProperty3 . '</td>
          <td>' . $this->lfChild13 . '</td>
          <td>' . $this->lfChild23 . '</td>
          <td>' . $this->lfChild33 . '</td>
          <td>' . $this->lfChildTotal3 . '</td>
        </tr>
      </tbody>
    </table><br>
    <!-- Other Assets -->

    <table border="1" class="table table-striped" id="bankAccountsTable" style="width: 100%;">
      <tbody>
        <tr style="background-color: #4A65CF;">
          <th colspan="8" style="text-align: left;"><strong>Other Assets</strong><span> </span></th>
        </tr>
        <tr>
          <td style="text-align: center;" width="190">&nbsp;</td>
          <td style="text-align: center;" width="">&nbsp;</td>
          <td style="text-align: center;" width="">&nbsp;</td>
          <td style="text-align: center;" width="">&nbsp;</td>
          <td class="text-center" colspan="3" style="text-align: center;">Allocation of Estate</td>
          <td style="text-align: center;" width="">&nbsp;</td>
        </tr>
        <tr class="text-center" style="font-weight: bold">
          <td style="text-align: center;">Description</td>
          <td style="text-align: center;">Value</td>
          <td style="text-align: center;" width="152">Property of</td>
          <td style="text-align: center;" width="70">Child 1</td>
          <td style="text-align: center;" width="70">Child 2</td>
          <td style="text-align: center;" width="70">Child 3</td>
          <td style="text-align: center;" width="">100%</td>
        </tr>
        <tr>
          <td>' . $this->oaCol11 . '</td>
          <td>' . $this->oaCol21 . '</td>
          <td>' . $this->oaProperty1 . '</td>
          <td>' . $this->oaChild11 . '</td>
          <td>' . $this->oaChild21 . '</td>
          <td>' . $this->oaChild31 . '</td>
          <td>' . $this->oaChildTotal1 . '</td>
        </tr>
        <tr>
          <td>' . $this->oaCol12 . '</td>
          <td>' . $this->oaCol22 . '</td>
          <td>' . $this->oaProperty2 . '</td>
          <td>' . $this->oaChild12 . '</td>
          <td>' . $this->oaChild22 . '</td>
          <td>' . $this->oaChild32 . '</td>
          <td>' . $this->oaChildTotal2 . '</td>
        </tr>
        <tr>
          <td>' . $this->oaCol13 . '</td>
          <td>' . $this->oaCol23 . '</td>
          <td>' . $this->oaProperty3 . '</td>
          <td>' . $this->oaChild13 . '</td>
          <td>' . $this->oaChild23 . '</td>
          <td>' . $this->oaChild33 . '</td>
          <td>' . $this->oaChildTotal3 . '</td>
        </tr>
      </tbody>
    </table><br>
    &nbsp;

    <h3 class="text-center"><strong>&nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; <span style="font-size:16px;">&nbsp; &nbsp; &nbsp; Summary</span></strong></h3>
    <!-- Assets -->

    <table border="0" class="table table-striped" id="bankAccountsTable">
      <tbody>
        <tr style="background-color: #4A65CF;">
          <th colspan="8"><strong>Assets</strong><span> </span></th>
        </tr>
        <tr>
          <td width="230">&nbsp;</td>
          <td width="">Husband/Single Person&#39;s Property</td>
          <td width="">Wife&#39;s Property</td>
          <td width="">Jointly Help Property</td>
        </tr>
        <tr>
          <td>Bank Accounts</td>
          <td style="text-align: center;">$ ' . $this->baTotalH . '</td>
          <td style="text-align: center;">$ ' . $this->baTotalW . '</td>
          <td style="text-align: center;">$ ' . $this->baTotalJ . '</td>
        </tr>
        <tr>
          <td>Cash Accounts</td>
          <td style="text-align: center;">$ ' . $this->caTotalH . '</td>
          <td style="text-align: center;">$ ' . $this->caTotalW . '</td>
          <td style="text-align: center;">$ ' . $this->caTotalJ . '</td>
        </tr>
        <tr>
          <td>Investments</td>
          <td style="text-align: center;">$ ' . $this->iTotalH . '</td>
          <td style="text-align: center;">$ ' . $this->iTotalW . '</td>
          <td style="text-align: center;">$ ' . $this->iTotalJ . '</td>
        </tr>
        <tr>
          <td>Business Interest</td>
          <td style="text-align: center;">$ ' . $this->biTotalH . '</td>
          <td style="text-align: center;">$ ' . $this->biTotalW . '</td>
          <td style="text-align: center;">$ ' . $this->biTotalJ . '</td>
        </tr>
        <tr>
          <td>Real Property</td>
          <td style="text-align: center;">$ ' . $this->rpTotalH . '</td>
          <td style="text-align: center;">$ ' . $this->rpTotalW . '</td>
          <td style="text-align: center;">$ ' . $this->rpTotalJ . '</td>
        </tr>
        <tr>
          <td>Retirement Accounts</td>
          <td style="text-align: center;">$ ' . $this->raTotalH . '</td>
          <td style="text-align: center;">$ ' . $this->raTotalW . '</td>
          <td style="text-align: center;">$ ' . $this->raTotalJ . '</td>
        </tr>
        <tr>
          <td>Collectibles</td>
          <td style="text-align: center;">$ ' . $this->cTotalH . '</td>
          <td style="text-align: center;">$ ' . $this->cTotalW . '</td>
          <td style="text-align: center;">$ ' . $this->cTotalJ . '</td>
        </tr>
        <tr>
          <td>Personal Property</td>
          <td style="text-align: center;">$ ' . $this->ppTotalH . '</td>
          <td style="text-align: center;">$ ' . $this->ppTotalJ . '</td>
          <td style="text-align: center;">$ ' . $this->ppTotalW . '</td>
        </tr>
        <tr>
          <td>Trust &amp; Annuities</td>
          <td style="text-align: center;">$ ' . $this->taTotalH . '</td>
          <td style="text-align: center;">$ ' . $this->taTotalW . '</td>
          <td style="text-align: center;">$ ' . $this->taTotalJ . '</td>
        </tr>
        <tr>
          <td>Life Insurance</td>
          <td style="text-align: center;">$ ' . $this->liTotalH . '</td>
          <td style="text-align: center;">$ ' . $this->liTotalW . '</td>
          <td style="text-align: center;">$ ' . $this->liTotalJ . '</td>
        </tr>
        <tr>
          <td>Death Benefits</td>
          <td style="text-align: center;">$ ' . $this->dbTotalH . '</td>
          <td style="text-align: center;">$ ' . $this->dbTotalW . '</td>
          <td style="text-align: center;">$ ' . $this->dbTotalJ . '</td>
        </tr>
        <tr>
          <td>Other Assets</td>
          <td style="text-align: center;">$ ' . $this->oaTotalH . '</td>
          <td style="text-align: center;">$ ' . $this->oaTotalW . '</td>
          <td style="text-align: center;">$ ' . $this->oaTotalJ . '</td>
        </tr>
        <tr style="background-color: #B1B7CC">
          <td><strong>Total Assets</strong></td>
          <td style="text-align: center;">$ ' . $this->grandTotalH . '</td>
          <td style="text-align: center;">$ ' . $this->grandTotalW . '</td>
          <td style="text-align: center;">$ ' . $this->grandTotalJ . '</td>
        </tr>
        <tr style="background-color: #4A65CF;">
          <th colspan="4"><strong>Liabilities</strong><span> </span></th>
        </tr>
        <tr>
          <td>Mortgages</td>
          <td style="text-align: center;">$ ' . $this->rpTotalH . '</td>
          <td style="text-align: center;">$ ' . $this->rpTotalW . '</td>
          <td style="text-align: center;">$ ' . $this->rpTotalJ . '</td>
        </tr>
        <tr>
          <td>&nbsp;</td>
          <td style="text-align: center;">&nbsp;</td>
          <td style="text-align: center;">&nbsp;</td>
          <td style="text-align: center;">&nbsp;</td>
        </tr>
        <tr style="background-color: #B1B7CC">
          <td><strong>Total Liabilities</strong></td>
          <td style="text-align: center;">$ ' . $this->grandLiabilitiesH . '</td>
          <td style="text-align: center;">$ ' . $this->grandLiabilitiesW . '</td>
          <td style="text-align: center;">$ ' . $this->grandLiabilitiesJ . '</td>
        </tr>
        <tr>
          <td>&nbsp;</td>
          <td>&nbsp;</td>
          <td>&nbsp;</td>
          <td>&nbsp;</td>
        </tr>
        <tr style="background-color: #5CCC98">
          <td><strong>Net Estate</strong></td>
          <td style="text-align: center;">$ ' . $this->netH . '</td>
          <td style="text-align: center;">$ ' . $this->netW . '</td>
          <td style="text-align: center;">$ ' . $this->netJ . '</td>
        </tr>
      </tbody>
    </table>

    <table border="0" class="table table-striped" id="bankAccountsTable">
      <tbody>
        <tr>
          <th style="background-color: #4A65CF;" width="230"><strong>Total Net Estate All Parties</strong><span> </span></th>
          <th><span style="font-size:14px;"><strong>$ ' . $this->grand . '</strong></span></th>
        </tr>
      </tbody>
    </table>
    </div>
    <!-- End Textarea -->

    <p>&nbsp;</p>
    ';

    return $content;
  }
}